﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using TOCFL.ISD.Args.Condition;
using TOCFL.ISD.Model.Testee;
using System.Data;
using DapperExtensions;
using TOCFL.ISD.Dto.Entity.Testee;
using TOCFL.ISD.Args.Response;
using static TOCFL.ISD.Args.Enum;
using TOCFL.ISD.Dao.Generic;

namespace TOCFL.ISD.Dao.Testee
{
    public class TesteeDao : BaseDao
    {
        /// <summary>
        /// 取得所有考生的詳細資料
        /// TODO 未來不可能有這個service
        /// </summary>
        public List<TesteeDetailInfo> GetAllTesteeDetail()
        {
            List<TesteeDetailInfo> result;
            try
            {
                string sql = @"SELECT * FROM Testee";
                //DynamicParameters paramter = new DynamicParameters();
                //paramter.Add("@AC_TOKEN", token);
                using (this.DbFixture.Db)
                {
                    result = this.DbFixture.Db.Connection.Query<TesteeDetailInfo>(sql).ToList();

                }
            }
            catch (Exception)
            {
                throw new Exception();
            }

            return result;
        }
        public TesteeDetailInfo GetTesteeDetailInfoByTesteeId(int testeeId)
        {
            TesteeEntity testeeEntity = new TesteeEntity();
            TesteeDetailInfo testeeDetailInfo = new TesteeDetailInfo();
            string sql = @" SELECT 
                                Testee_Id as TesteeId ,
                                Chinese_Name as ChineseName,
	                            English_Name as EnglishName,
	                            Address,
                                Birth_Date as BirthDate ,
                                Country as Nationality,
                                Gender,
                                Local_Id_No as LocalIdNo ,
                                Email,
                                Phone
                            FROM Testee
                            where Testee_Id = @TesteeId";
            DynamicParameters paramter = new DynamicParameters();
            paramter.Add("@TesteeId", testeeId);
            using (this.DbFixture.Db)
            {
                testeeDetailInfo = this.DbFixture.Db.Connection.Query<TesteeDetailInfo>(sql, paramter).ToList().FirstOrDefault();
            }
            
            return testeeDetailInfo;
        }
        public IList<TesteeGridData> GetTesteeDetailsByExamId(TesteeGridCondition testeeGridCondition , ref int total)
        {
            string sql = @" SELECT 
                                A.Testee_Id as TesteeId ,
                                B.Chinese_Name as ChineseName,
	                            B.English_Name as EnglishName,
	                            eno as TestNo ,
	                            absent as Absent  ,
	                            b1 as RScore,
	                            b2 as RLevel,
	                            a1 as LScore,
	                            a2 as LLevel,
	                            c1 as LRLevel,
                                c3 as c3,
								C.Pass_Status as PassStatus
                            FROM Testee_Exam_Rel A
                                JOIN Testee B ON A.Testee_Id = B.Testee_Id
                                JOIN Exam_Result C ON A.Exam_Result_Id = C.Exam_Result_Id
                            where Exam_Id = @ExamId";
                DynamicParameters paramter = new DynamicParameters();
                paramter.Add("@ExamId", testeeGridCondition.TesteeCondition.ExamId);
            //設定參數-排序設定(預設)
                if (string.IsNullOrEmpty(testeeGridCondition.PaggingArg.SortField))
                {
                    testeeGridCondition.PaggingArg.SortField = "TesteeId";
                    testeeGridCondition.PaggingArg.SortExpression = SortExpression.Desc;
                }
                //資料取得         
                var pagging = new SqlPagging<TesteeGridData>();
                pagging.Fill(sql, testeeGridCondition.PaggingArg, paramter);
                //回傳結果(分頁and排序)
                var result = pagging.Data;
                total = pagging.Total;
            return result;
        }

        public int GetIdxFromTestee()
        {
            int idx = 0;
            try
            {
                string sql = @"SELECT MAX(Testee_id) AS idx FROM Testee";
                //DynamicParameters paramter = new DynamicParameters();
                //paramter.Add("@AC_TOKEN", token);
                using (this.DbFixture.Db)
                {
                    idx = this.DbFixture.Db.Connection.Query<int>(sql).ToList().FirstOrDefault();

                }
            }
            catch (Exception)
            {
                return 0;
            }
            return idx+1;
        }

        public bool AddDataFromTesteeDetailInfo(IList<TesteeDetailInfo> testeeDetailInfos)
        {
            //還缺資料
            //這個要改成一個一個塞喔
            try { 
            IList<TesteeEntity> teeteeEntitys = testeeDetailInfos.Select(testeeDetailInfo => new TesteeEntity()
            {
                TesteeId = testeeDetailInfo.TesteeId,
                Address = testeeDetailInfo.Address,
                Age = testeeDetailInfo.Age,
                BirthDate = testeeDetailInfo.BirthDate,
                LocalIdNo = testeeDetailInfo.LocalIdNo,
                ChineseName = testeeDetailInfo.ChineseName,
                City = testeeDetailInfo.City,
                Country = testeeDetailInfo.Nationality,
                CountryCh = testeeDetailInfo.NationalityCh ,
                EnglishName = testeeDetailInfo.EnglishName,
                Gender = testeeDetailInfo.Gender,
                PostalCode = testeeDetailInfo.ZipCode,
                PassportId = testeeDetailInfo.PassportId,
                ChinaHoursWeekly = testeeDetailInfo.ChinaHoursWeekly,
                CMonths = testeeDetailInfo.CMonths,
                CYears = testeeDetailInfo.CYears,
                OtherCountryName = testeeDetailInfo.OtherCountryName,
                OtherHoursWeekly = testeeDetailInfo.OtherHoursWeekly,
                OYears = testeeDetailInfo.OYears,
                OMonths = testeeDetailInfo.OMonths,
                TaiwanHoursWeekly = testeeDetailInfo.TaiwanHoursWeekly,
                Years = testeeDetailInfo.Years,
                Months = testeeDetailInfo.Months,
                StudentMaterials = testeeDetailInfo.StudyingMaterials,
                SpeakChineseFreq = testeeDetailInfo.SpeakChineseFreq,
                Email = testeeDetailInfo.Email,
                Phone = testeeDetailInfo.Phone,
                Region = testeeDetailInfo.Region,
                FileId = testeeDetailInfo.FileId,
                CreDte = DateTime.Now,
                CreUsr = "Administrator",
                UpdDte = DateTime.Now,
                UpdUsr = "Administrator"
            }).ToList<TesteeEntity>();
            this.DbFixture.Db.Connection.Execute(TesteeImportSQL(), teeteeEntitys);
            }
            catch (Exception ex)
            {
                throw;
            }
            //result = entity.ExamId;

            return true;


        }

        /// <summary>
        /// 有條件式的去後端取資料
        /// </summary>
        public List<TesteeDetailInfo> GetTesteeDetailByCondition(ExamCondition testeeCondition)
        {
            List<TesteeDetailInfo> result;
            try
            {
                string sql = @"SELECT * FROM Testee";
                //DynamicParameters paramter = new DynamicParameters();
                //paramter.Add("@AC_TOKEN", token);
                using (this.DbFixture.Db)
                {
                    result = this.DbFixture.Db.Connection.Query<TesteeDetailInfo>(sql).ToList();

                }
            }
            catch (Exception)
            {
                throw new Exception();
            }

            return result;
        }
        public bool DeleteTesteeByFileId(int fileId)
        {
            string sql = @"DELETE
                           FROM [dbo].[Testee] WHERE File_Id = @FileId";
            using (this.DbFixture.Db)
            {
                DynamicParameters paramter = new DynamicParameters();
                paramter.Add("@FileId", fileId);
                int result = this.DbFixture.Db.Connection.Execute(sql, paramter);
            }
            return true;
        }
        public string TesteeImportSQL()
        {
            return @"INSERT INTO [dbo].[Testee] (
                             [Testee_Id]
                             ,[English_Name]
                             ,[Chinese_Name]
                             ,[Local_Id_No]
                             ,[Passport_Id]
                             ,[Birth_Date]
                             ,[Age]
                             ,[Gender]
                             ,[Address]
                             ,[City]
                             ,[Region]
                             ,[Postal_Code]
                             ,[Country]
                             ,[Country_Ch]
                             ,[Phone]
                             ,[Create_Date]
                             ,[Create_User]
                             ,[Update_Date]
                             ,[Update_User]
                             ,[Email]
                             ,[Taiwan_Hours_Weekly]
                             ,[Years]
                             ,[Months]
                             ,[China_Hours_Weekly]
                             ,[C_Years]
                             ,[C_Months]
                             ,[Other_Country_Name]
                             ,[Other_Hours_Weekly]
                             ,[O_Years]
                             ,[O_Months]
                             ,[Speak_Chinese_Freq]
                             ,[Student_Materials]
                             ,[File_Id]
                             ,[School]
                             ,[Grade]
                             ,[Languang_Location]
                             ,[Take_YCL]
                             ,[Take_YCL_When]
                             ,[Take_YCL_Which_Level]
                             ,[Take_YCL_Passed_Or_Not])
                             VALUES
                            (
                             @TesteeId
                            ,@EnglishName
                            ,@ChineseName
                            ,@LocalIdNo
                            ,@PassportId
                            ,@BirthDate
                            ,@Age
                            ,@Gender
                            ,@Address
                            ,@City
                            ,@Region
                            ,@PostalCode
                            ,@Country
                            ,@CountryCh
                            ,@Phone
                            ,@CreDte
                            ,@CreUsr
                            ,@UpdDte
                            ,@UpdUsr
                            ,@Email
                            ,@TaiwanHoursWeekly
                            ,@Years
                            ,@Months
                            ,@ChinaHoursWeekly
                            ,@CYears
                            ,@CMonths
                            ,@OtherCountryName
                            ,@OtherHoursWeekly
                            ,@OYears
                            ,@OMonths
                            ,@SpeakChineseFreq
                            ,@StudentMaterials
                            ,@FileId
                            ,@School
                            ,@Grade
                            ,@LanguangLocation
                            ,@TakeYCL
                            ,@TakeYCLWhen
                            ,@TakeYCLWhichLevel
                            ,@TakeYCLPassedOrNot)";
        }
    }
}