﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TOCFL.ISD.Dto.Entity.Testee;
using TOCFL.ISD.Model.Testee;

namespace TOCFL.ISD.Dao.Dao.Testee
{
    public class TesteeExamRelDao:BaseDao
    {

        public bool AddTesteeExamRel(IList<TesteeExamRel> testeeExamRels)
        {

            IList<TesteeExamRelEntity> testeeExamRelEntity =
                testeeExamRels.Select(examResultRel => new TesteeExamRelEntity()
                {
                    Account = "",
                    Password = "",
                    AdmissionId = examResultRel.AdmissionId,
                    Comment = examResultRel.Comment,
                    ExamId = examResultRel.ExamId,
                    ExamResultId = examResultRel.ExamResultId,
                    No = examResultRel.No,
                    TesteeId = examResultRel.TesteeId,
                    FileId = examResultRel.FileId ,
                    CreDte = DateTime.Now,
                    CreUsr = "Administrator",
                    UpdDte = DateTime.Now,
                    UpdUsr = "Administrator"
                }).ToList<TesteeExamRelEntity>();

            this.DbFixture.Db.Connection.Execute(ExamTesteeRelImportSQL(), testeeExamRelEntity);

            return true;
        }
        public bool DeleteTesteeExamRelByFileId(int fileId)
        {
            string sql = @"DELETE
                           FROM [dbo].[Testee_Exam_Rel] WHERE File_Id = @FileId";
            using (this.DbFixture.Db)
            {
                DynamicParameters paramter = new DynamicParameters();
                paramter.Add("@FileId", fileId);
                int result = this.DbFixture.Db.Connection.Execute(sql, paramter);
            }
            return true;
        }
        private string ExamTesteeRelImportSQL()
        {
            return @"INSERT INTO [dbo].[Testee_Exam_Rel]
                                 ([Exam_Id]
                                ,[Exam_Result_Id]
                                ,[Testee_Id]
                                ,[eno]
                                ,[euid]
                                ,[epwd]
                                ,[Comment]
                                ,[Signup_Date]
                                ,[Create_Date]
                                ,[Create_User]
                                ,[Update_Date]
                                ,[Update_User]
                                ,[File_Id])
                                VALUES
                                (@ExamId
                                ,@ExamResultId
                                ,@TesteeId
                                ,@AdmissionId
                                ,@Account
                                ,@Password
                                ,@Comment
                                ,@SignupDate
                                ,@CreDte
                                ,@CreUsr
                                ,@UpdDte
                                ,@UpdUsr
                                ,@FileId)   ";
        }
    }
}