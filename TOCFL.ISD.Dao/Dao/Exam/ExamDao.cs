﻿using Dapper;
using DapperExtensions;
using System;
using System.Collections.Generic;
using TOCFL.ISD.Dao.Code;
using TOCFL.ISD.Dto.Entity.Exam;
using TOCFL.ISD.Model.Exam;
using TOCFL.ISD.Args.Condition;
using static TOCFL.ISD.Args.Enum;
using TOCFL.ISD.Dao.Generic;
using TOCFL.ISD.Args.Response;
using TOCFL.ISD.Exam.Api.Areas.Exam.Controllers;
using TOCFL.ISD.Sys.Common;

namespace TOCFL.ISD.Dao.Exam
{
    public class ExamDao : BaseDao
    {
        CodeDataDao codeDataDao = new CodeDataDao();

        /// <summary>
        /// 帶入Condition 取得考試資訊
        /// </summary>
        /// <param name="exmaGridCondition"></param>
        /// <param name="total"></param>
        public IList<ExamGridData> GetExamByConditions(ExamGridCondition exmaGridCondition, ref int total)
        {
            string sql = @"Select 
                             Exam_Id as ExamId ,
                             Exam_Year as Years ,
							 Exam_Date  as ExamDate ,
                             ToExam_Date  as ToExamDate ,
                             FromExam_Date  as FromExamDate ,
                             Testee_Count as TesteeCount ,
                             Test_Type as TestType ,
                             Exam_Organizer as ExamOrganizer,
                             NoPass_Count as NoPassCount,
                             Pass_Count as  PassCount,
                             B.Name as School,
	                         C.Name as ExamDefine ,
							 D.ctitle as Country
                           FROM EXAM A (NOLOCK) 
                           JOIN Exam_Site B on A.Site_Id = B.Exam_Site_Id
                           JOIN Exam_Code C ON A.Exam_Define_Id = C.Code AND C.Type='Exam_Define'
						   JOIN country D ON D.gid = B.Country_Id Where 1=1";
            //TODO Condition 嵌入
            //paramter.Add("@MemberId", arg.MbrId);
            DynamicParameters paramter = new DynamicParameters();
            string sqlOR = string.Empty;
            foreach (var examCondition in exmaGridCondition.ExamConditions)
            {
                if (string.IsNullOrEmpty(sqlOR))
                {
                    sqlOR += " AND ";
                }
                else
                {

                    sqlOR += " OR ";
                }
                if (!string.IsNullOrEmpty(examCondition.ExamType))
                {
                    sqlOR += "A.Exam_Type = @ExamType  AND";
                    paramter.Add("@ExamType", examCondition.ExamType);
                }
                if (!string.IsNullOrEmpty(examCondition.ExamSite))
                {
                    sqlOR += " B.Name like @ExamSite  AND";
                    paramter.Add("@ExamSite", "%" + examCondition.ExamSite + "%");
                }
                if (!string.IsNullOrEmpty(examCondition.ExamSite))
                {
                    sqlOR += " A.Exam_Organizer like @ExamOrganizer  AND";
                    paramter.Add("@ExamOrganizer", "%" + examCondition.ExamOrganizer + "%");
                }
                if (!string.IsNullOrEmpty(examCondition.FromExamDate) && !string.IsNullOrEmpty(examCondition.ToExamDate))
                {
                    sqlOR += " FromExam_Date Between @FromExamDate AND @ToExamDate  AND";
                    paramter.Add("@FromExamDate", examCondition.FromExamDate);
                    paramter.Add("@ToExamDate", examCondition.ToExamDate);
                }
                sqlOR += " 1=1 ";

            }

            sql = sql + sqlOR;

            //設定參數-排序設定(預設)
            if (string.IsNullOrEmpty(exmaGridCondition.PaggingArg.SortField))
            {
                exmaGridCondition.PaggingArg.SortField = "FromExamDate";
                exmaGridCondition.PaggingArg.SortExpression = SortExpression.Desc;
            }
            //特殊運算過的欄位
            if (exmaGridCondition.PaggingArg.SortField.Equals("ExamDateFix"))
            {
                exmaGridCondition.PaggingArg.SortField = "FromExamDate";
            }
            if (exmaGridCondition.PaggingArg.SortField.Equals("PassRate"))
            {
                exmaGridCondition.PaggingArg.SortField = "NoPassCount";
            }

            //資料取得         
            var pagging = new SqlPagging<ExamGridData>();
            pagging.Fill(sql, exmaGridCondition.PaggingArg, paramter);
            //回傳結果(分頁and排序)
            var result = pagging.Data;
            total = pagging.Total;
            return result;
        }

        public bool UpdateExam(ExamDataArg examDataArg)
        {
            bool updateResult = false;


            using (this.DbFixture.Db)
            {
                DynamicParameters paramter = new DynamicParameters();
                ExamEntity examEntity = new ExamEntity()
                {
                    ExamId = examDataArg.ExamId
                };
                ExamEntity result = this.DbFixture.Db.Connection.Get<ExamEntity>(examEntity);
                result.FromExamDate = examDataArg.FromExamDate;
                result.ToExamDate = examDataArg.ToExamDate;
                result.SiteId = codeDataDao.GetIdBySiteName(examDataArg.ExamSite, examDataArg.CountryId);
                //ExamDefineId = codeDataDao.GetCodeByExamDefine(examDataArg.ExamDefine),
                //TesteeCount = examSpecResult.TesteeCount,
                result.ExamOrganizer = examDataArg.Organizer;
                result.TestCategory = examDataArg.TestCategoryId;
                result.TestType = examDataArg.TestTypeId;
                result.ExamType = examDataArg.ExamTypeId;
                result.ExamYear = examDataArg.ExamYear;

                result.UpdDte = DateTime.Now;
                result.UpdUsr = "Administrator";
                updateResult = this.DbFixture.Db.Connection.Update<ExamEntity>(result);
            }

            return updateResult;
        }

        public bool UpdateDataFromExamResult(ExamSpecResult examSpecResult)
        {
            bool resultWithUploadFlag = false;


            using (this.DbFixture.Db)
            {
                DynamicParameters paramter = new DynamicParameters();
                ExamEntity examEntity = new ExamEntity()
                {
                    ExamId = examSpecResult.ExamId
                };
                ExamEntity result = this.DbFixture.Db.Connection.Get<ExamEntity>(examEntity);
                result.FileId = examSpecResult.FileId;
                result.ExamDefineId = examSpecResult.ExamDefine;
                result.NoPassCount = examSpecResult.NoPassCount;
                result.PassCount = examSpecResult.PassCount;
                result.UploadFlag = examSpecResult.UploadFlag;
                resultWithUploadFlag = this.DbFixture.Db.Connection.Update<ExamEntity>(result);
            }

            return resultWithUploadFlag;
        }

        public bool SetUploadFlag(int examId, int fileId, string uploadFlag)
        {
            try
            {

                using (this.DbFixture.Db)
                {
                    DynamicParameters paramter = new DynamicParameters();
                    ExamEntity examEntity = new ExamEntity()
                    {
                        ExamId = examId

                    };
                    ExamEntity result = this.DbFixture.Db.Connection.Get<ExamEntity>(examEntity);
                    result.UploadFlag = uploadFlag;
                    result.FileId = fileId;
                    var resultWithUploadFlag = this.DbFixture.Db.Connection.Update<ExamEntity>(result);
                }
            }
            catch (Exception ex)
            {
                Logger.Write(LogCategoryEnum.Error, ex.ToString());
                throw ex;
            }
            return true;
        }

        public bool DeleteTempExamById(int exmaId)
        {
            try
            {
                string sql = @"DELETE
                           FROM [dbo].[Exam] WHERE Exam_Id = @ExamId";
                using (this.DbFixture.Db)
                {
                    DynamicParameters paramter = new DynamicParameters();
                    paramter.Add("@ExamId", exmaId);
                    int result = this.DbFixture.Db.Connection.Execute(sql, paramter);
                }
            }
            catch (Exception ex)
            {
                Logger.Write(LogCategoryEnum.Error, ex.ToString());
                throw ex;
            }
            return true;
        }


        public int AddExam(ExamDataArg examDataArg)
        {
            int result;
            ExamEntity entity = new ExamEntity()
            {
                FromExamDate = examDataArg.FromExamDate,
                ToExamDate = examDataArg.ToExamDate,
                SiteId = codeDataDao.GetIdBySiteName(examDataArg.ExamSite, examDataArg.CountryId),
                //ExamDefineId = codeDataDao.GetCodeByExamDefine(examDataArg.ExamDefine),
                //TesteeCount = examSpecResult.TesteeCount,
                ExamOrganizer = examDataArg.Organizer,
                TestCategory = examDataArg.TestCategoryId,
                TestType = examDataArg.TestTypeId,
                ExamType = examDataArg.ExamTypeId,
                UploadFlag = examDataArg.UploadFlag,
                ExamYear = examDataArg.ExamYear,

                CreDte = DateTime.Now,
                CreUsr = "Administrator",
                UpdDte = DateTime.Now,
                UpdUsr = "Administrator"
            };
            this.DbFixture.Db.Connection.Insert<ExamEntity>(entity);
            result = entity.ExamId;


            //examEntity.

            return result;
        }

        public IList<ExamUploadGridData> GetUploadExamByConditions(ExamUploadGridCondition examUploadGridCondition, ref int total)
        {
            string sql = @"Select
                             File_Id as FileId ,
                             Exam_Id as ExamId ,
                             Exam_Year as Years ,
							 Exam_Date  as ExamDate ,
                             ToExam_Date  as ToExamDate ,
                             FromExam_Date  as FromExamDate ,
                             Exam_Year  as ExamYear ,
                             Test_Type as TestType ,
							 Test_Category as TestCategory,
                             Exam_Organizer as ExamOrganizer,
                             Upload_Flag  as UploadFlag,
                             Create_Date as CreateDate ,
                             Update_Date as UpdateDate,
                             Exam_Type as ExamType,
                             B.Name as SiteName,
                             F.ctitle as Country, 
                             C.Name as ExamTypeName, 
                             D.Name as TestTypeName, 
                             E.Name as TestCategoryName
                           FROM EXAM (NOLOCK) 
                           Inner JOIN Exam_Site B on Site_Id = B.Exam_Site_Id
                           Inner JOIN Exam_Code C ON Exam_Type = C.Code AND C.Type = 'Exam_Type'
						   Inner JOIN Exam_Code D ON Test_Type = D.Code AND D.Type = 'Test_Type'
						   Inner JOIN Exam_Code E ON Test_Category = E.Code AND E.Type = 'Test_Category'
						   INNER JOIN country F ON F.gid = B.Country_Id 
                           where 1=1                
                           ";
            DynamicParameters paramter = new DynamicParameters();
            if (examUploadGridCondition.ExamUploadConditions.Country > 0)
            {
                sql = sql + @" AND B.Country_Id = @Country";
                paramter.Add("@Country", examUploadGridCondition.ExamUploadConditions.Country);
            }
            if (!string.IsNullOrEmpty(examUploadGridCondition.ExamUploadConditions.ExamType))
            {
                sql = sql + @" AND Exam_Type = @ExamType";
                paramter.Add("@ExamType", examUploadGridCondition.ExamUploadConditions.ExamType);
            }
            if (!string.IsNullOrEmpty(examUploadGridCondition.ExamUploadConditions.TestCategory))
            {
                sql = sql + @" AND Test_Category = @TestCategory";
                paramter.Add("@TestCategory", examUploadGridCondition.ExamUploadConditions.TestCategory);
            }
            if (!string.IsNullOrEmpty(examUploadGridCondition.ExamUploadConditions.TestType))
            {
                sql = sql + @" AND Test_Type = @TestType";
                paramter.Add("@TestType", examUploadGridCondition.ExamUploadConditions.TestType);
            }

            if (!string.IsNullOrEmpty(examUploadGridCondition.ExamUploadConditions.ToExamDate)
                && !string.IsNullOrEmpty(examUploadGridCondition.ExamUploadConditions.FromExamDate))
            {
                sql = sql + @" AND FromExam_Date BETWEEN  @FromExamDate AND @ToExamDate  ";
                paramter.Add("@FromExamDate", examUploadGridCondition.ExamUploadConditions.FromExamDate);

                paramter.Add("@ToExamDate", examUploadGridCondition.ExamUploadConditions.ToExamDate);
            }

            if (!string.IsNullOrEmpty(examUploadGridCondition.ExamUploadConditions.ExamOrganizer))
            {
                sql = sql + @" AND Exam_Organizer LIKE @ExamOrganizer";
                paramter.Add("@ExamOrganizer", "%" + examUploadGridCondition.ExamUploadConditions.ExamOrganizer + "%");
            }

            if (!string.IsNullOrEmpty(examUploadGridCondition.ExamUploadConditions.ExamSite))
            {
                sql = sql + @" AND B.Name LIKE @ExamSite";
                paramter.Add("@ExamSite", "%" + examUploadGridCondition.ExamUploadConditions.ExamSite + "%");
            }

            //設定參數-排序設定(預設)
            if (string.IsNullOrEmpty(examUploadGridCondition.PaggingArg.SortField))
            {
                examUploadGridCondition.PaggingArg.SortField = "CreateDate";
                examUploadGridCondition.PaggingArg.SortExpression = SortExpression.Desc;
            }
            if (examUploadGridCondition.PaggingArg.SortField.ToString().IndexOf("Fix") != -1)
            {
                examUploadGridCondition.PaggingArg.SortField = examUploadGridCondition.PaggingArg.SortField.ToString().Replace("Fix", "");
            }
            //資料取得         
            var pagging = new SqlPagging<ExamUploadGridData>();
            pagging.Fill(sql, examUploadGridCondition.PaggingArg, paramter);
            //回傳結果(分頁and排序)
            var result = pagging.Data;
            total = pagging.Total;
            return result;
        }

        public int AddDataFromExamResult(ExamSpecResult examSpecResult)
        {
            int result;
            ExamEntity entity = new ExamEntity()
            {
                ExamDate = examSpecResult.ExamDate,
                SiteId = codeDataDao.GetIdBySiteName(examSpecResult.School, null),
                ExamDefineId = codeDataDao.GetCodeByExamDefine(examSpecResult.ExamDefine),
                TesteeCount = examSpecResult.TesteeCount,
                ExamOrganizer = examSpecResult.ExamOrganizer,
                TestCategory = examSpecResult.TestCategory,
                TestType = examSpecResult.TestType,
                FileId = examSpecResult.FileId,
                PassCount = examSpecResult.PassCount,
                NoPassCount = examSpecResult.NoPassCount,
                CreDte = DateTime.Now,
                CreUsr = "Administrator",
                UpdDte = DateTime.Now,
                UpdUsr = "Administrator"
            };
            this.DbFixture.Db.Connection.Insert<ExamEntity>(entity);
            result = entity.ExamId;


            //examEntity.

            return result;
        }
        public bool DeleteExamByFileId(int fileId)
        {
            string sql = @"DELETE
                           FROM [dbo].[Exam] WHERE File_Id = @FileId";
            using (this.DbFixture.Db)
            {
                DynamicParameters paramter = new DynamicParameters();
                paramter.Add("@FileId", fileId);
                int result = this.DbFixture.Db.Connection.Execute(sql, paramter);
            }
            return true;
        }
    }
}