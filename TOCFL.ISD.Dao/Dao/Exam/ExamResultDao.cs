﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using TOCFL.ISD.Dto.Entity.Exam;
using TOCFL.ISD.Dto.Map.Exam;
using TOCFL.ISD.Model.Exam;
using TOCFL.ISD.Args.Response;
using TOCFL.ISD.Args.Condition;
using DapperExtensions;

namespace TOCFL.ISD.Dao.Exam
{
    public class ExamResultDao : BaseDao
    {
        public int GetIdxFromExamResult()
        {
            int idx = 0;
            try
            {
                string sql = @"SELECT MAX(Exam_Result_Id) AS idx FROM Exam_Result";
                using (this.DbFixture.Db)
                {
                    idx = this.DbFixture.Db.Connection.Query<int>(sql).ToList().FirstOrDefault();

                }
            }
            catch (Exception)
            {
                return 0;
            }
            return idx + 1;
        }

        public IList<CertificateData> GetPrintCertificates(TesteeGridCondition testeeGridCondition)
        {
            IList<CertificateData> certificateDatas = new List<CertificateData>();
            string composeSql = @"SELECT 
                            D.Exam_Result_Id as ExamResultId ,
                            B.eno as eno ,
                            C.Chinese_Name as name ,
	                        C.English_Name as ename,
							C.Country as country_e ,
							C.Country_Ch as country_c ,
							C.Birth_Date as birthdate ,
	                        A.FromExam_Date as test_date,
							D.Cert_Reg_No as regno ,
                            D.Cert_Print_No as printno,
                            D.c3 as c3,
							D.c2 as c2,
							E.Code as ctype
                          FROM  dbo.Exam AS A 
                          JOIN dbo.Testee_Exam_Rel AS B ON B.Exam_Id = A.Exam_Id 
                          JOIN dbo.Testee AS C ON B.Testee_Id = C.Testee_Id 
                          JOIN dbo.Exam_Result AS D ON D.Exam_Result_Id = B.Exam_Result_Id 
						  JOIN dbo.Exam_Code AS E ON E.Code = A.Exam_Type AND E.Type = 'Exam_Type'
                          WHERE 1=1 AND D.c3 !='No Pass' AND {0} AND D.absent ='' OR D.absent='到考'   order by eno asc";
            DynamicParameters paramter = new DynamicParameters();
            if (string.IsNullOrEmpty(testeeGridCondition.TesteeCondition.TesteeId))
            {
                composeSql = string.Format(composeSql, new string[] { "A.Exam_Id = @ExamId" });
                paramter.Add("@ExamId", testeeGridCondition.TesteeCondition.ExamId);
            }
            else
            {
                composeSql = string.Format(composeSql, new string[] { "C.Testee_Id = @TesteeId" });
                paramter.Add("@TesteeId", testeeGridCondition.TesteeCondition.TesteeId);
            }
            using (this.DbFixture.Db)
            {
                certificateDatas = this.DbFixture.Db.Connection.Query<CertificateData>(composeSql, paramter).ToList();
            }
            //Step 2 整理編號
            
            return certificateDatas.Select(certificateData => new CertificateData() {
                    ExamResultId = certificateData.ExamResultId ,
                    birthdate = certificateData.birthdate ,
                    e2  = certificateData.e2 ,
                    c3 = certificateData.c3 ,
                    c2 = certificateData.c2 ,
                    d2 = certificateData.d2 ,
                    ctype = certificateData.ctype ,
                    name = string.IsNullOrEmpty(certificateData.name) ?certificateData.ename :certificateData.name ,
                    ename = certificateData.ename ,
                    country_e = certificateData.country_e ,
                    country_c = certificateData.country_c ,
                    printno = testeeGridCondition.TesteeCondition.PrintStartNo++,
                    regno = testeeGridCondition.TesteeCondition.RegStartNo++ ,
                    test_date = certificateData.test_date 
            }).ToList();
        }

        public bool UpdateNosWithCert(IList<CertificateData> certificateDatas)
        {
            bool ret = false;
            foreach (var certificateData in certificateDatas)
            {
                using (this.DbFixture.Db)
                {
                    string sql = @"update Exam_Result 
                                   set  Cert_Reg_No = @RegNo , 
                                      Cert_Print_No = @PrintNo,
                                      fdate = @updateDate 
                                    where Exam_Result_Id = @ExamResultId";
                    DynamicParameters paramter = new DynamicParameters();
                    paramter.Add("@RegNo", certificateData.regno);
                    paramter.Add("@PrintNo", certificateData.printno);
                    paramter.Add("@updateDate", DateTime.Now);
                    paramter.Add("@ExamResultId", certificateData.ExamResultId);

                    this.DbFixture.Db.Connection.Execute(sql,paramter);
                }

            }
            ret = true;
            //TODO 保險一點 下次改成transition
            return ret;
        }

        public IList<TesteeScoreData> GetPrintScores(TesteeGridCondition testeeGridCondition)
        {
            IList<TesteeScoreData> testeeScoreDatas = new List<TesteeScoreData>();
            string composeSql = @"SELECT 
                            C.Chinese_Name as name ,
	                        C.English_Name as ename,
	                        B.eno as eno,
                            D.Test_Level as band ,
	                        D.Test_Level_Id as test_band ,
	                        A.ToExam_Date as test_date ,
	                        F.ctitle as test_area_c ,
	                        F.etitle as test_area_e,
	                        D.a1  ,
	                        D.a2  ,
	                        D.ls_cefr ,
	                        D.ls_actfl ,
	                        D.b1 ,
	                        D.b2  ,
	                        D.rd_cefr  ,
	                        D.rd_actfl ,
	                        D.c1  ,
	                        D.L1_Ability as ls_s1 ,
	                        D.L2_Ability as ls_s2 ,
	                        D.L3_Ability as ls_s3 ,
	                        D.R1_Ability as rd_s1 ,
	                        D.R2_Ability as rd_s2 ,
	                        D.R3_Ability as rd_s3 
                          FROM  dbo.Exam AS A 
                          JOIN dbo.Testee_Exam_Rel AS B ON B.Exam_Id = A.Exam_Id 
                          JOIN dbo.Testee AS C ON B.Testee_Id = C.Testee_Id 
                          JOIN dbo.Exam_Result AS D ON D.Exam_Result_Id = B.Exam_Result_Id 
                          JOIN dbo.Exam_Site AS E ON E.Exam_Site_Id = A.Site_Id
                          JOIN dbo.country AS F ON F.gid = E.Country_Id
                          WHERE 1=1 AND {0}  and D.absent ='' OR D.absent='到考'  order by eno asc";

            DynamicParameters paramter = new DynamicParameters();
            if (string.IsNullOrEmpty(testeeGridCondition.TesteeCondition.TesteeId))
            {
                composeSql = string.Format(composeSql, new string[] { "A.Exam_Id = @ExamId" });
                paramter.Add("@ExamId", testeeGridCondition.TesteeCondition.ExamId);
            }
            else
            {
                composeSql = string.Format(composeSql, new string[] { "C.Testee_Id = @TesteeId" });
                paramter.Add("@TesteeId", testeeGridCondition.TesteeCondition.TesteeId);
            }
            using (this.DbFixture.Db)
            {
                testeeScoreDatas = this.DbFixture.Db.Connection.Query<TesteeScoreData>(composeSql, paramter).ToList();
            }
            return testeeScoreDatas;
        }

        public bool AddDataFromExamResult(IList<ExamResult> examResults)
        {

            IList<ExamResultEntity> examResultEntitys = examResults.Select(examResult => new ExamResultEntity()
            {
                CharacterVersion = examResult.CharacterVersion,
                Comment = examResult.Comment,
                Absent = examResult.Absent,
                TestLevel = examResult.TestLevel,
                TestLevelId = examResult.TestLevelId,
                TestRoom = examResult.TestRoom,
                LACTFLLevel = examResult.LACTFLLevel,
                LCEFRLevel = examResult.LCEFRLevel,
                LLevel = examResult.LLevel,
                LRLevel = examResult.LRLevel,
                LRLevelEN = examResult.LRLevelEN,
                LRLevelTW = examResult.LRLevelTW,
                LScore = examResult.LScore,
                RACTFLlevel = examResult.RACTFLlevel,
                RCEFRLevel = examResult.RCEFRLevel,
                RLevel = examResult.RLevel,
                RScore = examResult.RScore,
                ExamResultId = examResult.ExamResultId,
                ExamScore = examResult.ExamScore,
                ExamSiteId = examResult.ExamSiteId,
                FileId = examResult.FileId,
                CreDte = DateTime.Now,
                CreUsr = "Administrator",
                UpdDte = DateTime.Now,
                UpdUsr = "Administrator",
                CEFRLevel = examResult.CEFRLevel,
                L1Ability = examResult.L1Ability,
                L2Ability = examResult.L2Ability,
                L3Ability = examResult.L3Ability,
                R1Ability = examResult.R1Ability,
                R2Ability = examResult.R2Ability,
                R3Ability = examResult.R3Ability,
                PassStatus = examResult.PassStatus,
                TotalScore = examResult.TotalScore
            }).ToList<ExamResultEntity>();

            this.DbFixture.Db.Connection.Execute(ExamResultImportSQL(), examResultEntitys);
            //result = entity.ExamId;
            return true;


        }
        public bool DeleteExamResultByFileId(int fileId)
        {
            string sql = @"DELETE
                           FROM [dbo].[Exam_Result] WHERE File_Id = @FileId";
            using (this.DbFixture.Db)
            {
                DynamicParameters paramter = new DynamicParameters();
                paramter.Add("@FileId", fileId);
                int result = this.DbFixture.Db.Connection.Execute(sql, paramter);
            }
            return true;
        }
        private string ExamResultImportSQL()
        {
            return @"INSERT INTO [dbo].[Exam_Result]
                        (
                         [Character_Version]
                        ,[Exam_Result_Id]
                        ,[Exam_Site_Id]
                        ,[Exam_Score]
                        ,[Test_Level]
                        ,[Test_Level_Id]
                        ,[Test_Room]
                        ,[absent]
                        ,[comment]
                        ,[a1]
                        ,[a2]
                        ,[ls_cefr]
                        ,[ls_actfl]
                        ,[b1]
                        ,[b2]
                        ,[rd_cefr]
                        ,[rd_actfl]
                        ,[c1]
                        ,[c2]
                        ,[c3]
                        ,[Create_Date]
                        ,[Create_User]
                        ,[fdate]
                        ,[lastuser]
                        ,[File_Id] 
                        ,[Pass_Status]
                        ,[CEFR_Level]
                        ,[L1_Ability]
                        ,[L2_Ability]
                        ,[L3_Ability]
                        ,[R1_Ability]
                        ,[R2_Ability]
                        ,[R3_Ability] )
                VALUES
                    (
                     @CharacterVersion
                    ,@ExamResultId
                    ,@ExamSiteId
                    ,@ExamScore
                    ,@TestLevel
                    ,@TestLevelId
                    ,@TestRoom
                    ,@Absent
                    ,@Comment
                    ,@LScore
                    ,@LLevel
                    ,@LCEFRLevel
                    ,@LACTFLLevel
                    ,@RScore
                    ,@RLevel
                    ,@RCEFRLevel
                    ,@RACTFLlevel
                    ,@LRLevel
                    ,@LRLevelTW
                    ,@LRLevelEN
                   
                    ,@CreDte
                    ,@CreUsr
                    ,@UpdDte
                    ,@UpdUsr
                    ,@FileId
                    ,@PassStatus
                    ,@CEFRLevel
                    ,@L1Ability
                    ,@L2Ability
                    ,@L3Ability
                    ,@R1Ability
                    ,@R2Ability
                    ,@R3Ability)";
        }
    }
}