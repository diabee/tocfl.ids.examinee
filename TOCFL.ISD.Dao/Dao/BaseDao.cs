﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using Dapper;
using DapperExtensions;
using DapperExtensions.Sql;
using System.Data;
using TOCFL.ISD.Dao.Generic;

namespace TOCFL.ISD.Dao
{
    public class BaseDao
    {
        protected SqlServerFixture DbFixture;

        public BaseDao()
        {
            this.ChangeDB(Variable.GenericConnectionID);
        }

        public void ChangeDB(string connectionID)
        {
            this.DbFixture = new SqlServerFixture(connectionID);
        }
    }
}