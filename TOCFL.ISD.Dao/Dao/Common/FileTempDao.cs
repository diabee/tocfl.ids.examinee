﻿using Dapper;
using DapperExtensions;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Transactions;
using System.Web;
using TOCFL.ISD.Dao.Dao.Testee;
using TOCFL.ISD.Dao.Exam;
using TOCFL.ISD.Dao.Testee;
using TOCFL.ISD.Dto.Entity.Common;
using TOCFL.ISD.Model.Common;

namespace TOCFL.ISD.Dao.Common
{
    public class FileTempDao : BaseDao
    {
        public int AddFileTempLog(FileTempLog fileTempLog)
        {
            int result;
            IDbTransaction tran = this.DbFixture.Db.Connection.BeginTransaction();
            try
            {
                ExamFileTempLogEntity entity = new ExamFileTempLogEntity()
                {
                    FileName = fileTempLog.FileName,
                    ExamId = fileTempLog.ExamId,
                    FileNameFix = fileTempLog.FileNameFix,
                    FileSize = fileTempLog.FileSize,
                    FilePath = fileTempLog.FilePath,
                    FilePathFix = fileTempLog.FilePathFix,
                    ParserFlag = fileTempLog.ParserFlag,
                    CreDte = fileTempLog.CreateDate,
                    CreUsr = fileTempLog.CreateUser,

                };
                this.DbFixture.Db.Connection.Insert<ExamFileTempLogEntity>(entity, tran);

                result = entity.FileId;


                tran.Commit();

            }
            catch (Exception ex)
            {
                tran.Rollback();
                throw ex;
            }
            return result;
        }

        public bool CheckTempFileExistByName(string fileName)
        {
            string sql = @"SELECT *
                           FROM [dbo].[Exam_Upload_Temp_Log] WHERE File_Name = @FileName";
            int count = 0;
            using (this.DbFixture.Db)
            {
                DynamicParameters paramter = new DynamicParameters();
                paramter.Add("@FileName", fileName);
                count = this.DbFixture.Db.Connection.Query<int>(sql, paramter).ToList().Count();
            }

            return count > 0;
        }

        public FileTempLog GetDataById(string fileId)
        {

            string sql = @"SELECT 
                           File_Name as FileName,
                           Exam_Id as ExamId ,
                           File_Name_Fix as FileNameFix ,
                           File_Size as FileSize,
                           File_Path as FilePath,
                           Parser_Flag as ParserFlag ,
                           Create_Date as CreateDate ,  
                           Create_User as CreateUser
                           FROM [dbo].[Exam_Upload_Temp_Log] WHERE File_Id = @FileId";
            FileTempLog fileTempLog = new FileTempLog();
            using (this.DbFixture.Db)
            {
                DynamicParameters paramter = new DynamicParameters();
                paramter.Add("@FileId", fileId);
                fileTempLog = this.DbFixture.Db.Connection.Query<FileTempLog>(sql, paramter).ToList().FirstOrDefault();
            }
            return fileTempLog;
        }

        public FileTempLog GetDataByExamId(string examId)
        {

            string sql = @"SELECT 
                           File_Id as FileId ,
                           File_Name as FileName,
                           Exam_Id as ExamId ,
                           File_Name_Fix as FileNameFix ,
                           File_Size as FileSize,
                           File_Path as FilePath,
                           Parser_Flag as ParserFlag ,
                           Create_Date as CreateDate ,  
                           Create_User as CreateUser
                           FROM [dbo].[Exam_Upload_Temp_Log] WHERE File_Id = @ExamId";
            FileTempLog fileTempLog = new FileTempLog();
            using (this.DbFixture.Db)
            {
                DynamicParameters paramter = new DynamicParameters();
                paramter.Add("@ExamId", examId);
                fileTempLog = this.DbFixture.Db.Connection.Query<FileTempLog>(sql, paramter).ToList().FirstOrDefault();
            }
            return fileTempLog;
        }

        public bool DeleteFileTempLogByExamId(int examId)
        {
            //use transitionscope 確保資料
            bool result;
            using (var transactionScope = new TransactionScope())
            {

                try
                {
                    result = this.DbFixture.Db.Connection.Delete<ExamFileTempLogEntity>(new { ExamId = examId });
                    
                    transactionScope.Complete();
                }
                catch (Exception ex)
                {
                    transactionScope.Dispose();
                    throw;
                }
            }
            return result;
        }
    }
}