﻿using Dapper;
using DapperExtensions;
using System;
using System.Data;
using System.Linq;
using System.Transactions;
using TOCFL.ISD.Dao.Dao.Testee;
using TOCFL.ISD.Dao.Exam;
using TOCFL.ISD.Dao.Testee;
using TOCFL.ISD.Dto.Entity.Common;
using TOCFL.ISD.Model.Common;

namespace TOCFL.ISD.Dao.Common
{
    public class FileDao : BaseDao
    {
        public int AddFileLog(FileLog fileLog)
        {
            int result;
            //TODO 測試一下transaction 感覺這裡並不需要
            IDbTransaction tran = this.DbFixture.Db.Connection.BeginTransaction();
            try
            {
                ExamFileLogEntity entity = new ExamFileLogEntity()
                {
                    FileName = fileLog.FileName,
                    FileNameFix = fileLog.FileNameFix,
                    FileSize = fileLog.FileSize,
                    FilePath = fileLog.FilePath,
                    FilePathFix = fileLog.FilePathFix,
                    ParserFlag = fileLog.ParserFlag,
                    CreDte = fileLog.CreateDate,
                    CreUsr = fileLog.CreateUser,

                };
                this.DbFixture.Db.Connection.Insert<ExamFileLogEntity>(entity, tran);

                result = entity.FileId;


                tran.Commit();

            }
            catch (Exception ex)
            {
                tran.Rollback();
                throw ex;
            }
            return result;
        }


        public FileTempLog GetFileTempByExamId(int examId)
        {
            string sql = @"SELECT 
                           File_Id as FileId ,
                           Exam_Id as ExamId ,
                           File_Name as FileName ,
                           File_Path as FilePath
                           FROM [dbo].[Exam_Upload_Temp_Log] WHERE Exam_Id = @ExamId";
            FileTempLog fileTempLog = new FileTempLog();
            using (this.DbFixture.Db)
            {
                //because it is not pKey
                //ExamFileTempLogEntity examFileTempLogEntity = new ExamFileTempLogEntity()
                //{
                //    ExamId = examId
                //};
                //path = this.DbFixture.Db.Connection.Get<ExamFileTempLogEntity>(examFileTempLogEntity).FilePath;
                DynamicParameters paramter = new DynamicParameters();
                paramter.Add("@ExamId", examId);
                fileTempLog = this.DbFixture.Db.Connection.Query<FileTempLog>(sql, paramter).ToList().FirstOrDefault();
            }
            return fileTempLog;
        }

        public FileTempLog GetFileTempById(int fileId)
        {
            string sql = @"SELECT 
                           File_Id as FileId ,
                           Exam_Id as ExamId ,
                           File_Name as FileName ,
                           File_Path as FilePath
                           FROM [dbo].[Exam_Upload_Temp_Log] WHERE File_Id = @FileId";
            FileTempLog fileTempLog = new FileTempLog();
            using (this.DbFixture.Db)
            {
                //because it is not pKey
                //ExamFileTempLogEntity examFileTempLogEntity = new ExamFileTempLogEntity()
                //{
                //    ExamId = examId
                //};
                //path = this.DbFixture.Db.Connection.Get<ExamFileTempLogEntity>(examFileTempLogEntity).FilePath;
                DynamicParameters paramter = new DynamicParameters();
                paramter.Add("@FileId", fileId);
                fileTempLog = this.DbFixture.Db.Connection.Query<FileTempLog>(sql, paramter).ToList().FirstOrDefault();
            }
            return fileTempLog;
        }

        public bool CheckFileExistByName(string fileName)
        {
            string sql = @"SELECT *
                           FROM [dbo].[Exam_Upload_Log] WHERE File_Name = @FileName";
            int count = 0;
            using (this.DbFixture.Db)
            {
                DynamicParameters paramter = new DynamicParameters();
                paramter.Add("@FileName", fileName);
                count = this.DbFixture.Db.Connection.Query<int>(sql, paramter).ToList().Count();
            }

            return count > 0;
        }

        public FileLog GetDataById(int fileId)
        {

            string sql = @"SELECT 
                           File_Id   as FileId,
                           File_Name as FileName,
                           File_Name_Fix as FileNameFix ,
                           File_Size as FileSize,
                           File_Path as FilePath,
                           Parser_Flag as ParserFlag ,
                           Create_Date as CreateDate ,  
                           Create_User as CreateUser
                           FROM [dbo].[Exam_Upload_Log] WHERE File_Id = @FileId";
            FileLog fileLog = new FileLog();
            using (this.DbFixture.Db)
            {
                DynamicParameters paramter = new DynamicParameters();
                paramter.Add("@FileId", fileId);
                fileLog = this.DbFixture.Db.Connection.Query<FileLog>(sql, paramter).ToList().FirstOrDefault();
            }
            return fileLog;
        }

        public FileLog GetFileByExamId(int examId)
        {
            string sql = @"SELECT 
                            A.File_Id as FileId ,
	                        B.Exam_Id as ExamId ,
                            A.File_Name as FileName,
                            A.File_Name_Fix as FileNameFix ,
	                        A.File_Size as FileSize,
                            A.File_Path as FilePath,
                            A.Parser_Flag as ParserFlag ,
                            A.Create_Date as CreateDate ,  
                            A.Create_User as CreateUser
                           FROM [dbo].[Exam_Upload_Log] A 
						   JOIN Exam B on B.File_Id = A.File_Id WHERE Exam_Id = @ExamId";
            FileLog fileLog = new FileLog();
            using (this.DbFixture.Db)
            {
                DynamicParameters paramter = new DynamicParameters();
                paramter.Add("@ExamId", examId);
                fileLog = this.DbFixture.Db.Connection.Query<FileLog>(sql, paramter).ToList().FirstOrDefault();
            }
            return fileLog;
        }
        public bool DeleteTempFileLogByfileId(int fileId)
        {
            bool result = false;
            using (this.DbFixture.Db)
            {
                ExamFileTempLogEntity examFileTempLogEntity = new ExamFileTempLogEntity()
                {
                    FileId = fileId
                };
                result = this.DbFixture.Db.Connection.Delete<ExamFileTempLogEntity>(examFileTempLogEntity);
            }
            return result;
        }
        public bool DeleteFileLogByFileId(int fileId)
        {
            bool result;
            result = this.DbFixture.Db.Connection.Delete<ExamFileLogEntity>(new { FileId = fileId });
            return result;
        }
        public bool DeleteFileLogRelationById(int fileId)
        {
            ExamDao examDao = new ExamDao();

            TesteeDao testeeDao = new TesteeDao();
            ExamResultDao examResultDao = new ExamResultDao();
            TesteeExamRelDao testeeExamRelDao = new TesteeExamRelDao();
            //use transitionscope 確保資料
            bool result;
            using (var transactionScope = new TransactionScope())
            {

                try
                {
                    result = this.DbFixture.Db.Connection.Delete<ExamFileLogEntity>(new { FileId = fileId });
                    testeeExamRelDao.DeleteTesteeExamRelByFileId(fileId);
                    examResultDao.DeleteExamResultByFileId(fileId);
                    testeeDao.DeleteTesteeByFileId(fileId);
                    examDao.DeleteExamByFileId(fileId);
                    transactionScope.Complete();
                }
                catch (Exception ex)
                {
                    transactionScope.Dispose();
                    throw;
                }
            }
            return result;
        }
    }
}