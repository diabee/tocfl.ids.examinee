﻿using Dapper;
using DapperExtensions;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TOCFL.ISD.Dto.Entity.Common;
using TOCFL.ISD.Model.Code;
using TOCFL.ISD.Model.Exam;

namespace TOCFL.ISD.Dao.Code
{
    public class CodeDataDao:BaseDao
    {
        public List<CodeData> GetExamCodeByType(string type)
        {
            var result = new List<CodeData>();
            try
            {
                string sql = @"SELECT Code as CodeId,
                                      Name as CodeName,
                                      EName as EnglishCodeName
                                      FROM Exam_Code
                               WHERE Type = @Type
                               Order by CodeId Asc";

                DynamicParameters paramter = new DynamicParameters();
                paramter.Add("@Type", type);
                using (this.DbFixture.Db)
                {
                    result = this.DbFixture.Db.Connection.Query<CodeData>(sql,paramter).ToList() ;
                }
            }
            catch (Exception)
            {
                throw new Exception();
            }
            return result;
        }
        

        public string GetCodeByExamDefine(string examDefine)
        {
            string result = "";
            try
            {
                string sql = @"SELECT Code as CodeId,
                                      Name as CodeName
                                      FROM Exam_Code
                               WHERE Type = 'Exam_Define' 
                               AND   Name = @examDefine";

                DynamicParameters paramter = new DynamicParameters();
                paramter.Add("@examDefine", examDefine);
                using (this.DbFixture.Db)
                {
                    result = this.DbFixture.Db.Connection.Query<CodeData>(sql, paramter).ToList().FirstOrDefault().CodeId;
                }
            }
            catch (Exception)
            {
                throw new Exception();
            }
            return result;
        }

      

        public List<CodeData> GetResultCodeByType(string type)
        {
            var result = new List<CodeData>();
            try
            {
                string sql = @"SELECT Code as CodeId,
                                      Name as CodeName,
                                      EN_Name as EnglishName
                                      FROM Result_Code
                               WHERE Type = @Type
                               Order by CodeId Asc";

                DynamicParameters paramter = new DynamicParameters();
                paramter.Add("@Type", type);
                using (this.DbFixture.Db)
                {
                    result = this.DbFixture.Db.Connection.Query<CodeData>(sql, paramter).ToList();
                }
            }
            catch (Exception)
            {
                throw new Exception();
            }
            return result;
        }

        public List<CodeData> GetExamCodeByType(object cHType)
        {
            throw new NotImplementedException();
        }

        public List<CountryCodeData> GetCountries()
        {
            var result = new List<CountryCodeData>();
            try
            {
                string sql = @"SELECT gid as CodeId,
                                      ctxt as TxtName ,
                                      ctitle as CodeName,
                                      etitle as EnglishName
                                      FROM country
                               Order by CodeId Asc";
                
                using (this.DbFixture.Db)
                {
                    result = this.DbFixture.Db.Connection.Query<CountryCodeData>(sql).ToList();
                }
            }
            catch (Exception)
            {
                throw new Exception();
            }
            return result;
        }

        public int GetIdBySiteName(string examSite,string countryId)
        {
            ExamSiteEntity examSiteEntity;
            int result = 0;
            examSite = string.IsNullOrEmpty(examSite) ? string.Empty : examSite;
            try
            {
                string sql = @"SELECT Exam_Site_Id as ExamSiteId,
                                      Name as Name
                                      FROM Exam_Site
                               WHERE Name = @Name AND Country_Id = @CountryId";

                DynamicParameters paramter = new DynamicParameters();
                paramter.Add("@Name", examSite);
                paramter.Add("@CountryId", countryId);
                using (this.DbFixture.Db)
                {
                    examSiteEntity = this.DbFixture.Db.Connection.Query<ExamSiteEntity>(sql, paramter).FirstOrDefault();
                }
            }
            catch (Exception)
            {
                throw new Exception();
            }
            

            if (examSiteEntity == null)
            {
                //CreateId 
                IDbTransaction tran = this.DbFixture.Db.Connection.BeginTransaction();
                try
                {
                    ExamSiteEntity entity = new ExamSiteEntity()
                    {
                        Name = examSite,
                        CountryId = countryId

                    };
                    this.DbFixture.Db.Connection.Insert<ExamSiteEntity>(entity, tran);

                    result = entity.ExamSiteId;


                    tran.Commit();

                }
                catch (Exception ex)
                {
                    tran.Rollback();
                    throw ex;
                }


            }else
            {
                result = examSiteEntity.ExamSiteId;
            }

            return result;
        }
    }
}