﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Reflection;
using DapperExtensions.Sql;
using TOCFL.ISD.Sys.Common;

namespace TOCFL.ISD.Dao.Generic
{
    public class SqlServerFixture
    {
        private string connectionID;
        private DapperExtensions.IDatabase _Db;
        /// <summary>
        /// ConnectionString 內定義的連線字串ID
        /// </summary>
        /// <param name="connectionID"></param>
        public SqlServerFixture(string connectionID)
        {
            this.connectionID = connectionID;
        }
        /// <summary>
        /// 取得DapperExtensions物件(並建立SQL Server Connection)
        /// </summary>
        public DapperExtensions.IDatabase Db
        {
            get
            {
                if (this._Db == null)
                {
                    SqlConnection Connection = new SqlConnection(ConfigManager.GetConnectionString(this.connectionID));
                    var config = new DapperExtensions.DapperExtensionsConfiguration(typeof(DapperExtensions.Mapper.AutoClassMapper<>), new List<Assembly>(), new SqlServerDialect());
                    var sqlGenerator = new SqlGeneratorImpl(config);
                    this._Db = new DapperExtensions.Database(Connection, sqlGenerator);
                    return this._Db;
                }
                else
                {
                    if (this._Db.Connection.State == System.Data.ConnectionState.Closed)
                    {
                        this._Db.Connection.Open();
                    }
                    return this._Db;
                }
            }
        }

        public void Dispose()
        {

            this._Db.Dispose();

        }
    }
}