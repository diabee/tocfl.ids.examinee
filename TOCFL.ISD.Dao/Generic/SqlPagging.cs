﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TOCFL.ISD.Args.Generic;
using TOCFL.ISD.Dao;

namespace TOCFL.ISD.Dao.Generic
{
    public class SqlPagging<T>
    {
        protected SqlServerFixture DbFixture;

        public SqlPagging()
        {
            this.ChangeDB(Variable.GenericConnectionID);
        }

        public SqlPagging(string connectionID)
        {
            this.DbFixture = new SqlServerFixture(connectionID);
        }

        public void ChangeDB(string connectionID)
        {
            this.DbFixture = new SqlServerFixture(connectionID);
        }
        public int Total { get; set; }


        public List<T> Data { get; set; }
        public void Fill(string sql, PaggingArg paggingArg, DynamicParameters parameters)
        {

            paggingArg.PageSize = paggingArg.PageSize == 0 ? Variable.DefaultPaggingPageSize : paggingArg.PageSize;
            int rowIndex = (paggingArg.PageIndex * paggingArg.PageSize) + 1;

            string orderSql = string.IsNullOrEmpty(paggingArg.SortField)
                ? string.Empty
                : "Order By " + paggingArg.SortField + " " + paggingArg.SortExpression.ToString();

            string composeSql = @"
                SELECT Count(1) From ({0}) As A
                SELECT A.* FROM
                    (
	                SELECT row_number() OVER ({1}) As RowSequence,  * FROM
	                    (
		                {2}
	                    ) As A
                    ) As A WHERE RowSequence BETWEEN {3} AND {4} {5}";
            composeSql = string.Format(composeSql, new string[] { sql, orderSql, sql, rowIndex.ToString(), (rowIndex + paggingArg.PageSize - 1).ToString(), orderSql });

            using (this.DbFixture.Db)
            {
                var query = this.DbFixture.Db.Connection.QueryMultiple(composeSql, parameters);
                this.Total = query.Read<int>().Single();
                this.Data = query.Read<T>().ToList();
            }
        }
    }
}