﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TOCFL.ISD.Model.Common
{
    public class ExcelDetailResult
    {

        //for ExamResult  
        public string No { get; set; }
        public string TestLevel { get; set; }
        public string TestRoom { get; set; }
        public string CharacterVersion { get; set; }
        public string TestNO { get; set; }
        public int ExamResultId { get; set; }
        public int ExamSiteId { get; set; }
        public int ExamScore { get; set; }
        public string Absent { get; set; }
        public string Comment { get; set; }
        public int LScore { get; set; }
        public string LLevel { get; set; }
        public string LCEFRLevel { get; set; }
        public string LACTFLLevel { get; set; }
        public int RScore { get; set; }
        public string RLevel { get; set; }
        public string RCEFRLevel { get; set; }
        public string RACTFLlevel { get; set; }
        public string LRLevel { get; set; }
        public string LRLevelTW { get; set; }
        public string LRLevelEN { get; set; }
        public string LAbilityDesc { get; set; }
        public string RAbilityDesc1 { get; set; }
        public string RAbilityDesc2 { get; set; }
        public string RAbilityDesc3 { get; set; }
        public int SPKScore { get; set; }
        public string SPKLevel { get; set; }
        public int WRScore { get; set; }
        public string WRLevel { get; set; }
        public int CLScroe { get; set; }
        public int CRScore { get; set; }
        public int CScore { get; set; }
        public string CResult { get; set; }
        
        //English Name
        public string EnglishName { get; set; }

        //Chinese Name
        public string ChineseName { get; set; }

        public string LocalIdNo { get; set; }

        public string PassportId { get; set; }

        public string BirthDate { get; set; }

        public string BirthDateCh { get; set; }

        public string BirthDateEn { get; set; }

        public int Age { get; set; }

        public string Gender { get; set; }

        public string Address { get; set; }

        public string City { get; set; }

        public string Region { get; set; }

        public string ZipCode { get; set; }

        public string Nationality { get; set; }
        public string NationalityCh { get; set; }

        public string NativeLanguage { get; set; }
        public string Occupation { get; set; }

        public string OccupationOthers { get; set; }
        public string TaiwanHoursWeekly { get; set; }
        public string Years { get; set; }
        public string Months { get; set; }
        public string ChinaHoursWeekly { get; set; }
        public string CYears { get; set; }
        public string CMonths { get; set; }
        public string OtherCountryName { get; set; }
        public string OtherHoursWeekly { get; set; }
        public string OYears { get; set; }
        public string OMonths { get; set; }
        public string SpeakChineseFreq { get; set; }

        public string StudyingMaterials { get; set; }
        public string Email { get; set; }


        public string Phone { get; set; }

        //CCCC
        public string CEFRLevel { get; set; }
        public string L1Ability { get; set; }
        public string L2Ability { get; set; }
        public string L3Ability { get; set; }
        public string R1Ability { get; set; }
        public string R2Ability { get; set; }
        public string R3Ability { get; set; }
        public string School { get; set; }
        public string Grade { get; set; }
        public string LanguangLocation { get; set; }
        public string TakeYCL { get; set; }
        public string TakeYCLWhen { get; set; }
        public string TakeYCLWhichLevel { get; set; }
        public string TakeYCLPassedOrNot { get; set; }
        public string PassStatus { get; set; }
        public string TotalScore { get; set; }
    }
}