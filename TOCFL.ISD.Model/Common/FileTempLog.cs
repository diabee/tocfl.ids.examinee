﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TOCFL.ISD.Model.Common
{
    public class FileTempLog
    {
        public int FileId { get; set; }
        public int ExamId { get; set; }
        public string FileName { get; set; }

        public string FileNameFix { get; set; }
        public string FileSize { get; set; }
        public string FilePath { get; set; }
        public string FilePathFix { get; set; }
        public string ParserFlag { get; set; }
        public DateTime CreateDate { get; set; }
        public string CreateUser { get; set; }

    }
}