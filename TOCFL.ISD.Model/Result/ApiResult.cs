﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TOCFL.ISD.Model
{
    public class ApiResult<DataT>
    {
        /// <summary>
        /// 後端通訊回覆用的
        /// </summary>
        public Enum.ApiStatusEnum ApiStatus { get; set; }
        /// <summary>
        /// 封包用的
        /// </summary>
        public int Status { get; set; }

        /// <summary>
        /// 訊息
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// 封包資料
        /// </summary>
        public DataT Data { get; set; }

        /// <summary>
        /// 編碼字串
        /// </summary>
        public string sData { get; set; }
    }
}