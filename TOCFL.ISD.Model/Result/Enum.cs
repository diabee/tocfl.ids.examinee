﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TOCFL.ISD.Model
{
    public class Enum
    {
        public enum ApiStatusEnum
        {
            Fail,
            OK,
            InternalServerError,
            Unauthorized,
            NotFound,
            Customer,
            UnKnow

        }

        private static readonly IDictionary<string, string> ApiStatus =
            new Dictionary<string, string>
            {
                {"Fail","失敗"},
                { "OK", "成功" },
                { "InternalServerError", "遠端API錯誤" },
                { "Unauthorized", "驗證失敗" },
                { "NotFound", "找不到符合方法" },
                { "UnKnow", "未知錯誤" },
                { "Customer", "成功" }

            };
        public static string GetApiStatuString(ApiStatusEnum apiStatus)
        {
            return ApiStatus[apiStatus.ToString()];
        }
    }
}