﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TOCFL.ISD.Model.Exam
{
    public class ExamSpecResult
    {
        //from file name
        public string FileName { get; set; }
        public int ExamId { get; set; }
        public string Category { get; set; }
        public string ExamDate { get; set; }
        public string ExamOrganizer { get; set; }
        public string TestType { get; set; }
        public string ExamDefine { get; set; }
        public string School { get; set; }
        public int TesteeCount { get; set; }
        public int NoPassCount { get; set; }
        public int PassCount { get; set; }
        public int FileId { get; set; }
        public string TestCategory { get; set; }
        public string UploadFlag { get; set; }
    }
}