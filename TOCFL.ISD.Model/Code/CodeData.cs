﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TOCFL.ISD.Model.Code
{
    public class CodeData
    {
        public string CodeId { get; set; }
        public string CodeName { get; set; }
        public string EnglishCodeName { get; set; }
    }
}