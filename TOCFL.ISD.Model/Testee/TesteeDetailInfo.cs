﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TOCFL.ISD.Model.Testee
{
    public class TesteeDetailInfo
    {
        public int TesteeId { get; set; }

        public string eno { get; set; }
        //English Name
        public string EnglishName { get; set; }

        //Chinese Name
        public string ChineseName { get; set; }

        public string LocalIdNo { get; set; }

        public string PassportId { get; set; }

        public DateTime? BirthDate { get; set; }

        public string BirthDateCh { get; set; }

        public string BirthDateEn { get; set; }

        public string BirthDateFix { get { return BirthDate?.ToString("yyyy/MM/dd"); } }

        public int Age { get; set; }

        public string Gender { get; set; }

        public string Address { get; set; }

        public string City { get; set; }

        public string Region { get; set; }

        public string ZipCode { get; set; }

        public string Nationality { get; set; }
        public string NationalityCh { get; set; }

        public string NativeLanguage { get; set; }
        public string Occupation { get; set; }

        public string OccupationOthers { get; set; }
        public string TaiwanHoursWeekly { get; set; }
        public string Years { get; set; }
        public string Months { get; set; }
        public string ChinaHoursWeekly { get; set; }
        public string CYears { get; set; }
        public string CMonths { get; set; }
        public string OtherCountryName { get; set; }
        public string OtherHoursWeekly { get; set; }
        public string OYears { get; set; }
        public string OMonths { get; set; }
        public string SpeakChineseFreq { get; set; }

        public string StudyingMaterials { get; set; }
        public string Email { get; set; }


        public string Phone { get; set; }
        public int FileId { get; set; }

        //CCCC
        public string School { get; set; }
        public string Grade { get; set; }
        public string LanguangLocation { get; set; }
        public string TakeYCL { get; set; }
        public string TakeYCLWhen { get; set; }
        public string TakeYCLWhichLevel { get; set; }
        public string TakeYCLPassedOrNot { get; set; }
    }
}