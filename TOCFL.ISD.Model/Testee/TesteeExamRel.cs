﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TOCFL.ISD.Model.Testee
{
    public class TesteeExamRel
    {
        public int TesteeExamId { get; set; }

        public int TesteeId { get; set; }

        public int ExamId { get; set; }

        public int ExamResultId { get; set; }

        public string SignupDate { get; set; }

        public string AdmissionId { get; set; }

        public string Comment { get; set; }
        public string Password { get; internal set; }
        public string Account { get; internal set; }
        public string No { get; internal set; }
        public int FileId { get; set; }
    }
}