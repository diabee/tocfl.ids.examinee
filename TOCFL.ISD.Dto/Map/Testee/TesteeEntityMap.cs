﻿using DapperExtensions.Mapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TOCFL.ISD.Dto.Entity.Testee;

namespace TOCFL.ISD.Dto.Map.Testee
{
    public class TesteeEntityMap: ClassMapper<TesteeEntity>
    {
        public TesteeEntityMap()
        {
            Table("Testee");
            Map(m => m.TesteeId).Column("Testee_Id");
            //to do
            Map(m => m.EnglishName).Column("English_Name"); 
            Map(m => m.ChineseName).Column("Chinese_Name");
            Map(m => m.LocalIdNo).Column("Local_Id_No");
            Map(m => m.PassportId).Column("Passport_Id");
            Map(m => m.BirthDate).Column("Birth_Date");
            Map(m => m.Age).Column("Age");
            Map(m => m.Gender).Column("Gender");
            Map(m => m.Address).Column("Address");
            Map(m => m.City).Column("City");
            Map(m => m.Region).Column("Region");
            Map(m => m.PostalCode).Column("Postal_Code");
            Map(m => m.Country).Column("Country");
            Map(m => m.CountryCh).Column("Country_Ch");
            Map(m => m.Phone).Column("Phone");
            Map(m => m.ChinaHoursWeekly).Column("China_Hours_Weekly");
            Map(m => m.CMonths).Column("C_Months");
            Map(m => m.CYears).Column("C_Years");
            Map(m => m.OtherCountryName).Column("Other_Country_Name");
            Map(m => m.OtherHoursWeekly).Column("Other_Hours_Weekly");
            Map(m => m.TaiwanHoursWeekly).Column("Taiwan_Hours_Weekly");
            Map(m => m.OYears).Column("O_Years");
            Map(m => m.OMonths).Column("O_Months");
            Map(m => m.Years).Column("Years");
            Map(m => m.Months).Column("Months");
            Map(m => m.StudentMaterials).Column("Student_Materials");
            Map(m => m.SpeakChineseFreq).Column("Speak_Chinese_Freq");
            Map(m => m.CreDte).Column("Create_Date");
            Map(m => m.CreUsr).Column("Create_User");
            Map(m => m.UpdDte).Column("Update_Date");
            Map(m => m.UpdUsr).Column("Update_User");
            Map(m => m.FileId).Column("File_Id");

            Map(m => m.School).Column("School");
            Map(m => m.Grade).Column("Grade");
            Map(m => m.LanguangLocation).Column("Languang_Location");
            Map(m => m.TakeYCL).Column("Take_YCL");
            Map(m => m.TakeYCLWhen).Column("Take_YCL_When");
            Map(m => m.TakeYCLWhichLevel).Column("Take_YCL_Which_Level");
            Map(m => m.TakeYCLPassedOrNot).Column("Take_YCL_Passed_Or_Not");

            AutoMap();
        }
    }
}