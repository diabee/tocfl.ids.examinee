﻿using DapperExtensions.Mapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TOCFL.ISD.Dto.Entity.Testee;

namespace TOCFL.ISD.Dto.Map.Testee
{
    public class TesteeExamRelEntityMap : ClassMapper<TesteeExamRelEntity>
    {
        public TesteeExamRelEntityMap()
        {
            Table("Testee_Exam_Rel");
            Map(m => m.TesteeExamId).Column("Testee_Exam_Id").Key(KeyType.Identity);
            Map(m => m.TesteeId).Column("Testee_Id");
            Map(m => m.ExamId).Column("Exam_Id");
            Map(m => m.ExamResultId).Column("Exam_Result_Id");
            Map(m => m.No).Column("No");
            Map(m => m.AdmissionId).Column("eno");
            Map(m => m.Account).Column("euid");
            Map(m => m.Password).Column("epwd");
            Map(m => m.SignupDate).Column("Signup_Date");
            Map(m => m.CreDte).Column("Create_Date");
            Map(m => m.CreUsr).Column("Create_User");
            Map(m => m.UpdDte).Column("Update_Date");
            Map(m => m.UpdUsr).Column("Testee_Exam_Id");
            Map(m => m.Comment).Column("Comment");

            Map(m => m.FileId).Column("File_Id");
        }
    }
}





