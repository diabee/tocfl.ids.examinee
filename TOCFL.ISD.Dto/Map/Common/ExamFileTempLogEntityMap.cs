﻿using DapperExtensions.Mapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TOCFL.ISD.Dto.Entity.Common;

namespace TOCFL.ISD.Dto.Map.Common
{
    public class ExamFileTempLogEntityMap : ClassMapper<ExamFileTempLogEntity>
    {
        public ExamFileTempLogEntityMap()
        {

            Table("Exam_Upload_Temp_Log");
            Map(m => m.FileId).Column("File_Id").Key(KeyType.Identity);
            Map(m => m.ExamId).Column("Exam_Id");
            Map(m => m.FileName).Column("File_Name");
            Map(m => m.FileNameFix).Column("File_Name_Fix");
            Map(m => m.FilePath).Column("File_Path");
            Map(m => m.FilePathFix).Column("File_Path_Fix");
            Map(m => m.FileSize).Column("File_Size");
            Map(m => m.ParserFlag).Column("Parser_Flag");
            Map(m => m.CreDte).Column("Create_Date");
            Map(m => m.CreUsr).Column("Create_User");
            AutoMap();
        }
    }
}