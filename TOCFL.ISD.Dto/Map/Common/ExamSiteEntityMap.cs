﻿using DapperExtensions.Mapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TOCFL.ISD.Dto.Entity.Common;

namespace TOCFL.ISD.Dto.Map.Common
{
    public class ExamSiteEntityMap:ClassMapper<ExamSiteEntity>
    {
        public ExamSiteEntityMap()
        {
            Table("Exam_Site");
            Map(m => m.ExamSiteId).Column("Exam_Site_Id").Key(KeyType.Identity);
            Map(m => m.Name).Column("Name");
            Map(m => m.CountryId).Column("Country_Id");
            AutoMap();
        }
    }
}