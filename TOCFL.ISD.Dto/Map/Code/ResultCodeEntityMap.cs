﻿using DapperExtensions.Mapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TOCFL.ISD.Dto.Entity.Code;

namespace TOCFL.ISD.Dto.Map.Code
{
    public class ResultCodeEntityMap:ClassMapper<ResultCodeEntity>
    {
        public ResultCodeEntityMap()
        {
            Table("Result_Code");
            Map(m => m.ResultCodeId).Column("Result_Code_Id");
            Map(m => m.Name).Column("Name");
            Map(m => m.Code).Column("Code");
            Map(m => m.EnglishName).Column("EN_Name");
            Map(m => m.AbsentComment).Column("Absent_Comment");

        }
    }
}