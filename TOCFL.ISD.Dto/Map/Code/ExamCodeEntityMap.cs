﻿using DapperExtensions.Mapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TOCFL.ISD.Dto.Entity.Code;

namespace TOCFL.ISD.Dto.Map.Code
{
    public class ExamCodeEntityMap:ClassMapper<ExamCodeEntity>
    {
        public ExamCodeEntityMap()
        {
            Table("Exam_Code");
            Map(m => m.ExamCodeId).Column("Exam_Code_Id");
            Map(m => m.Name).Column("Name");
            Map(m => m.Code).Column("Code");
            Map(m => m.Category).Column("Category");
        }
    }
}