﻿using DapperExtensions.Mapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TOCFL.ISD.Dto.Entity.Exam;

namespace TOCFL.ISD.Dto.Map.Exam
{
    public class ExamEntityMap:ClassMapper<ExamEntity>
    {
        public ExamEntityMap()
        {

            Table("Exam");
            Map(m => m.ExamId).Column("Exam_Id").Key(KeyType.Identity);
            Map(m => m.ExamDate).Column("Exam_Date");
            Map(m => m.ToExamDate).Column("ToExam_Date");
            Map(m => m.FromExamDate).Column("FromExam_Date");
            Map(m => m.ExamYear).Column("Exam_Year");
            Map(m => m.ExamDefineId).Column("Exam_Define_Id");
            Map(m => m.TesteeCount).Column("Testee_Count");
            Map(m => m.SiteId).Column("Site_Id");
            Map(m => m.TestType).Column("Test_Type");
            Map(m => m.ExamOrganizer).Column("Exam_Organizer");
            Map(m => m.TestCategory).Column("Test_Category");
            Map(m => m.UploadFlag).Column("Upload_Flag");
            Map(m => m.ExamType).Column("Exam_Type");
            Map(m => m.CreDte).Column("Create_Date");
            Map(m => m.CreUsr).Column("Create_User");
            Map(m => m.UpdDte).Column("Update_Date");
            Map(m => m.UpdUsr).Column("Update_User");

            Map(m => m.PassCount).Column("Pass_Count");
            Map(m => m.NoPassCount).Column("NoPass_Count");
            Map(m => m.FileId).Column("File_Id");
            AutoMap();
        }
    }
}