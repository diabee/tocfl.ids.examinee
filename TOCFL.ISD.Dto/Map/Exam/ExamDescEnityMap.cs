﻿using DapperExtensions.Mapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TOCFL.ISD.Dto.Entity.Testee.Exam;

namespace TOCFL.ISD.Dto.Map.Exam
{
    public class ExamDescEnityMap:ClassMapper<ExamDescEntity>
    {
        public ExamDescEnityMap()
        {
            Table("Exam_Desc");
            Map(m => m.ExamDescId).Column("Exam_Desc_Id").Key(KeyType.Identity);
            Map(m => m.Descript).Column("Descript");
        }
    }
}