﻿using DapperExtensions.Mapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TOCFL.ISD.Dto.Entity.Exam;

namespace TOCFL.ISD.Dto.Map.Exam
{
    public class ExamResultEntityMap : ClassMapper<ExamResultEntity>
    {
        public ExamResultEntityMap()
        {
            Table("Exam_Result");
            Map(m => m.ExamResultId).Column("Exam_Result_Id");
            Map(m => m.ExamSiteId).Column("Exam_Site_Id");
            Map(m => m.ExamScore).Column("Exam_Score");
            Map(m => m.TestLevel).Column("Test_Level");
            Map(m => m.TestLevelId).Column("Test_Level_Id");
            Map(m => m.TestRoom).Column("Test_Room");
            Map(m => m.Absent).Column("absent");
            Map(m => m.Comment).Column("comment");
            Map(m => m.LScore).Column("a1");
            Map(m => m.LLevel).Column("a2");
            Map(m => m.LCEFRLevel).Column("ls_cefr");
            Map(m => m.LACTFLLevel).Column("ls_actfl");
            Map(m => m.RScore).Column("b1");
            Map(m => m.RLevel).Column("b2");
            Map(m => m.RCEFRLevel).Column("rd_cefr");
            Map(m => m.RACTFLlevel).Column("rd_actfl");
            Map(m => m.LRLevel).Column("c1");
            Map(m => m.LRLevelEN).Column("c2");
            Map(m => m.LRLevelTW).Column("c3");
            
            Map(m => m.CreDte).Column("Create_Date");
            Map(m => m.CreUsr).Column("Create_User");
            Map(m => m.UpdDte).Column("fdate");
            Map(m => m.UpdUsr).Column("lastuser");
            Map(m => m.FileId).Column("File_Id");
            Map(m => m.CertPrintNo).Column("Cert_Print_No");
            Map(m => m.CertRegNo).Column("Cert_Reg_No");
            Map(m => m.CharacterVersion).Column("Character_Version");
            //CCCC
            Map(m => m.TotalScore).Column("Total_Score");
            Map(m => m.PassStatus).Column("Pass_Status");
            Map(m => m.CEFRLevel).Column("CEFR_Level");
            Map(m => m.L1Ability).Column("L1_Ability");
            Map(m => m.L2Ability).Column("L2_Ability");
            Map(m => m.L3Ability).Column("L3_Ability");
            Map(m => m.R1Ability).Column("R1_Ability");
            Map(m => m.R2Ability).Column("R2_Ability");
            Map(m => m.R3Ability).Column("R3_Ability");
            AutoMap();
        }
    }
}