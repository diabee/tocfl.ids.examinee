﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TOCFL.ISD.Dto.Entity
{

    public class BaseEntity
    {
        public string CreUsr { get; set; }
        public DateTime CreDte { get; set; }
        public string UpdUsr { get; set; }
        public DateTime UpdDte { get; set; }
    }
}