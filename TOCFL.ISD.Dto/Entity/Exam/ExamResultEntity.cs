﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TOCFL.ISD.Dto.Entity.Exam
{
    public class ExamResultEntity:BaseEntity
    {
        public int ExamResultId { get; set; }
        public int ExamSiteId { get; set; }
        public int ExamScore { get; set; }
        public string TestLevel { get; set; }

        public string TestLevelId { get; set; }
        public string Absent { get; set; }
        public string Comment { get; set; }
        public int LScore { get; set; }
        public string LLevel { get; set; }
        public string LCEFRLevel { get; set; }
        public string LACTFLLevel { get; set; }
        public int RScore { get; set; }
        public string RLevel { get; set; }
        public string RCEFRLevel { get; set; }
        public string RACTFLlevel { get; set; }
        public string LRLevel { get; set; }
        public string LRLevelTW { get; set; }
        public string LRLevelEN { get; set; }
        
        public string TestRoom { get; set; }
        public int FileId { get;  set; }
        //CCCC
        public int TotalScore { get; set; }
        public string PassStatus { get;  set; }
        public string CEFRLevel { get;  set; }
        public string L1Ability { get; set; }
        public string L2Ability { get; set; }
        public string L3Ability { get; set; }
        public string R1Ability { get; set; }
        public string R2Ability { get; set; }
        public string R3Ability { get; set; }


        public int CertPrintNo { get; set; }

        public int CertRegNo { get; set; }
        public string CharacterVersion { get; set; }
    }
}