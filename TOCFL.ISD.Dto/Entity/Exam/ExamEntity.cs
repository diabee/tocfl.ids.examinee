﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TOCFL.ISD.Dto.Entity.Exam
{
    public class ExamEntity:BaseEntity
    {

        public int ExamId { get; set; }

        //早期的
        public string ExamDate { get; set; }
        //新格式
        public string FromExamDate { get; set; }
        public string ToExamDate { get; set; }

        public string ExamYear { get; set; }

        public string ExamDefineId { get; set; }

        public int TesteeCount { get; set; }

        public int SiteId { get; set; }

        public string ExamOrganizer { get; set; }
        public string TestType { get; set; }
        public int FileId { get; set; }
        public int NoPassCount { get;  set; }
        public int PassCount { get; set; }
        public string TestCategory { get;  set; }
        public string ExamType { get; set; }
        public string UploadFlag { get; set; }


    }
}