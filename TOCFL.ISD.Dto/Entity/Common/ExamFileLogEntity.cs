﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TOCFL.ISD.Dto.Entity.Common
{
    public class ExamFileLogEntity 
    {

        public int FileId { get; set; }

        public string FileName { get; set; }
        

        public string FileSize { get; set; }

        public string FilePath { get; set; }

        public string FilePathFix { get; set; }

        public string ParserFlag { get; set; }

        public string CreUsr { get; set; }
        public DateTime CreDte { get; set; }
        public string FileNameFix { get; set; }
    }
}