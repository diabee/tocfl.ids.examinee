﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TOCFL.ISD.Dto.Entity.Common
{
    public class ExamSiteEntity
    {
        public int ExamSiteId { get; set; }

        public string Name { get; set; }

        public string CountryId { get; set; }
    }
}