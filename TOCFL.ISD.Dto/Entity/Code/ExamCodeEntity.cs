﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TOCFL.ISD.Dto.Entity.Code
{
    public class ExamCodeEntity
    {

        public int ExamCodeId { get; set; }

        public string Name { get; set; }

        public string Code { get; set; }

        public string Category { get; set; }
    }
}