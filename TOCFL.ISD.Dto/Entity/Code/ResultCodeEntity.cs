﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TOCFL.ISD.Dto.Entity.Code
{
    public class ResultCodeEntity
    {
        public int ResultCodeId { get; set; }

        public string Name { get; set; }

        public string Code { get; set; }

        public string EnglishName { get; set; }

        public string AbsentComment { get; set; }
    }
}