﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TOCFL.ISD.Dto.Entity.Testee
{
    public class TesteeEntity : BaseEntity
    {

        public int TesteeId { get; set; }

        public string EnglishName { get; set; }

        public string ChineseName { get; set; }

        public string LocalIdNo { get; set; }

        public string PassportId { get; set; }

        public DateTime? BirthDate { get; set; }

        public int Age { get; set; }

        public string Gender { get; set; }

        public string Address { get; set; }

        public string City { get; set; }

        public string Region { get; set; }

        public string PostalCode { get; set; }

        public string Country { get; set; }

        public string CountryCh { get; set; }
        public string Phone { get; set; }
        //---------------------以下
        public string Email { get; set; }

        public string TaiwanHoursWeekly { get; set; }
        public string Years { get; set; }
        public string Months { get; set; }
        public string ChinaHoursWeekly { get; set; }
        public string CYears { get; set; }
        public string CMonths { get; set; }
        public string OtherCountryName { get; set; }
        public string OtherHoursWeekly { get; set; }
        public string OYears { get; set; }
        public string OMonths { get; set; }
        public string SpeakChineseFreq { get; set; }
        public string StudentMaterials { get; set; }
        public int FileId { get; set; }
        //CCCC
        public string School { get;  set; }
        public string Grade { get;  set; }
        public string LanguangLocation { get;  set; }
        public string TakeYCL { get;  set; }
        public string TakeYCLWhen { get;  set; }
        public string TakeYCLWhichLevel { get;  set; }
        public string TakeYCLPassedOrNot { get;  set; }
    }
}