﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TOCFL.ISD.Sys.Common
{
    /// <summary>
    /// Http ContentType
    /// </summary>
    public enum ContentTypeEnum
    {
        Xml,
        Json,
        Text,
        x_www_form_urlencoded
    }
    /// <summary>
    /// 計錄Log時的要指定的狀態
    /// </summary>
    public enum LogCategoryEnum
    {
        Information,
        Error,
        Warning
    }
    /// <summary>
    /// DataUrl的資料型態
    /// </summary>
    public enum DataUrlTypeEnum
    {
        jpg,
        gif,
        png,
        htm,
        css
    }

    public enum StateKey
    {
        ApId,
        UserCulture,
        UserOrg,
        UserRole,
        UserProfile,
        RolFunRightCode,
        UserAgent,
        UserAp,
        SeqM,
        ITSMAPI,
        UploadExt,
        UploadSize,
        Logon
    }
}
