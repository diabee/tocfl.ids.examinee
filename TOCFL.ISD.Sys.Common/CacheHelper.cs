﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Enyim.Caching;
using Enyim.Caching.Memcached;
using System.Web;


namespace TOCFL.ISD.Sys.Common
{
    public class CacheHelper
    {
        #region 宣告

        private static MemcachedClient client;

        private static MemcachedClient Client
        {
            get
            {
                if (client == null)
                {
                    client = new MemcachedClient();
                }

                return client;
            }
        }

        #endregion 宣告

        #region 儲存cache

        /// <summary>
        /// 儲存cache
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool Set(TOCFL.ISD.Sys.Common.StateKey Key, object Value)
        {
            return Client.Store(StoreMode.Set, Key.ToString().ToString(), Value, new TimeSpan(20, 0, 0));
        }

        /// <summary>
        /// 儲存cache
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool Set(string Key, object Value)
        {
            return Client.Store(StoreMode.Set, Key, Value, new TimeSpan(20, 0, 0));
        }

        /// <summary>
        /// 儲存cache,包含到期時間
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <param name="ValidFor"></param>
        /// <returns></returns>
        public static bool Set(TOCFL.ISD.Sys.Common.StateKey  Key, object Value, TimeSpan ValidFor)
        {
            return Client.Store(StoreMode.Set, Key.ToString(), Value, ValidFor);
        }

        /// <summary>
        /// 儲存cache,包含到期時間
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <param name="ValidFor"></param>
        /// <returns></returns>
        public static bool Set(string Key, object Value, TimeSpan ValidFor)
        {
            return Client.Store(StoreMode.Set, Key, Value, ValidFor);
        }

        /// <summary>
        /// 儲存cache,包含到期時間
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <param name="ExpiresAt"></param>
        /// <returns></returns>
        public static bool Set(TOCFL.ISD.Sys.Common.StateKey  Key, object Value, DateTime ExpiresAt)
        {
            return Client.Store(StoreMode.Set, Key.ToString(), Value, ExpiresAt);
        }

        /// <summary>
        /// 儲存cache,包含到期時間
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <param name="ExpiresAt"></param>
        /// <returns></returns>
        public static bool Set(string Key, object Value, DateTime ExpiresAt)
        {
            return Client.Store(StoreMode.Set, Key, Value, ExpiresAt);
        }

        #endregion 儲存cache

        #region 取得cache

        /// <summary>
        /// 取得單筆cache
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static object Get(TOCFL.ISD.Sys.Common.StateKey  Key)
        {
            return Client.Get(Key.ToString());
        }

        /// <summary>
        /// 取得單筆cache
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static object Get(string Key)
        {
            object obj;
            bool bExists = Client.TryGet(Key, out obj);
            if (bExists)
                return obj;
            else
                return null;
            //return Client.Get(Key);
        }

        /// <summary>
        /// 取得多筆cache
        /// </summary>
        /// <param name="keys"></param>
        /// <returns></returns>
        public static IDictionary<string, object> Get(List<string> Keys)
        {
            return Client.Get(Keys);
        }

        #endregion 取得cache

        #region 刪除指定cache

        /// <summary>
        /// 刪除指定cache
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static bool Remove(TOCFL.ISD.Sys.Common.StateKey  Key)
        {
            return Client.Remove(Key.ToString());
        }

        /// <summary>
        /// 刪除指定cache
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static bool Remove(string Key)
        {
            return Client.Remove(Key);
        }

        #endregion 刪除指定cache

        #region 清除所有cache

        /// <summary>
        /// 清除所有cache
        /// </summary>
        public static void ClearAll()
        {
            Client.FlushAll();
        }

        #endregion 清除所有cache

        #region 遞增

        /// <summary>
        /// 遞增
        /// </summary>
        /// <param name="Key"></param>
        /// <param name="DefaultValue"></param>
        /// <param name="Delta"></param>
        /// <returns></returns>
        public static ulong Increment(TOCFL.ISD.Sys.Common.StateKey  Key, ulong DefaultValue, ulong Delta)
        {
            return Client.Increment(Key.ToString(), DefaultValue, Delta);
        }

        /// <summary>
        /// 遞增
        /// </summary>
        /// <param name="Key"></param>
        /// <param name="DefaultValue"></param>
        /// <param name="Delta"></param>
        /// <returns></returns>
        public static ulong Increment(string Key, ulong DefaultValue, ulong Delta)
        {
            return Client.Increment(Key, DefaultValue, Delta);
        }

        /// <summary>
        /// 遞增,包含到期時間
        /// </summary>
        /// <param name="Key"></param>
        /// <param name="DefaultValue"></param>
        /// <param name="Delta"></param>
        /// <param name="ExpireAt"></param>
        /// <returns></returns>
        public static ulong Increment(TOCFL.ISD.Sys.Common.StateKey  Key, ulong DefaultValue, ulong Delta, DateTime ExpireAt)
        {
            return Client.Increment(Key.ToString(), DefaultValue, Delta, ExpireAt);
        }

        /// <summary>
        /// 遞增,包含到期時間
        /// </summary>
        /// <param name="Key"></param>
        /// <param name="DefaultValue"></param>
        /// <param name="Delta"></param>
        /// <param name="ExpireAt"></param>
        /// <returns></returns>
        public static ulong Increment(string Key, ulong DefaultValue, ulong Delta, DateTime ExpireAt)
        {
            return Client.Increment(Key, DefaultValue, Delta, ExpireAt);
        }

        /// <summary>
        /// 遞增,包含到期時間
        /// </summary>
        /// <param name="Key"></param>
        /// <param name="DefaultValue"></param>
        /// <param name="Delta"></param>
        /// <param name="ValidFor"></param>
        /// <returns></returns>
        public static ulong Increment(TOCFL.ISD.Sys.Common.StateKey  Key, ulong DefaultValue, ulong Delta, TimeSpan ValidFor)
        {
            return Client.Increment(Key.ToString(), DefaultValue, Delta, ValidFor);
        }

        /// <summary>
        /// 遞增,包含到期時間
        /// </summary>
        /// <param name="Key"></param>
        /// <param name="DefaultValue"></param>
        /// <param name="Delta"></param>
        /// <param name="ValidFor"></param>
        /// <returns></returns>
        public static ulong Increment(string Key, ulong DefaultValue, ulong Delta, TimeSpan ValidFor)
        {
            return Client.Increment(Key, DefaultValue, Delta, ValidFor);
        }

        #endregion 遞增

        #region 遞減

        /// <summary>
        /// 遞減
        /// </summary>
        /// <param name="Key"></param>
        /// <param name="DefaultValue"></param>
        /// <param name="Delta"></param>
        /// <returns></returns>
        public static ulong Decrement(TOCFL.ISD.Sys.Common.StateKey  Key, ulong DefaultValue, ulong Delta)
        {
            return Client.Decrement(Key.ToString(), DefaultValue, Delta);
        }

        /// <summary>
        /// 遞減
        /// </summary>
        /// <param name="Key"></param>
        /// <param name="DefaultValue"></param>
        /// <param name="Delta"></param>
        /// <returns></returns>
        public static ulong Decrement(string Key, ulong DefaultValue, ulong Delta)
        {
            return Client.Decrement(Key, DefaultValue, Delta);
        }

        /// <summary>
        /// 遞減,包含到期時間
        /// </summary>
        /// <param name="Key"></param>
        /// <param name="DefaultValue"></param>
        /// <param name="Delta"></param>
        /// <param name="ExpireAt"></param>
        /// <returns></returns>
        public static ulong Decrement(TOCFL.ISD.Sys.Common.StateKey  Key, ulong DefaultValue, ulong Delta, DateTime ExpireAt)
        {
            return Client.Decrement(Key.ToString(), DefaultValue, Delta, ExpireAt);
        }

        /// <summary>
        /// 遞減,包含到期時間
        /// </summary>
        /// <param name="Key"></param>
        /// <param name="DefaultValue"></param>
        /// <param name="Delta"></param>
        /// <param name="ExpireAt"></param>
        /// <returns></returns>
        public static ulong Decrement(string Key, ulong DefaultValue, ulong Delta, DateTime ExpireAt)
        {
            return Client.Decrement(Key, DefaultValue, Delta, ExpireAt);
        }

        /// <summary>
        /// 遞減,包含到期時間
        /// </summary>
        /// <param name="Key"></param>
        /// <param name="DefaultValue"></param>
        /// <param name="Delta"></param>
        /// <param name="ValidFor"></param>
        /// <returns></returns>
        public static ulong Decrement(TOCFL.ISD.Sys.Common.StateKey  Key, ulong DefaultValue, ulong Delta, TimeSpan ValidFor)
        {
            return Client.Decrement(Key.ToString(), DefaultValue, Delta, ValidFor);
        }

        /// <summary>
        /// 遞減,包含到期時間
        /// </summary>
        /// <param name="Key"></param>
        /// <param name="DefaultValue"></param>
        /// <param name="Delta"></param>
        /// <param name="ValidFor"></param>
        /// <returns></returns>
        public static ulong Decrement(string Key, ulong DefaultValue, ulong Delta, TimeSpan ValidFor)
        {
            return Client.Decrement(Key, DefaultValue, Delta, ValidFor);
        }

        #endregion 遞減

        #region Session存取

        /// <summary>
        /// 儲存Session
        /// </summary>
        /// <param name="Key"></param>
        /// <param name="Value"></param>
        /// <returns></returns>
        public static bool SetSession(TOCFL.ISD.Sys.Common.StateKey  Key, object Value)
        {
            string NewKey = string.Format("{0}_{1}", Web.GetSessionId(), Key.ToString());
            return Set(NewKey, Value);
        }

        /// <summary>
        /// 儲存Session
        /// </summary>
        /// <param name="Key"></param>
        /// <param name="Value"></param>
        /// <returns></returns>
        public static bool SetSession(string Key, object Value)
        {
            string NewKey = string.Format("{0}_{1}", Web.GetSessionId(), Key);
            return Set(NewKey, Value);
        }

        /// <summary>
        /// 取得Session
        /// </summary>
        /// <param name="Key"></param>
        /// <returns></returns>
        public static object GetSession(TOCFL.ISD.Sys.Common.StateKey  Key)
        {
            string NewKey = string.Format("{0}_{1}", Web.GetSessionId(), Key.ToString());
            return Client.Get(NewKey);
        }

        /// <summary>
        /// 取得Session
        /// </summary>
        /// <param name="Key"></param>
        /// <returns></returns>
        public static object GetSession(string Key)
        {
            string NewKey = string.Format("{0}_{1}", Web.GetSessionId(), Key);
            return Client.Get(NewKey);
        }

        /// <summary>
        /// 刪除Session
        /// </summary>
        /// <param name="Key"></param>
        /// <returns></returns>
        public static object RemoveSession(TOCFL.ISD.Sys.Common.StateKey  Key)
        {
            string NewKey = string.Format("{0}_{1}", Web.GetSessionId(), Key.ToString());
            return Client.Remove(NewKey);
        }

        /// <summary>
        /// 刪除Session
        /// </summary>
        /// <param name="Key"></param>
        /// <returns></returns>
        public static object RemoveSession(string Key)
        {
            string NewKey = string.Format("{0}_{1}", Web.GetSessionId(), Key);
            return Client.Remove(NewKey);
        }

        #endregion Session存取
    }
}
