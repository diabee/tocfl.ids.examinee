﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
using System.IO;
using Microsoft.VisualBasic;

namespace TOCFL.ISD.Sys.Common
{
    public static partial class Conveter
    {
        /// <summary>
        /// timestamp 2 DateTime
        /// </summary>
        /// <param name="timeStamp">timeStamp String</param>
        /// <returns>dt</returns>
        public static DateTime Timestamp2Datetime(string timeStamp)
        {
            NumberStyles style = NumberStyles.Number;
            double tmpTimeStamp = double.Parse(timeStamp, style);
            // 百萬分之一秒的秒數轉 DateTime
            DateTime result = (new DateTime(1970, 1, 1, 0, 0, 0)).AddHours(8).AddMilliseconds(tmpTimeStamp);
            return result;
        }
        /// <summary>
        /// 日期轉換成短日期格式(yyyy/MM/dd)
        /// </summary>
        /// <param name="dateTime"></param>
        /// <returns></returns>
        public static string DateTime2ShortDate(System.DateTime dateTime)
        {
            try
            {
                return dateTime == System.DateTime.MinValue ? string.Empty :
                    dateTime.ToString("yyyy/MM/dd");
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }


                /// <summary>
        /// 轉換常用的各種日期時間格式字串
        /// </summary>
        /// <param name="vdtDate">欲轉換的日期時間，型別為 System.DateTime。</param>
        /// <param name="vintDateType">欲轉換成的日期字串格式。格式說明請參考 <see cref="ToDateString" /> 的 vintType 參數。</param>
        /// <param name="vintTimeType">欲轉換成的時間字串格式。格式說明請參考 <see cref="ToTimeString" /> 的 vintType 參數。</param>
        /// <returns>轉換後的日期時間格式字串</returns>
        public static string ToDateTimeString(DateTime vdtDate, int vintDateType, int vintTimeType)
        {
            if (Information.IsDate(vdtDate))
            {
                return ToDateTimeString(vdtDate.ToString("G"), vintDateType, vintTimeType);
            }
            else
            {
                return "";
            }

        }

        /// <summary>
        /// 轉換常用的各種日期時間格式字串
        /// </summary>
        /// <param name="vstrDateTime">欲轉換的日期時間字串。只要第一個空白前符合 <see cref="ToDateString"/> 的輸入日期格式，而第二個空白符合 ConvertTimeString 的輸入時間格式即可。例如 "01/30/2004 PM 12:32:58"。</param>
        /// <param name="vintDateType">欲轉換成的日期字串格式。格式說明請參考 <see cref="ToDateString"/> 的 vintType 參數。</param>
        /// <param name="vintTimeType">欲轉換成的時間字串格式。格式說明請參考 <see cref="ToTimeString"/> 的 vintType 參數。</param>
        /// <returns>轉換後的日期時間格式字串</returns>
        public static string ToDateTimeString(string vstrDateTime, int vintDateType, int vintTimeType)
        {
            if (Information.IsDate(vstrDateTime))
            {
                if (vstrDateTime.IndexOf(" ") >= 0)
                {
                    string strDate = vstrDateTime.Substring(0, vstrDateTime.IndexOf(" "));
                    string strTime = vstrDateTime.Substring(vstrDateTime.IndexOf(" ") + 1);

                    strDate = ToDateString(strDate, vintDateType);
                    strTime = ToTimeString(strTime, vintTimeType);

                    return (strDate + " " + strTime).Trim();
                }
                else
                {
                    ArgumentException ex = new ArgumentException(string.Format("型態[{0}]無法轉換成日期，僅接受DateTime與String", vstrDateTime));
                    throw ex;
                }
            }
            else
            {
                ArgumentException ex = new ArgumentException(string.Format("型態[{0}]無法轉換成日期，僅接受DateTime與String", vstrDateTime));
                throw ex;
            }
        }

        /// <summary>
        /// 轉換常用的各種時間格式字串
        /// </summary>
        /// <param name="vdtTime">欲轉換的時間，型別為 System.DateTime。</param>
        /// <param name="vintType">欲轉換成的字串格式</param>
        /// <returns>轉換後的時間格式字串。</returns>
        /// <remarks>
        /// vintType所代表的格式如下:
        /// <list type="talbe">
        /// <item><term>-1</term><description>不顯示，回傳空字串 ""</description></item>
        /// <item><term>0</term><description>19:08:54</description></item>
        /// <item><term>1</term><description>19:08</description></item>
        /// <item><term>2</term><description>PM 07:08:54</description></item>
        /// <item><term>3</term><description>PM 07:08</description></item>
        /// <item><term>4</term><description>07:08 PM</description></item>
        /// <item><term>5</term><description>07:08:54 PM</description></item>
        /// <item><term>6</term><description>下午 07:08</description></item>
        /// <item><term>7</term><description>下午 07:08:54</description></item>
        /// <item><term>8</term><description>07:08 下午</description></item>
        /// <item><term>9</term><description>07:08:54 下午</description></item>
        /// </list>
        /// <B>註 : 上午(AM)的 12 點是 24 小時制的 00 點，下午(PM) 12 點是 24 小時制的 12 點。</B>
        /// </remarks>
        public static string ToTimeString(DateTime vdtTime, int vintType)
        {

            return ToTimeString(vdtTime.ToString("T"), vintType);
        }

        /// <summary>
        /// 轉換常用的各種時間格式字串
        /// </summary>
        /// <param name="vstrTime">欲轉換的時間字串。可傳入如下回傳值所示的 0 ~ 9 格式字串。</param>
        /// <param name="vintType">欲轉換成的字串格式</param>
        /// <returns>轉換後的時間格式字串</returns>
        /// <remarks>
        /// vintType所代表的格式如下:
        /// <list type="talbe">
        /// <item><term>-1</term><description>不顯示，回傳空字串 ""</description></item>
        /// <item><term>0</term><description>19:08:54</description></item>
        /// <item><term>1</term><description>19:08</description></item>
        /// <item><term>2</term><description>PM 07:08:54</description></item>
        /// <item><term>3</term><description>PM 07:08</description></item>
        /// <item><term>4</term><description>07:08 PM</description></item>
        /// <item><term>5</term><description>07:08:54 PM</description></item>
        /// <item><term>6</term><description>下午 07:08</description></item>
        /// <item><term>7</term><description>下午 07:08:54</description></item>
        /// <item><term>8</term><description>07:08 下午</description></item>
        /// <item><term>9</term><description>07:08:54 下午</description></item>
        /// </list>
        /// <B>註 : 上午(AM)的 12 點是 24 小時制的 00 點，下午(PM) 12 點是 24 小時制的 12 點。</B>
        /// </remarks>
        public static string ToTimeString(string vstrTime, int vintType)
        {
            if (vintType == -1 || vstrTime.Length == 0)
            {
                return "";
            }

            string strRet = "";

            bool blnAM = false;
            string strTime = null;

            vstrTime = vstrTime.ToUpper();

            if (vstrTime.IndexOf("AM") >= 0)
            {
                strTime = vstrTime.Replace("AM", "").Trim();
                blnAM = true;
            }
            else if (vstrTime.IndexOf("上午") >= 0)
            {
                strTime = vstrTime.Replace("上午", "").Trim();
                blnAM = true;
            }
            else if (vstrTime.IndexOf("PM") >= 0)
            {
                strTime = vstrTime.Replace("PM", "").Trim();
                blnAM = false;
            }
            else if (vstrTime.IndexOf("下午") >= 0)
            {
                strTime = vstrTime.Replace("下午", "").Trim();
                blnAM = false;
            }
            else
            {
                //24 小時制
                string strHourTmp = vstrTime.Trim().Substring(0, 2);

                if (int.Parse(strHourTmp) > 11)
                {
                    //PM
                    blnAM = false;
                    if (strHourTmp != "12")
                    {
                        strTime = int.Parse(strHourTmp) - 12 + vstrTime.Trim().Substring(2);
                        if (strTime.Length == 7)
                        {
                            strTime = "0" + strTime;
                        }
                    }
                    else
                    {
                        strTime = vstrTime;

                    }
                }
                else
                {
                    //AM
                    blnAM = true;
                    if (strHourTmp == "00")
                    {
                        strTime = "12" + vstrTime.TrimStart().Substring(2);
                    }
                    else
                    {
                        strTime = vstrTime;
                    }
                }
            }



            string[] aryToken = strTime.Split(new char[] { ':' });
            string strHour = aryToken[0];
            string strMinute = aryToken[1];
            string strSecond = null;
            if (aryToken.Length == 3)
            {
                strSecond = aryToken[2];
            }
            else
            {
                strSecond = "00";
            }


            switch (vintType)
            {
                case 0:
                    if (blnAM)
                    {
                        if (strHour == "12")
                        {
                            strRet = "00" + ":" + strMinute + ":" + strSecond;
                        }
                        else
                        {
                            strRet = strTime;
                        }
                    }
                    else
                    {
                        if (strHour == "12")
                        {
                            strRet = strTime;
                        }
                        else
                        {
                            strRet = int.Parse(strHour) + 12 + ":" + strMinute + ":" + strSecond;
                        }
                    }


                    break;
                case 1:
                    if (blnAM)
                    {
                        if (strHour == "12")
                        {
                            strRet = "00" + ":" + strMinute;
                        }
                        else
                        {
                            strRet = strHour + ":" + strMinute;
                        }
                    }
                    else
                    {
                        if (strHour == "12")
                        {
                            strRet = strHour + ":" + strMinute;
                        }
                        else
                        {
                            strRet = int.Parse(strHour) + 12 + ":" + strMinute;
                        }
                    }


                    break;
                case 2:
                    if (blnAM)
                    {
                        strRet = "AM " + strTime;
                    }
                    else
                    {
                        strRet = "PM " + strTime;
                    }


                    break;
                case 3:
                    if (blnAM)
                    {
                        strRet = "AM " + strHour + ":" + strMinute;
                    }
                    else
                    {
                        strRet = "PM " + strHour + ":" + strMinute;
                    }


                    break;
                case 4:
                    if (blnAM)
                    {
                        strRet = strHour + ":" + strMinute + " AM";
                    }
                    else
                    {
                        strRet = strHour + ":" + strMinute + " PM";
                    }


                    break;
                case 5:
                    if (blnAM)
                    {
                        strRet = strTime + " AM";
                    }
                    else
                    {
                        strRet = strTime + " PM";
                    }


                    break;
                case 6:
                    if (blnAM)
                    {
                        strRet = "上午 " + strHour + ":" + strMinute;
                    }
                    else
                    {
                        strRet = "下午 " + strHour + ":" + strMinute;
                    }


                    break;
                case 7:
                    if (blnAM)
                    {
                        strRet = "上午 " + strTime;
                    }
                    else
                    {
                        strRet = "下午 " + strTime;
                    }


                    break;
                case 8:
                    if (blnAM)
                    {
                        strRet = strHour + ":" + strMinute + " 上午";
                    }
                    else
                    {
                        strRet = strHour + ":" + strMinute + " 下午";
                    }


                    break;
                case 9:
                    if (blnAM)
                    {
                        strRet = strTime + " 上午";
                    }
                    else
                    {
                        strRet = strTime + " 下午";
                    }

                    break;
            }


            return strRet;
        }

        /// <summary>
        /// 轉換常用的各種日期格式字串轉換成常用的時間格式字串
        /// </summary>
        /// <param name="vdtDate">欲轉換的日期，型別為 System.DateTime。</param>
        /// <param name="vintType">欲轉換成的字串格式</param>
        /// <returns>轉換後的日期格式字串。若是民國前的日期，數字類的年份會以負號表示，例如 "-91/02/20"。
        /// 國字類的年份則以多加 "前" 來表示，例如 "民國前九十一年二月二十日"</returns>
        /// <remarks>
        /// vintType所代表的格式如下:
        /// <list type="talbe">
        /// <item><term>-1</term><description>不顯示，回傳空字串 ""</description></item>
        /// <item><term>0</term><description>"2002/02/20"</description></item>
        /// <item><term>1</term><description>"91/02/20"</description></item>
        /// <item><term>2</term><description>"091/02/20" (以三位數表示民國年，若在民國 100 年前則補 0 )</description></item>
        /// <item><term>3</term><description>"2002年02月20日"</description></item>
        /// <item><term>4</term><description>"91年02月20日"</description></item>
        /// <item><term>5</term><description>"九十一年二月二十日"</description></item>
        /// <item><term>6</term><description>"民國九十一年二月二十日"</description></item>
        /// <item><term>7</term><description>"中華民國九十一年二月二十日"</description></item>
        /// <item><term>8</term><description>"2002年02月20日星期三"</description></item>
        /// <item><term>9</term><description>"91年02月20日星期三"</description></item>
        /// <item><term>10</term><description>"九十一年二月二十日星期三"</description></item>
        /// <item><term>11</term><description>"民國九十一年二月二十日星期三"</description></item>
        /// <item><term>12</term><description>"中華民國九十一年二月二十日星期三"</description></item>
        /// <item><term>13</term><description>"20020220"</description></item>
        /// <item><term>14</term><description>"910220"</description></item>
        /// <item><term>15</term><description>"0910220"</description></item>
        /// <item><term>16</term><description>"星期三"</description></item>
        /// <item><term>17</term><description>" 910220" (以三位數表示民國年，若在民國 100 年前則補一個空白)</description></item>
        /// <item><term>18</term><description>" 91/02/20" (以三位數表示民國年，若在民國 100 年前則補一個空白)</description></item>
        /// <item><term>19</term><description>"02/20/2002"</description></item>
        /// </list>
        /// </remarks>
        public static string ToDateString(DateTime vdtDate, int vintType)
        {

            return ToDateString(vdtDate.Year + "/" + vdtDate.Month + "/" + vdtDate.Day, vintType);
        }

        /// <summary>
        /// 轉換常用的各種日期格式字串轉換成常用的時間格式字串
        /// </summary>
        /// <param name="vstrDate">
        /// 欲轉換的日期字串，須依照年、月、日的順序。其中年如果為4個字元時代表西元年，1~3個字元則時代表民國年。例如: 20020220,910220,2002/2/20,91/2/20,2002-02-20,91-2-20,2002.02.20,91.02.20
        /// 若是有分隔符號且為西元年，也可將西元年放在最後，例如 2/20/2002,2-20-2002,2.20.2002另外若要表示民國前的年份可多加負號，例如 "-91/02/20"，但不接受以負號表示西元前的年份。
        /// </param>
        /// <param name="vintType">欲轉換成的字串格式</param>
        /// <returns>轉換後的日期格式字串。若是民國前的日期，數字類的年份會以負號表示，例如 "-91/02/20"。
        /// 國字類的年份則以多加 "前" 來表示，例如 "民國前九十一年二月二十日"</returns>
        /// <remarks>
        /// vintType所代表的格式如下:
        /// <list type="talbe">
        /// <item><term>-1</term><description>不顯示，回傳空字串 ""</description></item>
        /// <item><term>0</term><description>"2002/02/20"</description></item>
        /// <item><term>1</term><description>"91/02/20"</description></item>
        /// <item><term>2</term><description>"091/02/20" (以三位數表示民國年，若在民國 100 年前則補 0 )</description></item>
        /// <item><term>3</term><description>"2002年02月20日"</description></item>
        /// <item><term>4</term><description>"91年02月20日"</description></item>
        /// <item><term>5</term><description>"九十一年二月二十日"</description></item>
        /// <item><term>6</term><description>"民國九十一年二月二十日"</description></item>
        /// <item><term>7</term><description>"中華民國九十一年二月二十日"</description></item>
        /// <item><term>8</term><description>"2002年02月20日星期三"</description></item>
        /// <item><term>9</term><description>"91年02月20日星期三"</description></item>
        /// <item><term>10</term><description>"九十一年二月二十日星期三"</description></item>
        /// <item><term>11</term><description>"民國九十一年二月二十日星期三"</description></item>
        /// <item><term>12</term><description>"中華民國九十一年二月二十日星期三"</description></item>
        /// <item><term>13</term><description>"20020220"</description></item>
        /// <item><term>14</term><description>"910220"</description></item>
        /// <item><term>15</term><description>"0910220"</description></item>
        /// <item><term>16</term><description>"星期三"</description></item>
        /// <item><term>17</term><description>" 910220" (以三位數表示民國年，若在民國 100 年前則補一個空白)</description></item>
        /// <item><term>18</term><description>" 91/02/20" (以三位數表示民國年，若在民國 100 年前則補一個空白)</description></item>
        /// <item><term>19</term><description>"02/20/2002"</description></item>
        /// </list>
        /// </remarks>
        public static string ToDateString(string vstrDate, int vintType)
        {
            try
            {
                if (vintType == -1 || vstrDate.Length == 0)
                {
                    return "";
                }

                string strRet = null;
                string strDay = null;
                string strMonth = null;
                string strYear = null;
                string strCYear = null;
                string strDate = "";

                string strWeekName = "";
                DateTime objDateTime = default(DateTime);
                int i = 0;

                bool blnMinusCYear = false;

                //民國前的年份
                if (vstrDate.Substring(0, 1) == "-")
                {
                    blnMinusCYear = true;
                    //如果年是負的，先把負號拿掉
                    vstrDate = vstrDate.Substring(1, vstrDate.Length - 1);
                }


                //先轉成 yyyy/mm/dd              
                if (Information.IsDate(vstrDate) == false)
                {
                    //1. 可能為沒有分隔符號的日期格式（中文的AP通常允許，但是用 IsDate 來判斷會錯誤）
                    if (vstrDate.IndexOf("/") == -1 & vstrDate.IndexOf("-") == -1 & vstrDate.IndexOf(".") == -1)
                    {

                        for (i = 0; i <= vstrDate.Length - 1; i++)
                        {
                            if ((int)char.Parse(vstrDate.Substring(i, 1)) >= 48 || (int)char.Parse(vstrDate.Substring(i, 1)) <= 57)
                            {
                                //若為數字則加入，剔除非數字的字元
                                strDate = strDate + vstrDate.Substring(i, 1);
                            }

                        }

                        if (strDate.Length < 5 | strDate.Length > 8)
                        {

                            //strRet = "日期不合法"
                            //strDate & "為不支援的日期格式"
                            //throw new ArgumentException(strDate + GSS.Stirrup.NBase20.Resources.Message.NBase20_MSG._70005);
                        }
                        else
                        {
                            // 8 為西元
                            if (strDate.Length < 8)
                            {
                                //民國曆
                                if (!blnMinusCYear)
                                {
                                    strYear = Strings.Trim((int.Parse(Strings.Mid(strDate, 1, 2 + (Strings.Len(strDate) % 3))) + 1911).ToString());
                                    //fix in 1.0.1012.0
                                    strYear = (int.Parse(strDate.Substring(0, strDate.Length - 4)) + 1911).ToString();
                                }
                                else
                                {
                                    strYear = Strings.Trim((1911 - int.Parse(Strings.Mid(strDate, 1, 2 + (Strings.Len(strDate) % 3))) + 1).ToString());
                                }


                                strMonth = Strings.Mid(strDate, 3 + (Strings.Len(strDate) % 3), 2);
                                strDay = Strings.Right(strDate, 2);
                            }
                            else
                            {
                                //西元曆
                                strYear = Strings.Mid(strDate, 1, 4);
                                //西元年先不考慮西元"前"
                                strMonth = Strings.Mid(strDate, 5, 2);
                                strDay = Strings.Mid(strDate, 7, 2);
                            }
                            vstrDate = strYear + "/" + strMonth + "/" + strDay;
                        }
                    }
                }
                else
                {
                    vstrDate = string.Format("{0:yyyy/MM/dd}", vstrDate);
                }
                //到此已經轉換日期為標準模式 "1997/05/06"

                //2. 將有分隔符號而西元年在最右邊的格式轉成西元年在左邊 (mm/dd/yyyy, mm-dd-yyyy, mm.dd.yyyy)
                string chrSplitter = "";
                bool blnHit = false;
                string[] aryToken = null;
                if (vstrDate.IndexOf("/") > -1)
                {
                    chrSplitter = "/";
                    blnHit = true;
                }
                else if (vstrDate.IndexOf("-") > -1)
                {
                    chrSplitter = "-";
                    blnHit = true;
                }
                else if (vstrDate.IndexOf(".") > -1)
                {
                    chrSplitter = ".";
                    blnHit = true;
                }

                if (blnHit)
                {
                    aryToken = vstrDate.Split(char.Parse(chrSplitter));
                    if (aryToken.Length == 3)
                    {
                        //確定拿到的是日期格式
                        if (aryToken[2].Length == 4)
                        {
                            //將西元年放到最前面
                            vstrDate = aryToken[2] + chrSplitter + aryToken[0] + chrSplitter + aryToken[1];
                        }

                    }
                }

                //3.將以 . - 分隔符號全轉為 /
                vstrDate = vstrDate.Replace(".", "/");
                vstrDate = vstrDate.Replace("-", "/");

                strYear = Strings.Mid(vstrDate, 1, Strings.InStr(vstrDate, "/", CompareMethod.Text) - 1);
                string strTmp = strYear;

                if (Strings.Len(strYear) < 4)
                {
                    //輸入為民國年
                    strCYear = strYear;
                    if (!blnMinusCYear)
                    {
                        strYear = (int.Parse(strCYear) + 1911).ToString();
                    }
                    else
                    {
                        strYear = (1911 - int.Parse(strCYear) + 1).ToString();
                    }
                }
                else
                {
                    if (int.Parse(strYear) - 1911 <= 0)
                    {
                        strCYear = (-(int.Parse(strYear) - 1912)).ToString();
                        blnMinusCYear = true;
                    }
                    else
                    {
                        strCYear = (int.Parse(strYear) - 1911).ToString();

                    }
                }

                //換掉民國年成西元年       
                vstrDate = strYear + vstrDate.Substring(vstrDate.IndexOf("/"));

                try
                {
                    objDateTime = Convert.ToDateTime(vstrDate);
                }
                catch
                {
                    //strDate & "為不正確的日期格式"
                    //throw new ArgumentException(strDate + GSS.Stirrup.NBase20.Resources.Message.NBase20_MSG._70007);
                }

                strMonth = objDateTime.Month.ToString();
                if (strMonth.Length == 1)
                {
                    strMonth = "0" + strMonth;
                }

                strDay = objDateTime.Day.ToString();
                if (strDay.Length == 1)
                {
                    strDay = "0" + strDay;
                }

                //找出星期幾
                switch (objDateTime.DayOfWeek)
                {
                    case DayOfWeek.Sunday:
                        strWeekName = "星期日";
                        break;
                    case DayOfWeek.Monday:
                        strWeekName = "星期一";
                        break;
                    case DayOfWeek.Tuesday:
                        strWeekName = "星期二";
                        break;
                    case DayOfWeek.Wednesday:
                        strWeekName = "星期三";
                        break;
                    case DayOfWeek.Thursday:
                        strWeekName = "星期四";
                        break;
                    case DayOfWeek.Friday:
                        strWeekName = "星期五";
                        break;
                    case DayOfWeek.Saturday:
                        strWeekName = "星期六";
                        break;
                }

                switch (vintType)
                {
                    case 0:
                        //1997/08/05'
                        strRet = strYear + "/" + strMonth + "/" + strDay;

                        break;
                    case 1:
                        //85/08/05' or '-86/08/05'
                        if (!blnMinusCYear)
                        {
                            strRet = strCYear + "/" + strMonth + "/" + strDay;
                        }
                        else
                        {
                            strRet = "-" + strCYear + "/" + strMonth + "/" + strDay;
                        }


                        break;
                    case 2:
                        //085/08/05' or '-086/08/05'
                        strCYear = ("00" + strCYear);
                        strCYear = strCYear.Substring(strCYear.Length - 3, 3);
                        //strCYear = strCYear.PadLeft(3, char.Parse("0"));

                        if (!blnMinusCYear)
                        {
                            strRet = strCYear + "/" + strMonth + "/" + strDay;
                        }
                        else
                        {
                            strRet = "-" + strCYear + "/" + strMonth + "/" + strDay;
                        }


                        break;
                    case 3:
                        //1997年08月05日
                        strRet = strYear + "年" + strMonth + "月" + strDay + "日";

                        break;
                    case 4:
                        //85年08月05日' or '-86年08月05日'
                        if (!blnMinusCYear)
                        {
                            strRet = strCYear + "年" + strMonth + "月" + strDay + "日";
                        }
                        else
                        {
                            strRet = "-" + strCYear + "年" + strMonth + "月" + strDay + "日";
                        }


                        break;
                    case 5:
                        //八十五年八月五日 or 前八十六年八月五日
                        if (!blnMinusCYear)
                        {
                            strRet = Number2Chinese(long.Parse(strCYear), false, false) + "年" + Number2Chinese(long.Parse(strMonth), false, false) + "月" + Number2Chinese(long.Parse(strDay), false, false) + "日";
                        }
                        else
                        {
                            strRet = "前" + Number2Chinese(long.Parse(strCYear), false, false) + "年" + Number2Chinese(long.Parse(strMonth), false, false) + "月" + Number2Chinese(long.Parse(strDay), false, false) + "日";
                        }


                        break;
                    case 6:
                        //民國八十五年八月五日 or '民國前八十六年八月五日
                        if (!blnMinusCYear)
                        {
                            strRet = "民國" + Number2Chinese(long.Parse(strCYear), false, false) + "年" + Number2Chinese(long.Parse(strMonth), false, false) + "月" + Number2Chinese(long.Parse(strDay), false, false) + "日";
                        }
                        else
                        {
                            strRet = "民國前" + Number2Chinese(long.Parse(strCYear), false, false) + "年" + Number2Chinese(long.Parse(strMonth), false, false) + "月" + Number2Chinese(long.Parse(strDay), false, false) + "日";
                        }


                        break;
                    case 7:
                        //中華民國八十五年八月五日 or '中華民國前八十六年八月五日
                        if (!blnMinusCYear)
                        {
                            strRet = "中華民國" + Number2Chinese(long.Parse(strCYear), false, false) + "年" + Number2Chinese(long.Parse(strMonth), false, false) + "月" + Number2Chinese(long.Parse(strDay), false, false) + "日";
                        }
                        else
                        {
                            strRet = "中華民國前" + Number2Chinese(long.Parse(strCYear), false, false) + "年" + Number2Chinese(long.Parse(strMonth), false, false) + "月" + Number2Chinese(long.Parse(strDay), false, false) + "日";
                        }


                        break;
                    case 8:
                        //1997年08月05日星期一
                        strRet = strYear + "年" + strMonth + "月" + strDay + "日" + strWeekName;

                        break;
                    case 9:
                        //85年08月05日星期一 or -86年08月05日星期一
                        if (!blnMinusCYear)
                        {
                            strRet = strCYear + "年" + strMonth + "月" + strDay + "日" + strWeekName;
                        }
                        else
                        {
                            strRet = "-" + strCYear + "年" + strMonth + "月" + strDay + "日" + strWeekName;
                        }


                        break;
                    case 10:
                        //八十五年八月五日星期一 or 前八十六年八月五日星期一
                        if (!blnMinusCYear)
                        {
                            strRet = Number2Chinese(long.Parse(strCYear), false, false) + "年" + Number2Chinese(long.Parse(strMonth), false, false) + "月" + Number2Chinese(long.Parse(strDay), false, false) + "日" + strWeekName;
                        }
                        else
                        {
                            strRet = "前" + Number2Chinese(long.Parse(strCYear), false, false) + "年" + Number2Chinese(long.Parse(strMonth), false, false) + "月" + Number2Chinese(long.Parse(strDay), false, false) + "日" + strWeekName;
                        }


                        break;
                    case 11:
                        //民國八十五年八月五日星期一 or '民國前八十六年八月五日星期一
                        if (!blnMinusCYear)
                        {
                            strRet = "民國" + Number2Chinese(long.Parse(strCYear), false, false) + "年" + Number2Chinese(long.Parse(strMonth), false, false) + "月" + Number2Chinese(long.Parse(strDay), false, false) + "日" + strWeekName;
                        }
                        else
                        {
                            strRet = "民國前" + Number2Chinese(long.Parse(strCYear), false, false) + "年" + Number2Chinese(long.Parse(strMonth), false, false) + "月" + Number2Chinese(long.Parse(strDay), false, false) + "日" + strWeekName;
                        }


                        break;
                    case 12:
                        //中華民國八十五年八月五日星期一 or '中華民國前八十六年八月五日星期一
                        if (!blnMinusCYear)
                        {
                            strRet = "中華民國" + Number2Chinese(long.Parse(strCYear), false, false) + "年" + Number2Chinese(long.Parse(strMonth), false, false) + "月" + Number2Chinese(long.Parse(strDay), false, false) + "日" + strWeekName;
                        }
                        else
                        {
                            strRet = "中華民國前" + Number2Chinese(long.Parse(strCYear), false, false) + "年" + Number2Chinese(long.Parse(strMonth), false, false) + "月" + Number2Chinese(long.Parse(strDay), false, false) + "日" + strWeekName;
                        }


                        break;

                    case 13:
                        //19970805
                        strRet = strYear + strMonth + strDay;

                        break;
                    case 14:
                        //850805 or -860805
                        if (!blnMinusCYear)
                        {
                            strRet = strCYear + strMonth + strDay;
                        }
                        else
                        {
                            strRet = "-" + strCYear + strMonth + strDay;
                        }


                        break;
                    case 15:
                        //0850805 or -0860805

                        strCYear = ("00" + strCYear);
                        strCYear = strCYear.Substring(strCYear.Length - 3, 3);
                        //strCYear = strCYear.PadLeft(3, "0");


                        if (!blnMinusCYear)
                        {
                            strRet = strCYear + strMonth + strDay;
                        }
                        else
                        {
                            strRet = "-" + strCYear + strMonth + strDay;
                        }


                        break;

                    case 16:
                        //星期一
                        strRet = strWeekName;

                        break;
                    case 17:
                        // 850805 or -860805
                        if (Strings.Len(strCYear) == 2)
                        {
                            strCYear = " " + strCYear;
                        }


                        if (blnMinusCYear)
                        {
                            strCYear = strCYear.Replace(" ", " -");
                        }


                        strRet = strCYear + strMonth + strDay;

                        break;
                    case 18:
                        // 85/08/05 or -86/08/05'
                        if (Strings.Len(strCYear) == 2)
                        {
                            strCYear = " " + strCYear;
                        }


                        if (blnMinusCYear)
                        {
                            strCYear = strCYear.Replace(" ", " -");
                        }


                        strRet = strCYear + "/" + strMonth + "/" + strDay;

                        break;
                    case 19:
                        //02/20/2002 'add in 1.0.1008.4
                        strRet = strMonth + "/" + strDay + "/" + strYear;

                        break;
                    default:
                        //"vintType不接受此參數" & vintType

                        ArgumentException ex = new ArgumentException("vintType不接受此參數");
                        throw ex;
                }



                return strRet;
            }
            catch
            {
                throw;

            }
        }
        

        
        /// <summary>
        /// 將字串轉日期時間
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static DateTime? ToDateTime(this string s)
        {
            if (s == null || s == string.Empty)
            {
                return null;
            }
            else
            {
                try
                {
                    return Convert.ToDateTime(s);
                }
                catch
                {
                    return null;
                }

            }
        }

        /// <summary>
        /// 把民國年日期字串轉為西元年日期
        /// </summary>
        /// <param name="dateString"></param>
        /// <returns></returns>
        public static string RocDate2Date(string dateString)
        {
            return TOCFL.ISD.Sys.Common.Conveter.ToDateString(dateString, 0);
        }

        public static string Date2RocDate(string dateString)
        {
            return TOCFL.ISD.Sys.Common.Conveter.ToDateString(dateString, 2);
        }

        public static string Date2RocDate(DateTime dateValue)
        {
            return TOCFL.ISD.Sys.Common.Conveter.ToDateString(dateValue, 2);
        }

        public static string RocDatetime2DateTime(string dateTimeString)
        {
            return TOCFL.ISD.Sys.Common.Conveter.ToDateTimeString(dateTimeString, 0, 0);
        }

        public static string DateTime2RocDateTime(string dateTimeString)
        {
            return TOCFL.ISD.Sys.Common.Conveter.ToDateTimeString(dateTimeString,2,0);
        }

        public static string DateTime2RocDateTime(DateTime datetimeValue)
        {
            return TOCFL.ISD.Sys.Common.Conveter.ToDateTimeString(datetimeValue, 2, 0);
        }
        
    }
}
