﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TOCFL.ISD.Sys.Common
{
    public class TxtFileReader
    {
        public static string ReadAllTxtFile(string path)
        {
            return File.ReadAllText(path);
        }
    }
}
