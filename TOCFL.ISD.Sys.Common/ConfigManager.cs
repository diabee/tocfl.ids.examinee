﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TOCFL.ISD.Sys.Common
{
    /// <summary>
    /// 讀取Config資料的共用程式
    /// </summary>
    public static class ConfigManager
    {
        /// <summary>
        /// 取得Config檔內的AppSetting
        /// </summary>
        /// <param name="Key">Key Value</param>
        /// <returns></returns>
        public static string GetAppSetting(string Key)
        {
            string AppSetting = string.Empty;
            AppSetting = System.Configuration.ConfigurationManager.AppSettings[Key];

            return AppSetting;
        }

        /// <summary>
        /// 取得Config檔內的DB 連線字串
        /// </summary>
        /// <param name="DB">DB ID</param>
        /// <returns></returns>
        public static string GetConnectionString(string DB)
        {

            string ConnectionString;
            
            ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings[DB].ConnectionString.ToString();
            if (Variable.IsHashConnectionString == true)
            {
                ConnectionString = DBConnectionTool.GetDESDecryptConnectionString(ConnectionString);
            }
            
            return ConnectionString;
        }

    }
}
