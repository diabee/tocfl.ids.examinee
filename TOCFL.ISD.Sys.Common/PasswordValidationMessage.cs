﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace TOCFL.ISD.Sys.Common
{
    public class PasswordValidationMessage
    {
        public bool PasswordValidation(string password, int passwordPrower, int passwordLength)
        {
            try
            {
                int passwordStrength = 0;
                if (password.Length >= passwordLength)
                {
                    string pattern = @"[A-Z]";
                    string pattern0 = @"[a-z]";
                    string pattern1 = @"[0-9]";
                    string pattern2 = @"[!@#$%^&*?_~()`=:;' ]";
                    //string pattern3 = @"[\d+]";
                    //Regex rgx = new Regex(pattern);

                    if (Regex.IsMatch(password.ToString(), pattern) || Regex.IsMatch(password.ToString(), pattern0))
                    {
                        passwordStrength++;
                    }
                    if (Regex.IsMatch(password.ToString(), pattern1))
                    {
                        passwordStrength++;
                    }
                    //if (Regex.IsMatch(password.ToString(), pattern2))
                    //{
                    //    passwordStrength++;
                    //}
                    //if (Regex.IsMatch(password.ToString(), pattern3))
                    //{
                    //    passwordStrength++;
                    //}
                    if (password.Length >= 12)
                    {
                        passwordStrength++;
                    }

                }

                if (passwordStrength >= passwordPrower)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
