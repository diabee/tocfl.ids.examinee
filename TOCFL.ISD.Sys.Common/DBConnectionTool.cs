﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Security.Cryptography;

namespace TOCFL.ISD.Sys.Common
{
    public class DBConnectionTool
    {
        public static string GetDESDecryptConnectionString(string enCryptConnectionString)
        {
            
            string[] connStrCollection = enCryptConnectionString.Split(';');
            for (int i = 0; i < connStrCollection.Length; i++)
            {
                switch (connStrCollection[i].Split('=')[0])
                {
                    case "Password":
                        connStrCollection[i] = "Password=" +
                            TOCFL.ISD.Sys.Common.Encrypt.AESDecrypt(connStrCollection[i].Replace("Password=", ""));
                        break;
                    default:
                        break;
                }
            }
            return string.Join(";", connStrCollection);
        }        
    }
}
