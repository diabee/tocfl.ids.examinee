﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TOCFL.ISD.Sys.Common
{
    public class RandomTool
    {

        #region 產生數字亂數
        public static string GetRandomNumberString(int numberLength)
        {
            System.Text.StringBuilder str_Number = new System.Text.StringBuilder();//字串儲存器
            Random rand = new Random(Guid.NewGuid().GetHashCode());//亂數物件

            for (int i = 1; i <= numberLength; i++)
            {
                str_Number.Append(rand.Next(0, 10).ToString());//產生0~9的亂數
            }

            return str_Number.ToString();
        }
        #endregion

        #region 產生大小寫英文、數字亂數
        public static string GetRandomString(int numberLength)
        {
            long ri = 1;
            foreach (byte b in Guid.NewGuid().ToByteArray())
            {
                ri *= ((int)b + 1);
            }

            Random r = new Random((int)(ri - (int)DateTime.Now.Ticks));

            string code = "";

            for (int i = 0; i < numberLength; ++i)
            {
                switch (r.Next(0, 3))
                {
                    case 0: code += r.Next(0, 10); break;
                    case 1: code += (char)r.Next(65, 91); break;
                    case 2: code += (char)r.Next(97, 122); break;
                }
            }

            return code;
        }
        #endregion

        #region 產生大寫英文、數字亂數
        public static string GetRandomNumberAndUpperString(int numberLength)
        {
            return GetRandomString(numberLength).ToUpper();
        }
        #endregion

        #region 產生小寫英文、數字亂數
        public static string GetRandomNumberAndLowerString(int numberLength)
        {
            return GetRandomString(numberLength).ToLower();
        }
        #endregion

    }
}
