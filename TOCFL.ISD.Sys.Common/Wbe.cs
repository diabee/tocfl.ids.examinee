﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace TOCFL.ISD.Sys.Common
{
    public static class Web
    {
        /// <summary>
        /// 取得Client端IP
        /// </summary>
        /// <returns></returns>
        public static string GetClientIP()
        {
            string clientIP = string.Empty;
            clientIP = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            if (string.IsNullOrEmpty(clientIP)) clientIP = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
            return clientIP;
        }

        /// <summary>
        /// ClientIP是否為外部網段
        /// </summary>
        /// <returns></returns>
        public static bool ClientIPAtInternet()
        {
            bool res = true;
            string clientIP = GetClientIP();
            if (clientIP.Contains(":"))
            {
                //IPv6
                if (clientIP.Split(':')[0].ToLower() == "fe80" ||
                    clientIP.Split(':')[0].ToLower() == "fc00" ||
                    clientIP == "::1" || clientIP == "::1/128" ||
                    clientIP == "0:0:0:0:0:0:0:1")
                    res = false;
            }
            else
            {
                //IPv4 
                if (clientIP.Split('.')[0] == "10") res = false;
                if (clientIP.Split('.')[0] == "192" && clientIP.Split('.')[1] == "168") res = false;
                if (clientIP.Split('.')[0] == "172")
                {
                    int num = int.Parse(clientIP.Split('.')[1]);
                    if (num >= 16 && num <= 31) res = false;
                }
                if (clientIP == "127.0.0.1") res = false;
            }
            return res;
        }

        /// <summary>
        /// 取得Server IP
        /// </summary>
        /// <returns></returns>
        public static string GetServerIP()
        {
            IPHostEntry host = Dns.GetHostEntry(Dns.GetHostName());
            string ServerIP = "";
            if (host.AddressList.Length > 0)
            {
                ServerIP = host.AddressList[0].ToString();
            }
            return ServerIP;
        }

        /// <summary>
        /// 設定Cookie
        /// </summary>
        /// <param name="Key"></param>
        /// <param name="Value"></param>
        public static void SetCookie(TOCFL.ISD.Sys.Common.StateKey Key, string Value)
        {
            HttpContext.Current.Request.Cookies[Key.ToString()].Value = Value;
        }

        /// <summary>
        /// 設定Cookie
        /// </summary>
        /// <param name="Key"></param>
        /// <param name="Value"></param>
        public static void SetCookie(string Key, string Value)
        {
            HttpContext.Current.Request.Cookies[Key].Value = Value;
        }

        /// <summary>
        /// 取得Cookie
        /// </summary>
        /// <param name="Key"></param>
        /// <returns></returns>
        public static string GetCookie(TOCFL.ISD.Sys.Common.StateKey Key)
        {
            if (HttpContext.Current.Request.Cookies.AllKeys.Contains(Key.ToString()))
            {
                return HttpContext.Current.Request.Cookies.Get(Key.ToString()).Value;
            }
            else
                return string.Empty;
        }

        /// <summary>
        /// 取得Cookie
        /// </summary>
        /// <param name="Key"></param>
        /// <returns></returns>
        public static string GetCookie(string Key)
        {
            if (HttpContext.Current.Request.Cookies.AllKeys.Contains(Key))
            {
                return HttpContext.Current.Request.Cookies.Get(Key).Value;
            }
            else
                return string.Empty;
        }

        /// <summary>
        /// 設定Header
        /// </summary>
        /// <param name="Key"></param>
        /// <param name="Value"></param>
        public static void SetHeader(string Key, string Value)
        {
            HttpContext.Current.Request.Headers[Key] = Value;
        }

        /// <summary>
        /// 取得Header
        /// </summary>
        /// <param name="Key"></param>
        /// <returns></returns>
        public static string GetHeader(string Key)
        {
            if (HttpContext.Current.Request.Headers.AllKeys.Contains(Key))
            {
                return HttpContext.Current.Request.Headers.Get(Key);
            }
            else
                return string.Empty;
        }

        /// <summary>
        /// 取得Form Control
        /// </summary>
        /// <param name="Key"></param>
        /// <returns></returns>
        public static string GetForm(string Key)
        {
            if (HttpContext.Current.Request.Form.AllKeys.Contains(Key))
            {
                return HttpContext.Current.Request.Form.Get(Key);
            }
            else
                return string.Empty;
        }

        public static string GetSessionId()
        {
  
            var cookie = HttpContext.Current.Request.Cookies["ASP.NET_SessionId"];
            cookie.Expires = System.DateTime.Now;

            if (cookie != null)
                return cookie.Value;
            else
                return null;
        }

    }
}
