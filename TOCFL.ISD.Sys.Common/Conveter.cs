﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
using System.IO;
using Microsoft.VisualBasic;

namespace TOCFL.ISD.Sys.Common
{
    /// <summary>
    /// 型態轉換共用程式
    /// </summary>
    public static partial class Conveter
    {

        /// <summary>
        /// Source 轉 base64
        /// </summary>
        /// <param name="dataType">資料型態</param>
        /// <param name="filePath">檔案路徑</param>
        /// <returns></returns>
        public static string GetDataUrl(DataUrlTypeEnum dataType, string filePath)
        {
            try
            {
                string contentType = string.Empty;
                switch (dataType)
                {
                    case DataUrlTypeEnum.css:
                        contentType = "text/css";
                        break;
                    case DataUrlTypeEnum.gif:
                        contentType = "image/gif";
                        break;
                    case DataUrlTypeEnum.htm:
                        contentType = "text/html";
                        break;
                    case DataUrlTypeEnum.jpg:
                        contentType = "image/jpeg";
                        break;
                    case DataUrlTypeEnum.png:
                        contentType = "image/png";
                        break;
                }
                byte[] buff = null;
                buff = File.ReadAllBytes(filePath);
                return string.Format("data:{0};base64,{1}", contentType, Convert.ToBase64String(buff));
            }
            catch (Exception ex)
            {
                throw;
            }

        }


        /// <summary>
        /// 將阿拉伯數字轉為中文數字
        /// </summary>
        /// <param name="vlngNumber"></param>
        /// <param name="vblnZero"></param>
        /// <param name="vblnCap"></param>
        /// <returns></returns>
        private static string Number2Chinese(long vlngNumber, bool vblnZero, bool vblnCap)
        {
            string functionReturnValue = null;
            int Index = 0;
            int intCount = 0;
            string strString = null;
            string strValue = null;
            string strNumNo = "";
            string strUnin = "";
            string[] strNumArray = new string[14];

            strString = "";
            intCount = 0;

            if (vblnCap)
            {
                strNumArray[0] = "零";
                strNumArray[1] = "壹";
                strNumArray[2] = "貳";
                strNumArray[3] = "參";
                strNumArray[4] = "肆";
                strNumArray[5] = "伍";
                strNumArray[6] = "陸";
                strNumArray[7] = "柒";
                strNumArray[8] = "捌";
                strNumArray[9] = "玖";
                strNumArray[10] = "拾";
                strNumArray[11] = "佰";
                strNumArray[12] = "仟";
            }
            else
            {
                strNumArray[0] = "○";
                strNumArray[1] = "一";
                strNumArray[2] = "二";
                strNumArray[3] = "三";
                strNumArray[4] = "四";
                strNumArray[5] = "五";
                strNumArray[6] = "六";
                strNumArray[7] = "七";
                strNumArray[8] = "八";
                strNumArray[9] = "九";
                strNumArray[10] = "十";
                strNumArray[11] = "百";
                strNumArray[12] = "千";
            }

            if (vlngNumber > 10 & vlngNumber < 20)
            {
                functionReturnValue = strNumArray[10] + strNumArray[vlngNumber % 10];
                return functionReturnValue;
            }
            if (vlngNumber == 10)
            {
                functionReturnValue = strNumArray[10];
                return functionReturnValue;
            }

            for (Index = Strings.Len(vlngNumber.ToString().Trim()); Index >= 1; Index += -1)
            {
                strValue = Strings.Mid((vlngNumber.ToString().Trim()), Index, 1);
                switch (int.Parse(strValue))
                {
                    case 0:
                        strNumNo = strNumArray[0];
                        break;
                    case 1:
                        strNumNo = strNumArray[1];
                        break;
                    case 2:
                        strNumNo = strNumArray[2];
                        break;
                    case 3:
                        strNumNo = strNumArray[3];
                        break;
                    case 4:
                        strNumNo = strNumArray[4];
                        break;
                    case 5:
                        strNumNo = strNumArray[5];
                        break;
                    case 6:
                        strNumNo = strNumArray[6];
                        break;
                    case 7:
                        strNumNo = strNumArray[7];
                        break;
                    case 8:
                        strNumNo = strNumArray[8];
                        break;
                    case 9:
                        strNumNo = strNumArray[9];
                        break;
                }
                intCount = intCount + 1;
                if (!vblnZero & strNumNo == strNumArray[0])
                {
                    switch (intCount)
                    {
                        case 5:
                            strUnin = "萬";
                            break;
                        case 9:
                            strUnin = "億";
                            break;
                        default:
                            strUnin = "";
                            break;
                    }
                    strString = strUnin + strString;
                }
                else
                {
                    switch (intCount)
                    {
                        case 2:
                            strUnin = strNumArray[10];
                            break;
                        case 3:
                            strUnin = strNumArray[11];
                            break;
                        case 4:
                            strUnin = strNumArray[12];
                            break;
                        case 5:
                            strUnin = "萬";
                            break;
                        case 6:
                            strUnin = strNumArray[10];
                            break;
                        case 7:
                            strUnin = strNumArray[11];
                            break;
                        case 8:
                            strUnin = strNumArray[12];
                            break;
                        case 9:
                            strUnin = "億";
                            break;
                    }
                    strString = strNumNo + strUnin + strString;
                }
            }

            return strString;
        }
    }
}
