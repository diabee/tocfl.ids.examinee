﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace TOCFL.ISD.Sys.Common
{
    /// <summary>
    /// 產生Url(網址)的共用程式
    /// </summary>
    public class UrlBulider
    {

        //private string _authority;
        private string _host;
        private int? _port;
        private Dictionary<string, object> _query = new Dictionary<string, object>();

        /// <summary>
        /// 建構式
        /// </summary>
        /// <param name="host">機器名稱 Or Ip</param>
        public UrlBulider(string host)
            : this(host, null)
        {
        }
        /// <summary>
        /// 建構式
        /// </summary>
        /// <param name="host">機器名稱 Or Ip</param>
        /// <param name="port">通訊埠</param>
        public UrlBulider(string host, int? port)
        {
            this._host = host;
            this._port = port;
        }
        /// <summary>
        /// 增加URL參數
        /// </summary>
        /// <param name="key">參數名稱</param>
        /// <param name="value">參數值</param>
        /// <returns></returns>
        public UrlBulider AddQuery(string key, object value)
        {
            this._query.Add(key, value);
            return this;
        }
        /// <summary>
        /// 產生Url字串
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            string url = _host;
            if (_port.HasValue)
            {
                url += ":" + _port.ToString();
            }


            return AppendQuery(url);
        }

        /// <summary>
        /// 將Url 跟參數陣列轉換成完整的URL
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        private string AppendQuery(string url)
        {
            if (_query.Count == 0)
            {
                return url;
            }
            url += "?";
            bool isNotFirst = false;
            foreach (var key in this._query.Keys)
            {
                if (isNotFirst)
                {
                    url += "&";
                }

                url += HttpUtility.UrlEncode(key) + "=" + HttpUtility.UrlEncode(this._query[key].ToString());
                isNotFirst = true;
            }
            return url;
        }


        /// <summary>
        /// 加密，自己給 Dictionary
        /// </summary>
        /// <param name="getString"></param>
        /// <returns></returns>
        public string GetEnCryptUrl(Dictionary<string, string> getString)
        {
            string url = string.Empty;
            foreach (KeyValuePair<string, string> getData in getString)
            {
                url += getData.Key + "=" + getData.Value + "&";
            }
            url = url.Substring(0, url.Length - 1);//處理最後"&"
            string urlEnCrypt = TOCFL.ISD.Sys.Common.Encrypt.AESEnCrypt(url).ToString();//加密
            url = (_host == null || _host == "" ? "" : _host) + (_port == null ? "?p=" : ":" + _port + "?p=") + urlEnCrypt;
            return url;
        }

        /// <summary>
        /// 加密，直接用UrlBulider的 AddQuery
        /// </summary>
        /// <returns></returns>
        public string GetEnCryptUrl()
        {
            string url = string.Empty;
            foreach (KeyValuePair<string, object> getData in _query)
            {
                url += getData.Key + "=" + getData.Value + "&";
            }
            url = url.Substring(0, url.Length - 1);//處理最後"&"
            string urlEnCrypt = TOCFL.ISD.Sys.Common.Encrypt.AESEnCrypt(url).ToString();//加密
            url = (_host == null || _host == "" ? "" : _host ) + (_port == null ? "?p=" : ":" + _port + "?p=") + urlEnCrypt;
            return url;
        }

        /// <summary>
        /// 解密GetUrl
        /// </summary>
        /// <param name="url">p=dsv5g1ev165v15rvasfsgregwsgs (範例)</param>
        /// <returns></returns>
        public Dictionary<string, string> GetDecryptDataByGetEnCryptUrl(string url)
        {
            url = GetUrlRequestString(url);
            Dictionary<string, string> q = new Dictionary<string, string>();
            url = url.Replace("p=", "");
            string getString = TOCFL.ISD.Sys.Common.Encrypt.AESDecrypt(url);//解密GetUrl

            string[] getStrings = getString.Split('&');    

            foreach (string getStr in getStrings)
            {
                string[] getStrData = getStr.Split('=');
                q.Add(getStrData.ElementAtOrDefault(0), getStrData.ElementAtOrDefault(1));//取出key跟Value
            }

            return q;
        }

        /// <summary>
        /// 取得Url Get 字串
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        private string GetUrlRequestString(string url)
        {
            int urlStringIndex = url.IndexOf("?");
            string urlGetString = url.Substring(urlStringIndex + 1, url.Length - (urlStringIndex + 1));
            return urlGetString;
        }

    }
}
