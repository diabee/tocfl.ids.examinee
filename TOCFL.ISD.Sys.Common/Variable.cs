﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TOCFL.ISD.Sys.Common
{
    /// <summary>
    /// TOCFL.ISD.Sys.Common的公用變數
    /// </summary>
    public static class Variable
    {
        /// <summary>
        /// log4net設定檔的位置
        /// </summary>
        public static string Log4netConfigPath
        {
            get { return ConfigManager.GetAppSetting("Log4netConfigPath"); }
        }

        /// <summary>
        /// DB 連線字串是否加密
        /// </summary>
        public static bool IsHashConnectionString
        {
            get { return (ConfigManager.GetAppSetting("IsHashConnectionString") == "Y" ? true : false); }
        }

    }
}
