﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TOCFL.ISD.Dao.Code;
using TOCFL.ISD.Model.Code;

namespace TOCFL.ISD.Examinee.Service.Code
{
    public class CodeService
    {
        CodeDataDao codeDataDao = new CodeDataDao();
        //service instance
        /// <summary>
        /// 通過等級
        /// </summary>
        /// <returns>
        /// List<CodeData>
        /// </returns>
        public List<CodeData> GetPassLevel()
        {
            return codeDataDao.GetResultCodeByType(Variable.PassLevel);
        }
        

        /// <summary>
        /// 缺考註記
        /// </summary>
        /// <returns></returns>
        public List<CodeData> GetAbsentComment()
        {
            return codeDataDao.GetResultCodeByType(Variable.AbsentComment);
        }
        /// <summary>
        /// 通過CEFR等級
        /// </summary>
        /// <returns></returns>
        public List<CodeData> GetPassCEFRLevel()
        {
            return codeDataDao.GetResultCodeByType(Variable.PassCEFRLevel);
        }
        /// <summary>
        /// 通過ACTFL等級
        /// </summary>
        /// <returns></returns>
        public List<CodeData> GetPassACTFLLevel()
        {
            return codeDataDao.GetResultCodeByType(Variable.PassACTFLLevel);
        }

        public List<CodeData> GetTestCategory()
        {
            return codeDataDao.GetExamCodeByType(Variable.TestCategory);
        }
        public List<CodeData> GetTestType()
        {
            return codeDataDao.GetExamCodeByType(Variable.TestType);
        }


        /// <summary>
        /// 考試等級
        /// </summary>
        /// <returns></returns>
        public List<CodeData> GetExamLevel()
        {
            return codeDataDao.GetExamCodeByType(Variable.ExamLevel);
        }

        /// <summary>
        /// 考試類型
        /// </summary>
        /// <returns></returns>
        public List<CodeData> GetExamType()
        {
            return codeDataDao.GetExamCodeByType(Variable.ExamType);
        }

        public List<CountryCodeData> GetCountries()
        {
            return codeDataDao.GetCountries();
        }

        public List<CodeData> GetCHType()
        {
            return codeDataDao.GetExamCodeByType(Variable.CHType);
        }
    }
}