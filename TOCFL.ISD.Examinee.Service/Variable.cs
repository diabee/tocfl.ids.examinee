﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TOCFL.ISD.Examinee.Service
{
    public class Variable
    {
        public static string TestCategory
        {
            get { return "Test_Category"; }
        }
        public static string TestType
        {
            get { return "Test_Type"; }
        }
        public static string AbsentComment
        {
            get { return "Absent_Comment"; }
        }
        public static string PassLevel
        {
            get { return "Pass_Level"; }
        }
        public static string PassACTFLLevel
        {
            get { return "Pass_ACTFL_Level"; }
        }
        public static string PassCEFRLevel
        {
            get { return "Pass_CEFR_Level"; }
        }

        public static string ExamLevel
        {
            get { return "Exam_Level"; }
        }
        public static string ExamType
        {
            get { return "Exam_Type"; }
        }

        public static string N { get { return "N"; } }
        public static string Y { get { return "Y"; } }

        public static string C { get { return "C"; } }

        public static string CHType { get { return "Ch_Type"; } }
    }
}