﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TOCFL.ISD.Args.Condition;
using TOCFL.ISD.Args.Response;
using TOCFL.ISD.Dao.Testee;
using TOCFL.ISD.Model.Testee;

namespace TOCFL.ISD.Examinee.Service.Testee
{
    public class TesteeService
    {
        TesteeDao testeeDao = new TesteeDao();

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<TesteeDetailInfo> GetAllTesteeInfo()
        {
            return testeeDao.GetAllTesteeDetail(); ;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<TesteeDetailInfo> GetTesteeInfoByCondition(ExamCondition testeeCondition)
        {
            return testeeDao.GetTesteeDetailByCondition(testeeCondition); ;
        }

        public IList<TesteeGridData> GetTesteeByExamId(TesteeGridCondition testeeGridCondition, ref int total)
        {
            return testeeDao.GetTesteeDetailsByExamId(testeeGridCondition,ref total); ;
        }

        public TesteeDetailInfo GetTesteeByTesteeId(int testeeId)
        {
            return testeeDao.GetTesteeDetailInfoByTesteeId(testeeId);
        }

    }
}