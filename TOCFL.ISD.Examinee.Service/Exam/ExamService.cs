﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TOCFL.ISD.Args.Condition;
using TOCFL.ISD.Args.Generic;
using TOCFL.ISD.Args.Response;
using TOCFL.ISD.Dao.Code;
using TOCFL.ISD.Dao.Exam;
using TOCFL.ISD.Exam.Api.Areas.Exam.Controllers;
using TOCFL.ISD.Model.Exam;

namespace TOCFL.ISD.Examinee.Service.Exam
{
    public class ExamService
    {
        ExamDao examDao = new ExamDao();

        public IList<ExamGridData> GetExamByConditions(ExamGridCondition exmaGridCondition, ref int total)
        {
            return examDao.GetExamByConditions(exmaGridCondition, ref total);
        }

        public IList<ExamUploadGridData> GetExamUploadByConditions(ExamUploadGridCondition examUploadGridCondition, ref int total)
        {
            return examDao.GetUploadExamByConditions(examUploadGridCondition, ref total);

        }

        public int AddExam(ExamDataArg examDataArg)
        {
            examDataArg.UploadFlag = Variable.N;
            return examDao.AddExam(examDataArg);
        }

        public bool DeleteExamById(int exmaId, string uploadFlag)
        {
                return examDao.DeleteTempExamById(exmaId);

        }

        public bool SetUploadFlagCreate(int examId,int fileId)
        {
            return examDao.SetUploadFlag(examId,fileId,Variable.C);
        }

        /// <summary>
        /// 最後上傳結束專用
        /// </summary>
        /// <param name="examSpecResult"></param>
        public bool UpdateExamWithImport(ExamSpecResult examSpecResult)
        {
            CodeDataDao codeDataDao = new CodeDataDao();
            examSpecResult.UploadFlag = Variable.Y;
            examSpecResult.ExamDefine = codeDataDao.GetCodeByExamDefine(examSpecResult.ExamDefine);
            return examDao.UpdateDataFromExamResult(examSpecResult); ;
        }

        public bool UpateExam(ExamDataArg examDataArg)
        {
            return examDao.UpdateExam(examDataArg);
        }
    }
}