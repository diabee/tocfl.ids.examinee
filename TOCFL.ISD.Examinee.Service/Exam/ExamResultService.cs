﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TOCFL.ISD.Args.Condition;
using TOCFL.ISD.Args.Response;
using TOCFL.ISD.Dao.Exam;

namespace TOCFL.ISD.Examinee.Service.Exam
{
    public class ExamResultService
    {
        ExamResultDao examResultDao = new ExamResultDao();

        public IList<TesteeScoreData> GetPrintScoresByCondition(TesteeGridCondition testeeGridCondition)
        {
            return examResultDao.GetPrintScores(testeeGridCondition);
        }

        public IList<CertificateData> GetPrintCertificatesByCondition(TesteeGridCondition testeeGridCondition)
        {
            IList <CertificateData> certificateDatas = examResultDao.GetPrintCertificates(testeeGridCondition);
            return certificateDatas;
        }

        public bool UpdateNosWithCert(IList<CertificateData> certificateDatas)
        {
            return examResultDao.UpdateNosWithCert(certificateDatas);
        }
    }
}