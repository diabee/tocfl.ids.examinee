﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;
using TOCFL.ISD.Dao.Code;
using TOCFL.ISD.Dao.Dao.Testee;
using TOCFL.ISD.Dao.Exam;
using TOCFL.ISD.Dao.Testee;
using TOCFL.ISD.Model.Exam;
using TOCFL.ISD.Model.Testee;

namespace TOCFL.ISD.Examinee.Service.Common
{
    public class ExcelService
    {

        TesteeDao testeeDao = new TesteeDao();
        ExamDao examDao = new ExamDao();
        ExamResultDao examResultDao = new ExamResultDao();
        TesteeExamRelDao testeeExamRel = new TesteeExamRelDao();

        CodeDataDao codeDataDao = new CodeDataDao();

        public bool ImportExcel(ref ExamSpecResult examSpecResult, IList<ExamResult> examResults, IList<TesteeDetailInfo> testeeDetailInfos)
        {
            //use transitionscope 確保資料

            //1.先建立考試 塞資料到examDao

            int siteID = codeDataDao.GetIdBySiteName(examSpecResult.School,null);
            if (examSpecResult.ExamDefine.Equals("TOCFL"))
            {
                examSpecResult.NoPassCount = examResults.Count(n => n.LRLevelEN == "No Pass" || n.LRLevelEN == "");

            }
            else
            {
                examSpecResult.NoPassCount = examResults.Count(n => !n.PassStatus.Contains("Pass"));
            }

            examSpecResult.PassCount = examResults.Count - examSpecResult.NoPassCount;
            //int examID = examDao.UpdateDataFromExamResult(examSepcResult);
            IList<TesteeExamRel> testeeExamRefs = GenerateTesteeExamRel(siteID, examSpecResult.ExamId, ref examSpecResult, ref testeeDetailInfos, ref examResults);
            //1.塞資料到testeeDao

            testeeDao.AddDataFromTesteeDetailInfo(testeeDetailInfos);
            examResultDao.AddDataFromExamResult(examResults);
            //3.建立TesteeExamRel
            testeeExamRel.AddTesteeExamRel(testeeExamRefs);

            return true;
        }

        private IList<TesteeExamRel> GenerateTesteeExamRel(int siteId, int examId, ref ExamSpecResult examSpecResult, ref IList<TesteeDetailInfo> testeeDetailInfos, ref IList<ExamResult> examResults)
        {
            IList<TesteeExamRel> testeeExamRels = new List<TesteeExamRel>();

            foreach (var examResult in examResults)
            {
                examResult.ExamSiteId = siteId;
                examResult.FileId = examSpecResult.FileId;
                foreach (var testeeDetailInfo in testeeDetailInfos)
                {
                    TesteeExamRel testeeExamRel = new TesteeExamRel();
                    testeeExamRel.FileId = examSpecResult.FileId;
                    if (testeeDetailInfo.eno.Equals(examResult.eno))
                    {
                        testeeDetailInfo.FileId = examSpecResult.FileId;
                        testeeExamRel.ExamId = examId;
                        testeeExamRel.TesteeId = testeeDetailInfo.TesteeId;
                        testeeExamRel.AdmissionId = examResult.eno;
                        testeeExamRel.ExamResultId = examResult.ExamResultId;
                        testeeExamRel.Comment = examResult.Comment;
                        testeeExamRels.Add(testeeExamRel);
                        break;
                    }
                    ///testeeExamRel.Account = examResult.eno;
                }

            }
            return testeeExamRels;
        }

        public int GetTesteeIdx()
        {
            return testeeDao.GetIdxFromTestee();
        }
        public int GetExamResultIdx()
        {
            return examResultDao.GetIdxFromExamResult();
        }

    }
}