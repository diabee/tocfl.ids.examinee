﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TOCFL.ISD.Dao.Common;
using TOCFL.ISD.Model.Common;

namespace TOCFL.ISD.Examinee.Service.Common
{
    public class FileService
    {
        FileDao fileDao = new FileDao();
        FileTempDao fileTempDao = new FileTempDao();
        public int AddUploadLog(FileLog fileLog)
        {
            return fileDao.AddFileLog(fileLog);
        }

        public string GetPathById(int fileId)
        {

            return fileDao.GetDataById(fileId).FilePath;
        }
        public bool DeleteFileLogRelationById(int fileId)
        {

            return fileDao.DeleteFileLogRelationById(fileId);
        }

        public bool CheckFileExist(string fileName)
        {
            return fileDao.CheckFileExistByName(fileName);
        }

        public bool CheckTempFileExist(string fileName)
        {
            return fileTempDao.CheckTempFileExistByName(fileName);
        }

        public int AddUploadTempLog(FileTempLog fileTempLog)
        {
            return fileTempDao.AddFileTempLog(fileTempLog);
        }

        public FileTempLog GetFileTempByExamId(int examId)
        {
            return fileDao.GetFileTempByExamId(examId);
        }
        public FileLog GetFileByExamId(int examId)
        {
            return fileDao.GetFileByExamId(examId);
        }
        public bool DeleteTempFileLogByFileId(int fileId)
        {
            return fileDao.DeleteTempFileLogByfileId(fileId);
        }

        public FileLog GetFileByFileId(int fileId)
        {
            return fileDao.GetDataById(fileId);
        }
        public FileTempLog GetFileTempByFileId(int fileId)
        {
            return fileDao.GetFileTempById(fileId);
        }

        public bool DeleteFileLogByFileId(int fileId)
        {
            return fileDao.DeleteFileLogByFileId(fileId);
        }
    }
}