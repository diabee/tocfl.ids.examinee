﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TOCFL.ISD.Sys.Common;

namespace TOCFL.ISD.Exam.Api
{
    public class Variable
    {
        public static string MainFolderPath
        {
            get { return ConfigManager.GetAppSetting("MainFolderPath"); }
        }

        public static string TempFolderPath
        {
            get { return ConfigManager.GetAppSetting("TempFolderPath"); }
        }
        public static string CCCC
        {
            get { return "CCCC"; }
        }
        public static string TOCFL
        {
            get { return "TOCFL"; }
        }

        public static string SUCCESS
        {
            get { return "success"; }
        }
        public static string FAIL
        {
            get { return "fail"; }
        }

        public static string N { get { return "N"; } }
        public static string Y { get { return "Y"; } }
        public static string C { get { return "C"; } }
        public static string DownloadRoot
        {
            get { return ConfigManager.GetAppSetting("DownloadRoot"); }
        }
        public static string DownloadFolder
        {
            get { return ConfigManager.GetAppSetting("DownloadFolder"); }
        }


        public static string ExceklParserError { get {return "Excel工作表為{0}第{1}列的[{2}]欄位出現問題，請檢察Excel文件。" ; } }

        public static string FileExist { get { return "此Excel檔案已經上傳過了，請再進行確認。"; } }

        public static string FileUploadTempSuccess { get { return "此Excel檔案已經上傳到Temp資料夾下，要進入系統請再執行建檔。"; } }

        public static string PrintScore { get { return ConfigManager.GetAppSetting("PrintScoreURL"); }  }
        public static string PrintCert { get { return ConfigManager.GetAppSetting("PrintCertURL"); } }
    }

}