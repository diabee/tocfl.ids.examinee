﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TOCFL.ISD.Exam.Api.Controllers;
using TOCFL.ISD.Exam.Api.Models;
using TOCFL.ISD.Examinee.Service.Common;
using static TOCFL.ISD.Model.Enum;
using TOCFL.ISD.Args;
using TOCFL.ISD.Exam.Api.Helper;
using TOCFL.ISD.Sys.Common;
using TOCFL.ISD.Model.Common;
using NPOI.XSSF.UserModel;

namespace TOCFL.ISD.Exam.Api.Areas.Common.Controllers
{
    [RoutePrefix("Api")]
    public class FileController : BaseController
    {
        [Route("File/CheckExist")]
        [HttpPost()]
        public IHttpActionResult CheckExist(FileArg fileArg)
        {
            FileService fileService = new FileService();
            try
            {
                if (fileService.CheckFileExist(fileArg.FileName))
                {
                    return Ok(this.ThrowResult<bool>
                        (ApiStatusEnum.OK, (int)Message.ResponseType.SUCCESS, "檔案已存在", true));
                }
                else
                {
                    return Ok(this.ThrowResult<bool>
                        (ApiStatusEnum.OK, (int)Message.ResponseType.SUCCESS, "檔案不存在", false));
                }
            }
            catch (Exception ex)
            {
                Logger.Write(LogCategoryEnum.Error, ex.ToString());
                return Ok(this.ThrowResult<bool>
                       (ApiStatusEnum.InternalServerError, (int)Message.ResponseType.FAIL, ex.ToString(), false));
            }
        }
        /// <summary>
        /// review create and created excel data
        /// </summary>
        /// <param name="fileId"></param>
        /// <param name="uploadFlag"></param>
        /// <returns></returns>
        [Route("File/GetExcelDetail/{fileId}/Upload/{uploadFlag}")]
        [HttpGet]
        public IHttpActionResult GetExcelDetail(int fileId,string uploadFlag)
        {
            FileService fileService = new FileService();
            ExcelParserHelper excelParserHelper = new ExcelParserHelper();
            UploadFileHandler uploadFileHandler = new UploadFileHandler();
            XSSFWorkbook wk = new XSSFWorkbook();
            IList<ExcelDetailResult> excelResults =new List<ExcelDetailResult>(); 
            string define = string.Empty;
            string filePath = string.Empty;
            string fileName = string.Empty;
            if (uploadFlag.Equals(Variable.C))
            {
                FileTempLog fileTempLog = fileService.GetFileTempByFileId(fileId);
                filePath = fileTempLog.FilePath;
                fileName = fileTempLog.FileName;
                wk = uploadFileHandler.GetFileByPath(fileTempLog.FilePath);
                define = fileTempLog.FileName.IndexOf(Variable.TOCFL) != -1 ? Variable.TOCFL : Variable.CCCC;
                
            }
            else if (uploadFlag.Equals(Variable.Y))
            {
                FileLog fileLog = fileService.GetFileByFileId(fileId);
                filePath = fileLog.FilePath;
                fileName = fileLog.FileName;
                wk = uploadFileHandler.GetFileByPath(fileLog.FilePath);
                define = fileLog.FileName.IndexOf(Variable.TOCFL) != -1 ? Variable.TOCFL : Variable.CCCC;
            
            }
            string url = uploadFileHandler.CopyToDownload(filePath , fileName);
            excelResults = excelParserHelper.GetExcelResultsFromExcel(wk, define);
            //Step1 get Data from local exist excel files
            var result =new 
            {
                fileURL = url,
                Data = excelResults
            };



            return Ok(this.ThrowResult<object>
                   (ApiStatusEnum.OK, (int)Message.ResponseType.SUCCESS, string.Empty, result));

        }
        ///TODO 暫時不知道要怎麼用
        /// <summary>
        /// 
        /// </summary>
        /// <param name="fileId"></param>
        /// <returns></returns>
        [Route("File/DeleteUploadFile/{fileId}")]
        [HttpGet()]
        public IHttpActionResult DeleteUploadFile(int fileId)
        {

            FileService fileService = new FileService();
            string path = fileService.GetPathById(fileId);
            UploadFileHandler uploadFileHandler = new UploadFileHandler();
            uploadFileHandler.DeleteUploadFile(path);
            fileService.DeleteFileLogByFileId(fileId);
            return Ok(this.ThrowResult<bool>
                    (ApiStatusEnum.OK, (int)Message.ResponseType.SUCCESS, "檔案已存在", true));
        }
    }
}