﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TOCFL.ISD.Exam.Api.Controllers;
using TOCFL.ISD.Exam.Api.Models;
using TOCFL.ISD.Examinee.Service.Code;
using TOCFL.ISD.Model.Code;
using static TOCFL.ISD.Model.Enum;

namespace TOCFL.ISD.Exam.Api.Areas.Common.Controllers
{
    [RoutePrefix("Api")]
    public class CommonController : BaseController
    {
        CodeService codeService = new CodeService();
        /// <summary>
        /// 考試類型代碼
        /// </summary>
        /// <returns></returns>
        [Route("Code/ExamType")]
        [HttpGet]
        public IHttpActionResult GetExamType()
        {
            List<CodeData> codeDatas = codeService.GetExamType();
            return Ok(this.ThrowResult<List<CodeData>>
                    (ApiStatusEnum.OK, (int)Message.ResponseType.SUCCESS, string.Empty, codeDatas));
        }

        /// <summary>
        /// 考試類型代碼
        /// </summary>
        /// <returns></returns>
        [Route("Code/TestCategory")]
        [HttpGet]
        public IHttpActionResult GetTestCategory()
        {
            List<CodeData> codeDatas = codeService.GetTestCategory();
            return Ok(this.ThrowResult<List<CodeData>>
                    (ApiStatusEnum.OK, (int)Message.ResponseType.SUCCESS, string.Empty, codeDatas));
        }
        /// <summary>
        /// 考試類型代碼
        /// </summary>
        /// <returns></returns>
        [Route("Code/TestType")]
        [HttpGet]
        public IHttpActionResult GetTestType()
        {
            List<CodeData> codeDatas = codeService.GetTestType();
            return Ok(this.ThrowResult<List<CodeData>>
                    (ApiStatusEnum.OK, (int)Message.ResponseType.SUCCESS, string.Empty, codeDatas));
        }
        /// <summary>
        /// 考試等級代碼
        /// </summary>
        /// <returns></returns>
        [Route("Code/ExamLevel")]
        [HttpGet]
        public IHttpActionResult ExamLevel()
        {
            List<CodeData> codeDatas = codeService.GetExamLevel();
            return Ok(this.ThrowResult<List<CodeData>>
                    (ApiStatusEnum.OK, (int)Message.ResponseType.SUCCESS, string.Empty, codeDatas));
        }
        /// <summary>
        /// 正簡體代碼
        /// </summary>
        /// <returns></returns>
        [Route("Code/CHType")]
        [HttpGet]
        public IHttpActionResult CHType()
        {
            List<CodeData> codeDatas = codeService.GetCHType();
            return Ok(this.ThrowResult<List<CodeData>>
                    (ApiStatusEnum.OK, (int)Message.ResponseType.SUCCESS, string.Empty, codeDatas));
        }

        /// <summary>
        /// ACTFL 等級代碼
        /// </summary>
        /// <returns></returns>
        [Route("Code/ACTFLLevel")]
        [HttpGet]
        public IHttpActionResult PassACTFLLevel()
        {
            List<CodeData> codeDatas = codeService.GetPassACTFLLevel();
            return Ok(this.ThrowResult<List<CodeData>>
                    (ApiStatusEnum.OK, (int)Message.ResponseType.SUCCESS, string.Empty, codeDatas));
        }
        /// <summary>
        /// CEFR 等級代碼
        /// </summary>
        /// <returns></returns>
        [Route("Code/CEFRLevel")]
        [HttpGet]
        public IHttpActionResult PassCEFRLevel()
        {
            List<CodeData> codeDatas = codeService.GetPassCEFRLevel();
            return Ok(this.ThrowResult<List<CodeData>>
                    (ApiStatusEnum.OK, (int)Message.ResponseType.SUCCESS, string.Empty, codeDatas));
        }
        /// <summary>
        /// 通過等級代碼
        /// </summary>
        /// <returns></returns>
        [Route("Code/PassLevel")]
        [HttpGet]
        public IHttpActionResult PassLevel()
        {
            List<CodeData> codeDatas = codeService.GetPassLevel();
            return Ok(this.ThrowResult<List<CodeData>>
                    (ApiStatusEnum.OK, (int)Message.ResponseType.SUCCESS, string.Empty, codeDatas));
        }
        /// <summary>
        /// 缺考註記代碼
        /// </summary>
        /// <returns></returns>
        [Route("Code/AbsentComment")]
        [HttpGet]
        public IHttpActionResult AbsentComment()
        {
            List<CodeData> codeDatas = codeService.GetAbsentComment();
            return Ok(this.ThrowResult<List<CodeData>>
                    (ApiStatusEnum.OK, (int)Message.ResponseType.SUCCESS, string.Empty, codeDatas));
        }

        /// <summary>
        /// 缺考註記代碼
        /// </summary>
        /// <returns></returns>
        [Route("Upload/UploadAttachment")]
        [HttpPost]
        public IHttpActionResult UploadAttachment()
        {
            List<CodeData> codeDatas = codeService.GetAbsentComment();
            return Ok(this.ThrowResult<List<CodeData>>
                    (ApiStatusEnum.OK, (int)Message.ResponseType.SUCCESS, string.Empty, codeDatas));
        }

        /// <summary>
        /// 國籍代碼
        /// </summary>
        /// <returns></returns>
        [Route("Code/Country")]
        [HttpGet]
        public IHttpActionResult GetCountryCode()
        {
          List<CountryCodeData> countryCodeData = codeService.GetCountries();
            return Ok(this.ThrowResult<List<CountryCodeData>>
                    (ApiStatusEnum.OK, (int)Message.ResponseType.SUCCESS, string.Empty, countryCodeData));
        }
    }
}
