﻿using System.Collections.Generic;
using System.Web.Http;
using TOCFL.ISD.Args.Condition;
using TOCFL.ISD.Args.Generic;
using TOCFL.ISD.Args.Response;
using TOCFL.ISD.Exam.Api.Controllers;
using TOCFL.ISD.Exam.Api.Models;
using TOCFL.ISD.Examinee.Service.Testee;
using TOCFL.ISD.Model.Testee;
using static TOCFL.ISD.Model.Enum;

namespace TOCFL.ISD.Exam.Api.Areas.Testee.Controllers
{
    [RoutePrefix("Api")]
    public class TesteeController : BaseController
    {
        TesteeService testeeService = new TesteeService();
        
        /// <summary>
        /// 取得考生詳細資料List
        /// </summary>
        /// <returns></returns>
        [Route("Testee/DetailInfo")]
        [HttpGet]
        public IHttpActionResult DetailInfo()
        {
            List<TesteeDetailInfo> testeeDetailInfos = testeeService.GetTesteeInfoByCondition(new ExamCondition());
            return Ok(this.ThrowResult<List<TesteeDetailInfo>>
                    (ApiStatusEnum.OK, (int)Message.ResponseType.FAIL, string.Empty, testeeDetailInfos));
        }
        /// <summary>
        /// 主介面取得考試資訊
        /// </summary>
        /// <param name="examCondition"></param>
        /// <returns></returns>
        [Route("Testee/GetTestee")]
        [HttpPost]
        public IHttpActionResult GetTesteeExamId(TesteeGridCondition testeeGridCondition)
        {
            PaggingContainer<IList<TesteeGridData>> testeeGridData = new PaggingContainer<IList<TesteeGridData>>();
            int total = 0;
            testeeGridData.Data = this.testeeService.GetTesteeByExamId(testeeGridCondition, ref total);
            testeeGridData.Total = total;
            return Ok(this.ThrowResult<PaggingContainer<IList<TesteeGridData>>>
                    (ApiStatusEnum.OK, (int)Message.ResponseType.SUCCESS, string.Empty, testeeGridData));
        }

        

        /// <summary>
        /// 取得考生詳細資訊
        /// </summary>
        /// <param name="examCondition"></param>
        /// <returns></returns>
        [Route("Testee/GetTesteeDetail/{testeeId}")]
        [HttpGet]
        public IHttpActionResult GetTesteeDetailByTesteeId(int testeeId)
        {
            TesteeDetailInfo testeeDetailInfo = this.testeeService.GetTesteeByTesteeId(testeeId);
            return Ok(this.ThrowResult<TesteeDetailInfo>
                    (ApiStatusEnum.OK, (int)Message.ResponseType.SUCCESS, string.Empty, testeeDetailInfo));
        }
        /// <summary>
        /// 取得考生考試相關資訊
        /// </summary>
        /// <returns></returns>
        [Route("Testee/ExamInfo")]
        [HttpPost]
        public IHttpActionResult ExamInfo()
        {
            List<TesteeDetailInfo> codeDatas = testeeService.GetTesteeInfoByCondition(new ExamCondition());
            return Ok(this.ThrowResult<List<TesteeDetailInfo>>
                    (ApiStatusEnum.OK, (int)Message.ResponseType.FAIL, string.Empty, codeDatas));
        }
    }
}
