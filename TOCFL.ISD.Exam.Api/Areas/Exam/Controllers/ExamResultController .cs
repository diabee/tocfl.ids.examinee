﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using TOCFL.ISD.Args.Condition;
using TOCFL.ISD.Args.Response;
using TOCFL.ISD.Exam.Api.Controllers;
using TOCFL.ISD.Exam.Api.Helper;
using TOCFL.ISD.Exam.Api.Models;
using TOCFL.ISD.Examinee.Service.Exam;
using TOCFL.ISD.Model;
using TOCFL.ISD.Sys.Common;
using static TOCFL.ISD.Model.Enum;
namespace TOCFL.ISD.Exam.Api.Areas.Exam.Controllers
{
    [RoutePrefix("Api")]
    public class ExamResultController : BaseController
    {
        ProxyHelper porxyHelper = new ProxyHelper();
        ExamService examService = new ExamService();
        ExamResultService examResultService = new ExamResultService();
        /// <summary>
        /// 主介面取得列印資訊成績單
        /// </summary>
        /// <param name="testeeGridCondition"></param>
        /// <returns></returns>
        [Route("ExamResult/PrintScore")]
        [HttpPost]
        public IHttpActionResult PrintScore(TesteeGridCondition testeeGridCondition)
        {
            try
            {
                //IList<TesteeScoreData> testeeScoreDatas = this.examResultService.GetPrintScoresByCondition(testeeGridCondition);
                var apiResult = new ApiResult<object>();
                IList<TesteeScoreData> testeeScoreDatas = this.examResultService.GetPrintScoresByCondition(testeeGridCondition);
                //p
                this.porxyHelper.ProxyPostAction<object>
                    (Variable.PrintScore, JsonSerializer.objToJsonString(testeeScoreDatas),
                    ContentTypeEnum.Json, ref apiResult);
                return Ok(this.ThrowResult<string>
                        (apiResult.ApiStatus, apiResult.Status, apiResult.Message, apiResult.sData));
            } catch (Exception ex) {
                Logger.Write(LogCategoryEnum.Error, ex.ToString());
                return Ok(this.ThrowResult<int>
                   (ApiStatusEnum.InternalServerError, (int)Message.ResponseType.FAIL, ex.Message, 0));
            }
        }
        /// <summary>
        /// 主介面取得列印資訊成績單
        /// </summary>
        /// <param name="testeeGridCondition"></param>
        /// <returns></returns>
        [Route("ExamResult/PrintCert")]
        [HttpPost]
        public IHttpActionResult PrintCert(TesteeGridCondition testeeGridCondition)
        {
            try
            {
                //IList<TesteeScoreData> testeeScoreDatas = this.examResultService.GetPrintScoresByCondition(testeeGridCondition);
                var apiResult = new ApiResult<object>();
                IList<CertificateData> certificateDatas = this.examResultService.GetPrintCertificatesByCondition(testeeGridCondition);
                
                this.porxyHelper.ProxyPostAction<object>
                    (Variable.PrintCert, JsonSerializer.objToJsonString(certificateDatas),
                    ContentTypeEnum.Json, ref apiResult);
                //step 3
                //回寫編號
                examResultService.UpdateNosWithCert(certificateDatas);

                return Ok(this.ThrowResult<string>
                        (apiResult.ApiStatus, apiResult.Status, apiResult.Message, apiResult.sData));
            }
            catch (Exception ex)
            {
                Logger.Write(LogCategoryEnum.Error, ex.ToString());
                return Ok(this.ThrowResult<int>
                   (ApiStatusEnum.InternalServerError, (int)Message.ResponseType.FAIL, ex.Message, 0));
            }
        }
        /// <summary>
        /// 主介面取得列印證書
        /// </summary>
        /// <param name="testeeGridCondition"></param>
        /// <returns></returns>
        [Route("ExamResult/PrintCertificate")]
        [HttpPost]
        public IHttpActionResult PrintCertificate(TesteeGridCondition testeeGridCondition)
        {
            IList<CertificateData> testeeScoreDatas = this.examResultService.GetPrintCertificatesByCondition(testeeGridCondition);
            return Ok(this.ThrowResult<IList<CertificateData>>
                    (ApiStatusEnum.OK, (int)Message.ResponseType.SUCCESS, string.Empty, testeeScoreDatas));
        }
    }
}
