﻿using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Transactions;
using System.Web.Http;
using TOCFL.ISD.Args;
using TOCFL.ISD.Args.Condition;
using TOCFL.ISD.Args.Generic;
using TOCFL.ISD.Args.Response;
using TOCFL.ISD.Exam.Api.Controllers;
using TOCFL.ISD.Exam.Api.Helper;
using TOCFL.ISD.Exam.Api.Models;
using TOCFL.ISD.Examinee.Service.Common;
using TOCFL.ISD.Examinee.Service.Exam;
using TOCFL.ISD.Model.Common;
using TOCFL.ISD.Model.Exam;
using TOCFL.ISD.Model.Testee;
using TOCFL.ISD.Sys.Common;
using static TOCFL.ISD.Model.Enum;

namespace TOCFL.ISD.Exam.Api.Areas.Exam.Controllers
{
    [RoutePrefix("Api")]
    public class ExamController : BaseController
    {
        ExamService examService = new ExamService();
        /// <summary>
        /// 主介面取得考試資訊
        /// </summary>
        /// <param name="examCondition"></param>
        /// <returns></returns>
        [Route("Exam/GetExam")]
        [HttpPost]
        public IHttpActionResult GetExamByCdt(ExamGridCondition examGridCondition)
        {
            PaggingContainer<IList<ExamGridData>> examGridDatas = new PaggingContainer<IList<ExamGridData>>();
            int total = 0;
            examGridDatas.Data = this.examService.GetExamByConditions(examGridCondition, ref total);
            examGridDatas.Total = total;
            return Ok(this.ThrowResult<PaggingContainer<IList<ExamGridData>>>
                    (ApiStatusEnum.OK, (int)Message.ResponseType.SUCCESS, string.Empty, examGridDatas));
        }

        [Route("Exam/UpdateExam")]
        [HttpPost]
        public IHttpActionResult UpdateExam(ExamDataArg examDataArg)
        {
            bool result = false;
            try
            {
                ExamService examService = new ExamService();
                result = examService.UpateExam(examDataArg);
                return Ok(this.ThrowResult<bool>
                       (ApiStatusEnum.OK, (int)Message.ResponseType.SUCCESS, string.Empty, result));
            }
            catch (Exception ex)
            {

                Logger.Write(LogCategoryEnum.Error, ex.ToString());
                return Ok(this.ThrowResult<bool>
                   (ApiStatusEnum.InternalServerError, (int)Message.ResponseType.FAIL, ex.Message.ToString(), result));
            }
        }

        [Route("Exam/AddExam")]
        [HttpPost]
        public IHttpActionResult AddExam(ExamDataArg examDataArg)
        {
            var ExamId = 0;
            try
            {
                ExamService examService = new ExamService();
                ExamId = examService.AddExam(examDataArg);
                return Ok(this.ThrowResult<int>
                       (ApiStatusEnum.OK, (int)Message.ResponseType.SUCCESS, string.Empty, ExamId));
            }
            catch (Exception ex)
            {

                Logger.Write(LogCategoryEnum.Error, ex.ToString());
                return Ok(this.ThrowResult<int>
                   (ApiStatusEnum.InternalServerError, (int)Message.ResponseType.FAIL, ex.Message.ToString(), ExamId));
            }
        }

        [Route("Exam/DeleteExam/{examId}/Upload/{uploadFlag}")]
        [HttpGet]
        public IHttpActionResult DeleteExam(int examId, string uploadFlag)
        {
            bool result = false;
            try
            {
                ExamService examService = new ExamService();
                FileService fileService = new FileService();



                UploadFileHandler uploadFileHandler = new UploadFileHandler();
                if (uploadFlag.Equals(Variable.C))
                {
                    FileTempLog fileTempLog = fileService.GetFileTempByExamId(examId);
                    fileService.DeleteTempFileLogByFileId(fileTempLog.FileId);
                    result = examService.DeleteExamById(examId, uploadFlag);
                    uploadFileHandler.DeleteUploadFile(fileTempLog.FilePath);

                }
                else if (uploadFlag.Equals(Variable.Y))
                {
                    FileLog fileLog = fileService.GetFileByExamId(examId);
                    result = fileService.DeleteFileLogRelationById(fileLog.FileId);
                    uploadFileHandler.DeleteUploadFile(fileLog.FilePath);
                }
                else
                {
                    result = examService.DeleteExamById(examId, uploadFlag);
                }
                return Ok(this.ThrowResult<bool>
                       (ApiStatusEnum.OK, (int)Message.ResponseType.SUCCESS, string.Empty, result));
            }
            catch (Exception ex)
            {
                Logger.Write(LogCategoryEnum.Error, ex.ToString());
                return Ok(this.ThrowResult<bool>
                      (ApiStatusEnum.InternalServerError, (int)Message.ResponseType.FAIL, ex.Message.ToString(), result));
            }
        }
        /// <summary>
        /// 上傳主介面取得考試資訊 (從上傳的觀點)
        /// </summary>
        /// <param name="examUploadGridCondition"></param>
        /// <returns></returns>
        [Route("Exam/GetExamWithUpload")]
        [HttpPost]
        public IHttpActionResult GetExamWithUpload(ExamUploadGridCondition examUploadGridCondition)
        {
            try
            {
                PaggingContainer<IList<ExamUploadGridData>> examUploadGridDatas = new PaggingContainer<IList<ExamUploadGridData>>();
                int total = 0;
                examUploadGridDatas.Data = this.examService.GetExamUploadByConditions(examUploadGridCondition, ref total);
                examUploadGridDatas.Total = total;
                return Ok(this.ThrowResult<PaggingContainer<IList<ExamUploadGridData>>>
                        (ApiStatusEnum.OK, (int)Message.ResponseType.SUCCESS, string.Empty, examUploadGridDatas));
            }
            catch (Exception ex)
            {
                Logger.Write(LogCategoryEnum.Error, ex.ToString());
                return Ok(this.ThrowResult<PaggingContainer<IList<ExamUploadGridData>>>
                        (ApiStatusEnum.InternalServerError, (int)Message.ResponseType.FAIL, ex.Message, new PaggingContainer<IList<ExamUploadGridData>>()));
            }

        }
        /// <summary>
        /// 匯入DB資料
        /// </summary>
        /// <param name="fileTempLog">Grid Row Data</param>
        /// <returns></returns>
        [Route("Exam/ImportExcelToDB")]
        [HttpPost]
        public IHttpActionResult ImportToDB(FileArg fileArg)
        {
            //TODO Error Handle
            FileService fileService = new FileService();
            ExamService examService = new ExamService();
            FileTempLog fileTempLog = fileService.GetFileTempByExamId(fileArg.ExamId);
            //ex.20160424@駐芝加哥_芝加哥慈濟人文學校TOCFL考生名單(駐外正式)+成績   =>files.FileName

            //不檢查的原因 是因為在上傳temp的時候就已經驗正過了
            //if (!fileService.CheckFileExist(fileTempLog.FileName))
            //{
            //Step 1 先讀取Path
            ExcelParserHelper excelParserHelper = new ExcelParserHelper();
            ExcelService excelService = new ExcelService();
            UploadFileHandler uploadFileHandler = new UploadFileHandler();

            ExamSpecResult examSpecResult;
            IList<ExamResult> examResults;
            IList<TesteeDetailInfo> testeeDetailInfos;

            try
            {
                XSSFWorkbook wk = uploadFileHandler.GetFileByPath(fileTempLog.FilePath);
                examSpecResult = excelParserHelper.GetSpecResuleFromFileName(fileTempLog.FileName);

                int ExamResultIdx = excelService.GetExamResultIdx();
                examResults = excelParserHelper.GetExamModelByExcel(wk, ExamResultIdx, examSpecResult.ExamDefine);//Exam Model
                int TesteeIdx = excelService.GetTesteeIdx();

                testeeDetailInfos = excelParserHelper.GetTesteeModelByExcel(wk, TesteeIdx, examSpecResult.ExamDefine);//Testee Model
                examSpecResult.TesteeCount = testeeDetailInfos.Count();
                examSpecResult.ExamId = fileTempLog.ExamId;

                examSpecResult.FileId = uploadFileHandler.SaveToPhysicalFromTemp(fileTempLog);

                //transaction scope 目前會包在這裡 
                //TODO 要思考一下 放在這裡是對的嗎?
                using (var transactionScope = new TransactionScope())
                {
                    try
                    {
                        //step 3
                        excelService.ImportExcel(ref examSpecResult, examResults, testeeDetailInfos);
                        examService.UpdateExamWithImport(examSpecResult);
                        fileService.DeleteTempFileLogByFileId(fileTempLog.FileId);
                        transactionScope.Complete();
                    }
                    catch (Exception ex)
                    {
                        transactionScope.Dispose();
                        FileLog filelog = fileService.GetFileByFileId(examSpecResult.FileId);
                        uploadFileHandler.CopyToTempFromPhysical(fileTempLog, filelog);
                        fileService.DeleteFileLogByFileId(filelog.FileId);
                        Logger.Write(LogCategoryEnum.Error, ex.ToString());
                        return Ok(this.ThrowResult<bool>
                                  (ApiStatusEnum.InternalServerError, (int)Message.ResponseType.FAIL, ex.Message.ToString(), false));

                    }
                }
            }
            catch (Exception ex)
            {
                return Ok(this.ThrowResult<bool>
                                  (ApiStatusEnum.InternalServerError, (int)Message.ResponseType.FAIL, ex.Message.ToString(), false));
            }

            return Ok(this.ThrowResult<bool>
                                (ApiStatusEnum.OK, (int)Message.ResponseType.SUCCESS, string.Empty, true));
        }
    }
}
