﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using TOCFL.ISD.Model;
using TOCFL.ISD.Sys.Common;
using static TOCFL.ISD.Model.Enum;

namespace TOCFL.ISD.Exam.Api.Helper
{
    public class ProxyHelper
    {
        public void ProxyPostAction<DataT>(string url,
            string body, ContentTypeEnum contentType, ref ApiResult<DataT> apiResult)
        {
            var httpHeaders = new Dictionary<HttpRequestHeader, string>();
            httpHeaders.Add(HttpRequestHeader.ContentType, HttpExecutor.GetContentTypesString(ContentTypeEnum.Json));
            //httpHeaders.Add(HttpRequestHeader.Authorization, Variable.FakeToken);
            var httpExecutorResult = HttpExecutor.Post(url, body, httpHeaders);
            this.ProcessResult<DataT>(httpExecutorResult, ref apiResult);
        }




        public void ProcessResult<T>(HttpExecutor.HttpExecutorResult httpExecutorResult, ref ApiResult<T> apiResult)
        {
            switch (httpExecutorResult.HttpStatus)
            {
                case HttpStatusCode.InternalServerError:
                    apiResult.ApiStatus = ApiStatusEnum.InternalServerError;
                    apiResult.Message = GetApiStatuString(ApiStatusEnum.InternalServerError);
                    break;
                case HttpStatusCode.NotFound:
                    apiResult.ApiStatus = ApiStatusEnum.NotFound;
                    apiResult.Message = GetApiStatuString(ApiStatusEnum.NotFound);
                    break;
                case HttpStatusCode.OK:
                    //apiResult = JsonSerializer.JsonStringToObj<ApiResult<T>>
                    //(httpExecutorResult.Data);

                    //if (string.IsNullOrEmpty(apiResult.Message) == true)
                    //{
                        apiResult.sData = httpExecutorResult.Data;
                        apiResult.ApiStatus =ApiStatusEnum.OK;
                    //}
                    //else
                    //{
                    //    apiResult.ApiStatus = ApiStatusEnum.Customer;
                    //}

                    break;
                case HttpStatusCode.Unauthorized:
                    apiResult.ApiStatus = ApiStatusEnum.Unauthorized;
                    apiResult.Message = GetApiStatuString(ApiStatusEnum.Unauthorized);
                    break;
                default:
                    apiResult.ApiStatus = ApiStatusEnum.UnKnow;
                    apiResult.Message = GetApiStatuString(ApiStatusEnum.UnKnow);
                    break;
            }
        }
    }
}