﻿using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using TOCFL.ISD.Examinee.Service.Common;
using TOCFL.ISD.Model.Common;
using TOCFL.ISD.Sys.Common;

namespace TOCFL.ISD.Exam.Api.Helper
{
    public class UploadFileHandler
    {

        public string DownloadRoot = Variable.DownloadRoot;
        public string FolderPath = Variable.MainFolderPath;
        public string TempFolderPath = Variable.TempFolderPath;

        public string DownloadFolder = Variable.DownloadFolder;
        /// <summary>
        /// 儲存至資料夾
        /// </summary>
        /// <param name="fileTempLog"></param>
        /// <returns></returns>
        public int SaveToPhysicalFromTemp(FileTempLog fileTempLog)
        {
            FileService fileService = new FileService();
            //存到temp資料夾
            try
            {
                DateTime dNow = DateTime.Now;
                var dateTimePostFix = dNow.ToString("HHmmss");
                var dateTimePreFix = dNow.ToString("yyyyMMdd");
                var fileName = Path.GetFileName(fileTempLog.FileName);
                var fileNamefix = dateTimePostFix + "_" + fileName;
                string folderPath = string.Format(FolderPath, new string[] { dateTimePreFix });
                string tempFolderPath = string.Format(TempFolderPath, new string[] { dateTimePreFix });
                if (!Directory.Exists(folderPath))
                {
                    Directory.CreateDirectory(folderPath);
                }
                string filePath = Path.Combine(folderPath, fileName);
                File.Copy(Path.Combine(tempFolderPath, fileName),Path.Combine(folderPath, fileName) );
                DeleteUploadFile(Path.Combine(tempFolderPath, fileName));
                return fileService.AddUploadLog(new FileLog()
                {
                    FileName = fileName,
                    FileNameFix = fileNamefix,
                    FilePath = filePath,
                    FilePathFix = filePath,
                    FileSize = fileTempLog.FileSize,
                    ParserFlag = Variable.SUCCESS,
                    CreateDate = dNow,
                    CreateUser = "Administrator"

                });
                ;
            }
            catch (Exception ex)
            {
                Logger.Write(LogCategoryEnum.Error, ex.ToString());
                throw ex;
            }

        }
        /// <summary>
        /// 儲存至Temp資料夾
        /// </summary>
        /// <param name="files"></param>
        /// <returns></returns>
        public int SaveToTemp(HttpPostedFileBase file,int examId)
        {
            FileService fileService = new FileService();
            //存到temp資料夾
            try
            {
                DateTime dNow = DateTime.Now;
                var dateTimePostFix = dNow.ToString("HHmmss");
                var dateTimePreFix = dNow.ToString("yyyyMMdd");
                var fileName = Path.GetFileName(file.FileName);
                var fileNamefix = dateTimePostFix + "_" + fileName;
                string path = "";
                string folderPath = string.Format(TempFolderPath, new string[] { dateTimePreFix });
                if (!Directory.Exists(folderPath))
                {
                    Directory.CreateDirectory(folderPath);
                }
                path = Path.Combine(folderPath, fileName);
                file.SaveAs(path);

                return fileService.AddUploadTempLog(new FileTempLog()
                {
                    FileName = fileName,
                    ExamId = examId,
                    FileNameFix = fileNamefix,
                    FilePath = path,
                    FilePathFix = path,
                    FileSize = file.ContentLength.ToString(),
                    ParserFlag = Variable.SUCCESS,
                    CreateDate = dNow,
                    CreateUser = "Administrator"

                });
                ;
            }
            catch (Exception ex)
            {
                Logger.Write(LogCategoryEnum.Error, ex.ToString());
                throw ex;
            }

        }

        public string CopyToDownload(string filePath, string fileName)
        {
            string fixFolder  = DateTime.Now.GetHashCode() + "/";
            string folderPath = string.Format(DownloadFolder, new string[] { fixFolder });
            if (!Directory.Exists(folderPath))
            {
                Directory.CreateDirectory(folderPath);
            }
            string downloadURL = string.Format(DownloadRoot, new string[] { fixFolder , fileName.Split('+')[0] + ".xlsx" }) ;
            File.Copy(filePath, Path.Combine(folderPath, fileName.Split('+')[0] + ".xlsx"));
            return downloadURL;
        }


        /// <summary>
        /// 刪除附加檔案
        /// </summary>
        /// <param name="FileNo"></param>
        /// <param name="actionType"></param>
        /// <returns></returns>
        public bool DeleteUploadFile(string path)
        {
            try
            {
                File.Delete(path);
            }
            catch (Exception ex)
            {
                Logger.Write(LogCategoryEnum.Error, ex.ToString());
                //return false; 
            }
            return true;
        }

        public XSSFWorkbook GetFileByPath(string filePath)
        {
            FileStream fs;
            //Step 2 先讀取資料 和整理匯入Model
            using (fs = new FileStream(filePath, FileMode.Open))
            {
                return new XSSFWorkbook(fs);
            }
        }

        public void CopyToTempFromPhysical(FileTempLog fileTempLog , FileLog fileLog)
        {
            FileService fileService = new FileService();
            //存到temp資料夾
            try
            {
                
                File.Copy(fileLog.FilePath, fileTempLog.FilePath);
                DeleteUploadFile(fileLog.FilePath);
                
            }
            catch (Exception ex)
            {
                Logger.Write(LogCategoryEnum.Error, ex.ToString());
                throw ex;
            }
        }
    }
}