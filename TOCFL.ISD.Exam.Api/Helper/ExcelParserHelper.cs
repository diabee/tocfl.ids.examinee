﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NPOI.XSSF.UserModel;
using NPOI.SS.UserModel;
using TOCFL.ISD.Model.Testee;
using TOCFL.ISD.Model.Exam;
using TOCFL.ISD.Sys.Common;
using TOCFL.ISD.Model.Common;
using TOCFL.ISD.Examinee.Service.Code;
using TOCFL.ISD.Model.Code;

namespace TOCFL.ISD.Exam.Api.Helper
{
    public class ExcelParserHelper
    {
        CodeService codeService = new CodeService() ;
        /// <summary>
        /// 獲得考生Mode From Excel Help
        /// </summary>
        /// <param name="wk"></param>
        /// <param name="testIdx"></param>
        /// <param name="define"></param>
        /// <returns></returns>
        public IList<TesteeDetailInfo> GetTesteeModelByExcel(XSSFWorkbook wk, int testIdx, string define)
        {
            IList<CodeData> codeDatas = codeService.GetExamLevel();
            IList<TesteeDetailInfo> testeeDetailInfos = new List<TesteeDetailInfo>();
            if (define.Equals(Variable.TOCFL))
            {
                for (var sheetNo = 0; sheetNo < (wk.NumberOfSheets - 1); sheetNo++)
                {
                    ISheet sheet = wk.GetSheetAt(sheetNo);
                    for (int rowNo = 1; rowNo <= sheet.LastRowNum; rowNo++)
                    {
                        TesteeDetailInfo testeeDetailInfo = new TesteeDetailInfo();
                        testeeDetailInfo.TesteeId = testIdx++;
                        testeeDetailInfo.eno = sheet.GetRow(rowNo).GetCell(4).ToString();
                        testeeDetailInfo.ChineseName = sheet.GetRow(rowNo).GetCell(5) == null ? string.Empty : sheet.GetRow(rowNo).GetCell(5).ToString();
                        testeeDetailInfo.EnglishName = sheet.GetRow(rowNo).GetCell(6).ToString();
                        testeeDetailInfo.Gender = sheet.GetRow(rowNo).GetCell(20).ToString();
                        testeeDetailInfo.BirthDate = Conveter.ToDateTime(sheet.GetRow(rowNo).GetCell(21).ToString());
                        testeeDetailInfo.BirthDateCh = sheet.GetRow(rowNo).GetCell(22).ToString();
                        testeeDetailInfo.BirthDateEn = sheet.GetRow(rowNo).GetCell(23).ToString();
                        testeeDetailInfo.Nationality = sheet.GetRow(rowNo).GetCell(24).ToString();
                        testeeDetailInfo.NationalityCh = sheet.GetRow(rowNo).GetCell(25).ToString();
                        testeeDetailInfo.NativeLanguage = sheet.GetRow(rowNo).GetCell(26).ToString();
                        testeeDetailInfo.LocalIdNo = sheet.GetRow(rowNo).GetCell(27).ToString();
                        testeeDetailInfo.Occupation = sheet.GetRow(rowNo).GetCell(28).ToString();
                        testeeDetailInfo.OccupationOthers = sheet.GetRow(rowNo).GetCell(29).ToString();
                        testeeDetailInfo.Email = sheet.GetRow(rowNo).GetCell(30).ToString();
                        testeeDetailInfo.Phone = sheet.GetRow(rowNo).GetCell(31).ToString();
                        testeeDetailInfo.ZipCode = sheet.GetRow(rowNo).GetCell(32).ToString();
                        testeeDetailInfo.Address = sheet.GetRow(rowNo).GetCell(33).ToString();
                        testeeDetailInfo.TaiwanHoursWeekly = sheet.GetRow(rowNo).GetCell(34).ToString();
                        testeeDetailInfo.Years = sheet.GetRow(rowNo).GetCell(35).ToString();
                        testeeDetailInfo.Months = sheet.GetRow(rowNo).GetCell(36).ToString();
                        testeeDetailInfo.ChinaHoursWeekly = sheet.GetRow(rowNo).GetCell(37).ToString();
                        testeeDetailInfo.CYears = sheet.GetRow(rowNo).GetCell(38).ToString();
                        testeeDetailInfo.CMonths = sheet.GetRow(rowNo).GetCell(39).ToString();
                        testeeDetailInfo.OtherCountryName = sheet.GetRow(rowNo).GetCell(40).ToString();
                        testeeDetailInfo.OtherHoursWeekly = sheet.GetRow(rowNo).GetCell(41).ToString();
                        testeeDetailInfo.OYears = sheet.GetRow(rowNo).GetCell(42).ToString();
                        testeeDetailInfo.OMonths = sheet.GetRow(rowNo).GetCell(43).ToString();
                        testeeDetailInfo.SpeakChineseFreq = sheet.GetRow(rowNo).GetCell(44).ToString();
                        testeeDetailInfo.StudyingMaterials = sheet.GetRow(rowNo).GetCell(45).ToString();
                        testeeDetailInfos.Add(testeeDetailInfo);
                    }
                }
            }
            else if (define.Equals(Variable.CCCC))
            {
                for (var sheetNo = 0; sheetNo < (wk.NumberOfSheets - 1); sheetNo++)
                {
                    ISheet sheet = wk.GetSheetAt(sheetNo);
                    for (int rowNo = 1; rowNo <= sheet.LastRowNum; rowNo++)
                    {
                        TesteeDetailInfo testeeDetailInfo = new TesteeDetailInfo();
                        testeeDetailInfo.TesteeId = testIdx++;
                        testeeDetailInfo.eno = sheet.GetRow(rowNo).GetCell(4).ToString();
                        testeeDetailInfo.ChineseName = sheet.GetRow(rowNo).GetCell(5) == null ? string.Empty : sheet.GetRow(rowNo).GetCell(5).ToString();
                        testeeDetailInfo.EnglishName = sheet.GetRow(rowNo).GetCell(6).ToString();
                        testeeDetailInfo.Gender = sheet.GetRow(rowNo).GetCell(23).ToString();
                        testeeDetailInfo.BirthDate = Conveter.ToDateTime(sheet.GetRow(rowNo).GetCell(24).ToString());
                        testeeDetailInfo.Nationality = sheet.GetRow(rowNo).GetCell(25).ToString();
                        testeeDetailInfo.NationalityCh = sheet.GetRow(rowNo).GetCell(26).ToString();
                        testeeDetailInfo.NativeLanguage = sheet.GetRow(rowNo).GetCell(27).ToString();
                        testeeDetailInfo.School = sheet.GetRow(rowNo).GetCell(28).ToString();
                        testeeDetailInfo.Grade = sheet.GetRow(rowNo).GetCell(29).ToString();
                        testeeDetailInfo.Email = sheet.GetRow(rowNo).GetCell(30).ToString();
                        testeeDetailInfo.Phone = sheet.GetRow(rowNo).GetCell(31).ToString();
                        testeeDetailInfo.ZipCode = sheet.GetRow(rowNo).GetCell(32).ToString();
                        testeeDetailInfo.Address = sheet.GetRow(rowNo).GetCell(33).ToString();
                        testeeDetailInfo.TaiwanHoursWeekly = sheet.GetRow(rowNo).GetCell(34).ToString();
                        testeeDetailInfo.Years = sheet.GetRow(rowNo).GetCell(35).ToString();
                        testeeDetailInfo.Months = sheet.GetRow(rowNo).GetCell(36).ToString();
                        testeeDetailInfo.ChinaHoursWeekly = sheet.GetRow(rowNo).GetCell(37).ToString();
                        testeeDetailInfo.CYears = sheet.GetRow(rowNo).GetCell(38).ToString();
                        testeeDetailInfo.CMonths = sheet.GetRow(rowNo).GetCell(39).ToString();
                        testeeDetailInfo.OtherCountryName = sheet.GetRow(rowNo).GetCell(40).ToString();
                        testeeDetailInfo.OtherHoursWeekly = sheet.GetRow(rowNo).GetCell(41).ToString();
                        testeeDetailInfo.OYears = sheet.GetRow(rowNo).GetCell(42).ToString();
                        testeeDetailInfo.OMonths = sheet.GetRow(rowNo).GetCell(43).ToString();
                        testeeDetailInfo.SpeakChineseFreq = sheet.GetRow(rowNo).GetCell(44).ToString();
                        testeeDetailInfo.LanguangLocation = sheet.GetRow(rowNo).GetCell(45).ToString();
                        testeeDetailInfo.StudyingMaterials = sheet.GetRow(rowNo).GetCell(46).ToString();
                        testeeDetailInfo.TakeYCL = sheet.GetRow(rowNo).GetCell(47).ToString();
                        testeeDetailInfo.TakeYCLWhen = sheet.GetRow(rowNo).GetCell(48).ToString();
                        testeeDetailInfo.TakeYCLWhichLevel = sheet.GetRow(rowNo).GetCell(49).ToString();
                        testeeDetailInfo.TakeYCLPassedOrNot = sheet.GetRow(rowNo).GetCell(50).ToString();
                        testeeDetailInfos.Add(testeeDetailInfo);
                    }
                }
            }

            return testeeDetailInfos;
        }
        /// <summary>
        /// 獲得考試Mode From Excel Helper
        /// </summary>
        /// <param name="file"></param>
        public IList<ExamResult> GetExamModelByExcel(XSSFWorkbook wk, int examResultIdx, string define)
        {
            IList<ExamResult> examResults = new List<ExamResult>();
            IList<ExcelDetailResult> excelDetailResults = new List<ExcelDetailResult>();
            IList<CodeData> codeDatas = codeService.GetExamLevel();

            if (define.Equals(Variable.TOCFL))
            {
                for (var sheetNo = 0; sheetNo < (wk.NumberOfSheets - 1); sheetNo++)
                {
                    ISheet sheet = wk.GetSheetAt(sheetNo);
                    for (int rowNo = 1; rowNo <= sheet.LastRowNum; rowNo++)
                    {
                        ExamResult examResult = new ExamResult();
                        examResult.ExamResultId = examResultIdx++;
                        examResult.No = sheet.GetRow(rowNo).GetCell(1).ToString() + "_" + sheet.GetRow(rowNo).GetCell(0).ToString();
                        examResult.TestLevel = sheet.GetRow(rowNo).GetCell(1).ToString();
                        //examResult.TestLevelId = codeDatas.Where(m => m.EnglishCodeName.Contains(examResult.TestLevel));
                        if(
                            codeDatas.Where(c => c.EnglishCodeName.Contains(sheet.GetRow(rowNo).GetCell(1).ToString())).ToList().Count > 0)
                        {
                            examResult.TestLevelId = codeDatas.Where(c => c.EnglishCodeName.Contains(sheet.GetRow(rowNo).GetCell(1).ToString())).ToList()[0].CodeId;
                        }
                        else
                        {
                            examResult.TestLevelId = "0";
                        }
                        examResult.TestRoom = sheet.GetRow(rowNo).GetCell(2).ToString();
                        examResult.CharacterVersion = sheet.GetRow(rowNo).GetCell(3).ToString();
                        examResult.eno = sheet.GetRow(rowNo).GetCell(4).ToString();
                        examResult.TestNO = sheet.GetRow(rowNo).GetCell(4).ToString();
                        examResult.Absent = sheet.GetRow(rowNo).GetCell(7).ToString();
                        examResult.Comment = sheet.GetRow(rowNo).GetCell(8).ToString();
                        var LScore = string.IsNullOrEmpty(sheet.GetRow(rowNo).GetCell(9).ToString()) ?
                                     "0" :
                                     sheet.GetRow(rowNo).GetCell(9).ToString();
                        examResult.LScore = Int32.Parse(LScore);
                        examResult.LLevel = sheet.GetRow(rowNo).GetCell(10).ToString();
                        var RScore = string.IsNullOrEmpty(sheet.GetRow(rowNo).GetCell(11).ToString()) ?
                                     "0" :
                                     sheet.GetRow(rowNo).GetCell(11).ToString();
                        examResult.RScore = Int32.Parse(RScore);

                        examResult.RLevel = sheet.GetRow(rowNo).GetCell(12).ToString();
                        examResult.LRLevel = sheet.GetRow(rowNo).GetCell(13).ToString();

                        examResult.LRLevelTW = sheet.GetRow(rowNo).GetCell(14).ToString();
                        examResult.LRLevelEN = sheet.GetRow(rowNo).GetCell(15).ToString();
                        examResult.LCEFRLevel = sheet.GetRow(rowNo).GetCell(16).ToString();
                        examResult.RCEFRLevel = sheet.GetRow(rowNo).GetCell(17).ToString();
                        examResult.LACTFLLevel = sheet.GetRow(rowNo).GetCell(18).ToString();
                        examResult.RACTFLlevel = sheet.GetRow(rowNo).GetCell(19).ToString();
                        examResult.PassStatus = string.Empty;
                        examResults.Add(examResult);
                    }
                }
            }
            else if (define.Equals(Variable.CCCC))
            {
                for (var sheetNo = 0; sheetNo < (wk.NumberOfSheets - 1); sheetNo++)
                {
                    ISheet sheet = wk.GetSheetAt(sheetNo);
                    for (int rowNo = 1; rowNo <= sheet.LastRowNum; rowNo++)
                    {
                        ExamResult examResult = new ExamResult();
                        examResult.ExamResultId = examResultIdx++;
                        examResult.No = sheet.GetRow(rowNo).GetCell(1).ToString() + "_" + sheet.GetRow(rowNo).GetCell(0).ToString();

                        if (
                           codeDatas.Where(c => c.CodeName.Contains(sheet.GetRow(rowNo).GetCell(1).ToString())).ToList().Count > 0)
                        {
                            examResult.TestLevelId = codeDatas.Where(c => c.CodeName.Contains(sheet.GetRow(rowNo).GetCell(1).ToString())).ToList()[0].CodeId;
                        }
                        else
                        {
                            examResult.TestLevelId = "0";
                        }
                        examResult.TestRoom = sheet.GetRow(rowNo).GetCell(2).ToString();
                        examResult.CharacterVersion = sheet.GetRow(rowNo).GetCell(3).ToString();
                        examResult.eno = sheet.GetRow(rowNo).GetCell(4).ToString();
                        examResult.TestNO = sheet.GetRow(rowNo).GetCell(4).ToString();
                        examResult.Absent = sheet.GetRow(rowNo).GetCell(7).ToString();
                        examResult.Comment = sheet.GetRow(rowNo).GetCell(8).ToString();
                        var totalScore = string.IsNullOrEmpty(sheet.GetRow(rowNo).GetCell(9).ToString()) ?
                                     "0" :
                                     sheet.GetRow(rowNo).GetCell(9).ToString();
                        examResult.TotalScore = Int32.Parse(totalScore);
                        examResult.PassStatus = sheet.GetRow(rowNo).GetCell(10).ToString();
                        var LScore = string.IsNullOrEmpty(sheet.GetRow(rowNo).GetCell(11).ToString()) ?
                                     "0" :
                                     sheet.GetRow(rowNo).GetCell(11).ToString();
                        examResult.LScore = Int32.Parse(LScore);
                        var RScore = string.IsNullOrEmpty(sheet.GetRow(rowNo).GetCell(12).ToString()) ?
                                     "0" :
                                     sheet.GetRow(rowNo).GetCell(12).ToString();
                        examResult.RScore = Int32.Parse(RScore);
                        examResult.CEFRLevel = sheet.GetRow(rowNo).GetCell(13).ToString();
                        examResult.LACTFLLevel = sheet.GetRow(rowNo).GetCell(14).ToString();
                        examResult.RACTFLlevel = sheet.GetRow(rowNo).GetCell(15).ToString();

                        examResult.L1Ability = sheet.GetRow(rowNo).GetCell(17).ToString();
                        examResult.L2Ability = sheet.GetRow(rowNo).GetCell(18).ToString();
                        examResult.L3Ability = sheet.GetRow(rowNo).GetCell(19).ToString();
                        examResult.R1Ability = sheet.GetRow(rowNo).GetCell(20).ToString();
                        examResult.R2Ability = sheet.GetRow(rowNo).GetCell(21).ToString();
                        examResult.R3Ability = sheet.GetRow(rowNo).GetCell(22).ToString();

                        examResults.Add(examResult);
                    }
                }
            }
            return examResults;
        }
        /// <summary>
        /// 獲得所有資訊 from Excel
        /// </summary>
        /// <param name="wk"></param>
        /// <param name="define"></param>
        /// <returns></returns>
        public IList<ExcelDetailResult> GetExcelResultsFromExcel(XSSFWorkbook wk, string define )
        {
            IList<ExcelDetailResult> excelDetailResults = new List<ExcelDetailResult>();


            if (define.Equals(Variable.TOCFL))
            {
                for (var sheetNo = 0; sheetNo < (wk.NumberOfSheets - 1); sheetNo++)
                {
                    ISheet sheet = wk.GetSheetAt(sheetNo);
                    for (int rowNo = 1; rowNo <= sheet.LastRowNum; rowNo++)
                    {
                        int cellIdx = 1;
                        try
                        {
                            ExcelDetailResult excelDetailResult = new ExcelDetailResult();
                            excelDetailResult.No = sheet.GetRow(rowNo).GetCell(cellIdx).ToString() + "_" + sheet.GetRow(rowNo).GetCell(0).ToString();
                            excelDetailResult.TestLevel = sheet.GetRow(rowNo).GetCell(cellIdx++).ToString();
                            excelDetailResult.TestRoom = sheet.GetRow(rowNo).GetCell(cellIdx++).ToString();
                            excelDetailResult.CharacterVersion = sheet.GetRow(rowNo).GetCell(cellIdx++).ToString();
                            //excelDetailResult.eno = sheet.GetRow(rowNo).GetCell(4).ToString();
                            excelDetailResult.TestNO = sheet.GetRow(rowNo).GetCell(cellIdx++).ToString();
                            excelDetailResult.ChineseName = sheet.GetRow(rowNo).GetCell(cellIdx) == null ? string.Empty : sheet.GetRow(rowNo).GetCell(cellIdx).ToString();
                            cellIdx += 1;
                            excelDetailResult.EnglishName = sheet.GetRow(rowNo).GetCell(cellIdx++).ToString();
                            excelDetailResult.Absent = sheet.GetRow(rowNo).GetCell(cellIdx++).ToString();
                            excelDetailResult.Comment = sheet.GetRow(rowNo).GetCell(cellIdx++).ToString();
                            
                            var LScore = string.IsNullOrEmpty(sheet.GetRow(rowNo).GetCell(cellIdx).ToString()) ?
                                         "0" :
                                         sheet.GetRow(rowNo).GetCell(cellIdx).ToString();
                            excelDetailResult.LScore = Int32.Parse(LScore);
                            cellIdx += 1;
                            excelDetailResult.LLevel = sheet.GetRow(rowNo).GetCell(cellIdx++).ToString();
                            var RScore = string.IsNullOrEmpty(sheet.GetRow(rowNo).GetCell(cellIdx).ToString()) ?
                                         "0" :
                                         sheet.GetRow(rowNo).GetCell(cellIdx).ToString();
                            excelDetailResult.RScore = Int32.Parse(RScore);

                            cellIdx += 1;
                            excelDetailResult.RLevel = sheet.GetRow(rowNo).GetCell(cellIdx++).ToString();
                            excelDetailResult.LRLevel = sheet.GetRow(rowNo).GetCell(cellIdx++).ToString();

                            excelDetailResult.LRLevelTW = sheet.GetRow(rowNo).GetCell(cellIdx++).ToString();
                            excelDetailResult.LRLevelEN = sheet.GetRow(rowNo).GetCell(cellIdx++).ToString();
                            excelDetailResult.LCEFRLevel = sheet.GetRow(rowNo).GetCell(cellIdx++).ToString();
                            excelDetailResult.RCEFRLevel = sheet.GetRow(rowNo).GetCell(cellIdx++).ToString();
                            excelDetailResult.LACTFLLevel = sheet.GetRow(rowNo).GetCell(cellIdx++).ToString();
                            excelDetailResult.RACTFLlevel = sheet.GetRow(rowNo).GetCell(cellIdx++).ToString();
                            
                            excelDetailResult.Gender = sheet.GetRow(rowNo).GetCell(cellIdx++).ToString();
                            excelDetailResult.BirthDate = sheet.GetRow(rowNo).GetCell(cellIdx++).ToString();
                            excelDetailResult.BirthDateCh = sheet.GetRow(rowNo).GetCell(cellIdx++).ToString();
                            excelDetailResult.BirthDateEn = sheet.GetRow(rowNo).GetCell(cellIdx++).ToString();
                            excelDetailResult.Nationality = sheet.GetRow(rowNo).GetCell(cellIdx++).ToString();
                            excelDetailResult.NationalityCh = sheet.GetRow(rowNo).GetCell(cellIdx++).ToString();
                            excelDetailResult.NativeLanguage = sheet.GetRow(rowNo).GetCell(cellIdx++).ToString();
                            excelDetailResult.LocalIdNo = sheet.GetRow(rowNo).GetCell(cellIdx++).ToString();
                            excelDetailResult.Occupation = sheet.GetRow(rowNo).GetCell(cellIdx++).ToString();
                            excelDetailResult.OccupationOthers = sheet.GetRow(rowNo).GetCell(cellIdx++).ToString();
                            excelDetailResult.Email = sheet.GetRow(rowNo).GetCell(cellIdx++).ToString();
                            excelDetailResult.Phone = sheet.GetRow(rowNo).GetCell(cellIdx++).ToString();
                            excelDetailResult.ZipCode = sheet.GetRow(rowNo).GetCell(cellIdx++).ToString();
                            excelDetailResult.Address = sheet.GetRow(rowNo).GetCell(cellIdx++).ToString();
                            excelDetailResult.TaiwanHoursWeekly = sheet.GetRow(rowNo).GetCell(cellIdx++).ToString();
                            excelDetailResult.Years = sheet.GetRow(rowNo).GetCell(cellIdx++).ToString();
                            excelDetailResult.Months = sheet.GetRow(rowNo).GetCell(cellIdx++).ToString();
                            excelDetailResult.ChinaHoursWeekly = sheet.GetRow(rowNo).GetCell(cellIdx++).ToString();
                            excelDetailResult.CYears = sheet.GetRow(rowNo).GetCell(cellIdx++).ToString();
                            excelDetailResult.CMonths = sheet.GetRow(rowNo).GetCell(cellIdx++).ToString();
                            excelDetailResult.OtherCountryName = sheet.GetRow(rowNo).GetCell(cellIdx++).ToString();
                            excelDetailResult.OtherHoursWeekly = sheet.GetRow(rowNo).GetCell(cellIdx++).ToString();
                            excelDetailResult.OYears = sheet.GetRow(rowNo).GetCell(cellIdx++).ToString();
                            excelDetailResult.OMonths = sheet.GetRow(rowNo).GetCell(cellIdx++).ToString();
                            excelDetailResult.SpeakChineseFreq = sheet.GetRow(rowNo).GetCell(cellIdx++).ToString();
                            excelDetailResult.StudyingMaterials = sheet.GetRow(rowNo).GetCell(cellIdx++).ToString();
                            excelDetailResults.Add(excelDetailResult);
                            excelDetailResult.PassStatus = string.Empty;
                        }
                        catch (Exception ex)
                        {
                            string tempError = string.Format(Variable.ExceklParserError, new string[] {  sheet.Workbook.GetSheetName(sheetNo) , rowNo.ToString() , sheet.GetRow(0).GetCell(cellIdx-1).ToString() });
                            Logger.Write(LogCategoryEnum.Error, tempError);
                            throw new Exception(tempError);
                        }
                    }
                }
            }
            else if (define.Equals(Variable.CCCC))
            {
                for (var sheetNo = 0; sheetNo < (wk.NumberOfSheets - 1); sheetNo++)
                {
                    ISheet sheet = wk.GetSheetAt(sheetNo);
                    for (int rowNo = 1; rowNo <= sheet.LastRowNum; rowNo++)
                    {
                        int cellIdx = 1;
                        try
                        {
                            ExcelDetailResult excelDetailResult = new ExcelDetailResult();
                            excelDetailResult.No = sheet.GetRow(rowNo).GetCell(cellIdx).ToString() + "_" + sheet.GetRow(rowNo).GetCell(0).ToString();
                            excelDetailResult.TestLevel = sheet.GetRow(rowNo).GetCell(cellIdx++).ToString();
                            excelDetailResult.TestRoom = sheet.GetRow(rowNo).GetCell(cellIdx++).ToString();
                            excelDetailResult.CharacterVersion = sheet.GetRow(rowNo).GetCell(cellIdx++).ToString();
                            //excelDetailResult.eno = sheet.GetRow(rowNo).GetCell(4).ToString();
                            excelDetailResult.TestNO = sheet.GetRow(rowNo).GetCell(cellIdx++).ToString();
                            
                            excelDetailResult.ChineseName = sheet.GetRow(rowNo).GetCell(cellIdx) == null ? string.Empty : sheet.GetRow(rowNo).GetCell(cellIdx).ToString();
                            cellIdx += 1;
                            excelDetailResult.EnglishName = sheet.GetRow(rowNo).GetCell(cellIdx++).ToString();
                            excelDetailResult.Absent = sheet.GetRow(rowNo).GetCell(cellIdx++).ToString();
                            excelDetailResult.Comment = sheet.GetRow(rowNo).GetCell(cellIdx++).ToString();
                            var totalScore = string.IsNullOrEmpty(sheet.GetRow(rowNo).GetCell(cellIdx).ToString()) ?
                                         "0" :
                                         sheet.GetRow(rowNo).GetCell(cellIdx).ToString();
                            excelDetailResult.TotalScore = totalScore;
                            //excelDetailResult.LScore = Int32.Parse(totalScore);
                            cellIdx += 1;
                            excelDetailResult.PassStatus = sheet.GetRow(rowNo).GetCell(cellIdx++).ToString();
                            //excelDetailResult.LLevel = sheet.GetRow(rowNo).GetCell(10).ToString();

                            var LScore = string.IsNullOrEmpty(sheet.GetRow(rowNo).GetCell(cellIdx).ToString()) ?
                                         "0" :
                                         sheet.GetRow(rowNo).GetCell(cellIdx).ToString();
                            excelDetailResult.LScore = Int32.Parse(LScore);
                            cellIdx++;
                            var RScore = string.IsNullOrEmpty(sheet.GetRow(rowNo).GetCell(cellIdx).ToString()) ?
                                         "0" :
                                         sheet.GetRow(rowNo).GetCell(cellIdx).ToString();
                            excelDetailResult.RScore = Int32.Parse(RScore);
                            cellIdx++;

                            excelDetailResult.CEFRLevel = sheet.GetRow(rowNo).GetCell(cellIdx++).ToString();
                            excelDetailResult.LACTFLLevel = sheet.GetRow(rowNo).GetCell(cellIdx++).ToString();
                            excelDetailResult.RACTFLlevel = sheet.GetRow(rowNo).GetCell(cellIdx++).ToString();
                            cellIdx = 17;
                            excelDetailResult.L1Ability = sheet.GetRow(rowNo).GetCell(cellIdx++).ToString();
                            excelDetailResult.L2Ability = sheet.GetRow(rowNo).GetCell(cellIdx++).ToString();
                            excelDetailResult.L3Ability = sheet.GetRow(rowNo).GetCell(cellIdx++).ToString();
                            excelDetailResult.R1Ability = sheet.GetRow(rowNo).GetCell(cellIdx++).ToString();
                            excelDetailResult.R2Ability = sheet.GetRow(rowNo).GetCell(cellIdx++).ToString();
                            excelDetailResult.R3Ability = sheet.GetRow(rowNo).GetCell(cellIdx++).ToString();


                            excelDetailResult.Gender = sheet.GetRow(rowNo).GetCell(cellIdx++).ToString();
                            excelDetailResult.BirthDate = sheet.GetRow(rowNo).GetCell(cellIdx++).ToString();
                            excelDetailResult.Nationality = sheet.GetRow(rowNo).GetCell(cellIdx++).ToString();
                            excelDetailResult.NationalityCh = sheet.GetRow(rowNo).GetCell(cellIdx++).ToString();
                            excelDetailResult.NativeLanguage = sheet.GetRow(rowNo).GetCell(cellIdx++).ToString();
                            excelDetailResult.School = sheet.GetRow(rowNo).GetCell(cellIdx++).ToString();//
                            excelDetailResult.Grade = sheet.GetRow(rowNo).GetCell(cellIdx++).ToString();
                            excelDetailResult.Email = sheet.GetRow(rowNo).GetCell(cellIdx++).ToString();
                            excelDetailResult.Phone = sheet.GetRow(rowNo).GetCell(cellIdx++).ToString();
                            excelDetailResult.ZipCode = sheet.GetRow(rowNo).GetCell(cellIdx++).ToString();
                            excelDetailResult.Address = sheet.GetRow(rowNo).GetCell(cellIdx++).ToString();
                            excelDetailResult.TaiwanHoursWeekly = sheet.GetRow(rowNo).GetCell(cellIdx++).ToString();
                            excelDetailResult.Years = sheet.GetRow(rowNo).GetCell(cellIdx++).ToString();
                            excelDetailResult.Months = sheet.GetRow(rowNo).GetCell(cellIdx++).ToString();
                            excelDetailResult.ChinaHoursWeekly = sheet.GetRow(rowNo).GetCell(cellIdx++).ToString();
                            excelDetailResult.CYears = sheet.GetRow(rowNo).GetCell(cellIdx++).ToString();
                            excelDetailResult.CMonths = sheet.GetRow(rowNo).GetCell(cellIdx++).ToString();
                            excelDetailResult.OtherCountryName = sheet.GetRow(rowNo).GetCell(cellIdx++).ToString();
                            excelDetailResult.OtherHoursWeekly = sheet.GetRow(rowNo).GetCell(cellIdx++).ToString();
                            excelDetailResult.OYears = sheet.GetRow(rowNo).GetCell(cellIdx++).ToString();
                            excelDetailResult.OMonths = sheet.GetRow(rowNo).GetCell(cellIdx++).ToString();
                            excelDetailResult.SpeakChineseFreq = sheet.GetRow(rowNo).GetCell(cellIdx++).ToString();
                            excelDetailResult.LanguangLocation = sheet.GetRow(rowNo).GetCell(cellIdx++).ToString();
                            excelDetailResult.StudyingMaterials = sheet.GetRow(rowNo).GetCell(cellIdx++).ToString();
                            excelDetailResult.TakeYCL = sheet.GetRow(rowNo).GetCell(cellIdx++).ToString();
                            excelDetailResult.TakeYCLWhen = sheet.GetRow(rowNo).GetCell(cellIdx++).ToString();
                            excelDetailResult.TakeYCLWhichLevel = sheet.GetRow(rowNo).GetCell(cellIdx++).ToString();
                            excelDetailResult.TakeYCLPassedOrNot = sheet.GetRow(rowNo).GetCell(cellIdx++).ToString();
                            excelDetailResults.Add(excelDetailResult);
                        }
                        catch (Exception ex)
                        {
                            string tempError = string.Format(Variable.ExceklParserError, new string[] { sheet.Workbook.GetSheetName(sheetNo), rowNo.ToString(), sheet.GetRow(0).GetCell(cellIdx - 1).ToString() });
                            Logger.Write(LogCategoryEnum.Error, tempError);
                            throw new Exception(tempError);
                        }

                    }
                }
            }


            return excelDetailResults;
        }
        /// <summary>
        /// 處理檔名
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public ExamSpecResult GetSpecResuleFromFileName(string fileName)
        {
            //20160424@駐芝加哥_芝加哥慈濟人文學校TOCFL考生名單(駐外正式)+成績   =>files.FileName
            ExamSpecResult examResult = new ExamSpecResult();
            examResult.FileName = fileName;
            examResult.ExamDate = fileName.Split('@')[0];
            //DateTime dt = DateTime.ParseExact(date, "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture);



            if (fileName.IndexOf(Variable.CCCC) != -1)
            {

                examResult.ExamDefine = Variable.CCCC;
            }
            else if (fileName.IndexOf(Variable.TOCFL) != -1)
            {

                examResult.ExamDefine = Variable.TOCFL;
            }//in furture

            return examResult;
        }
    }
}