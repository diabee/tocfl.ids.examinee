﻿using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using TOCFL.ISD.Exam.Api.Helper;
using TOCFL.ISD.Exam.Api.Models;
using TOCFL.ISD.Examinee.Service;
using TOCFL.ISD.Examinee.Service.Common;
using TOCFL.ISD.Examinee.Service.Exam;
using TOCFL.ISD.Model.Common;
using TOCFL.ISD.Model.Exam;
using TOCFL.ISD.Model.Testee;
using TOCFL.ISD.Sys.Common;

namespace TOCFL.ISD.Exam.Api.Controllers
{
    public class UploadController : Controller
    {
        /// <summary>
        /// Temp檔案之上傳
        /// </summary>
        /// <param name="file"></param>
        /// <param name="CaseNo"></param>
        /// <param name="CompId"></param>
        /// <param name="UsrId"></param>
        /// <returns>fileNo</returns>
        public ActionResult UploadTempFile([FromBody]HttpPostedFileBase files, [FromBody]int examId)
        {
            //TODO Error Handle
            FileService fileService = new FileService();
            ExamService examService = new ExamService();
            UploadFileHandler uploadFileHandler;
            //ex.20160424@駐芝加哥_芝加哥慈濟人文學校TOCFL考生名單(駐外正式)+成績   =>files.FileName
            if (!fileService.CheckTempFileExist(files.FileName)&& !fileService.CheckFileExist(files.FileName))
            {
                string define = "";
                 if (files.FileName.IndexOf(Variable.CCCC)!=-1)
                {
                    define = Variable.CCCC;
                }
                else if (files.FileName.IndexOf(Variable.TOCFL) != -1)
                {
                    define = Variable.TOCFL;
                }


                ExcelParserHelper excelParserHelper = new ExcelParserHelper();
                ExcelService excelService = new ExcelService();
                //step 1
                uploadFileHandler = new UploadFileHandler();
                int fileId = 0;
                //transaction scope 目前會包在這裡 
                IList<ExcelDetailResult> excelResults;
                try
                {

                    fileId = uploadFileHandler.SaveToTemp(files, examId);
                    excelResults = excelParserHelper.GetExcelResultsFromExcel(new XSSFWorkbook(files.InputStream),define);
                    examService.SetUploadFlagCreate(examId,fileId);
                }
                catch (Exception ex)
                {
                    var dateTimePreFix = DateTime.Now.ToString("yyyyMMdd");
                    string folderPath = string.Format(Variable.TempFolderPath, new string[] { dateTimePreFix });
                    fileService.DeleteTempFileLogByFileId(fileId);
                    uploadFileHandler.DeleteUploadFile(Path.Combine(folderPath, files.FileName));
                    Logger.Write(LogCategoryEnum.Error, ex.ToString());
                    return Json(new
                    {
                        Type = (int)Message.ResponseType.FAIL,
                        Message = ex.Message
                    });
                }
                return Json(new
                {
                    Type = (int)Message.ResponseType.SUCCESS,
                    Message = Variable.FileUploadTempSuccess,
                    Data = new { fileId = fileId ,
                                 ExcelResults = excelResults }
                });
            }
            else
            {
                //TODO 為了讓上傳的bar出現錯誤 這個要改進喔
                //uploadFileHandler.DeleteUploadFile(files.FileName);
                return Json(new
                {
                    Type = (int)Message.ResponseType.FAIL,
                    Message = Variable.FileExist
                });
            }

        }
    }
}