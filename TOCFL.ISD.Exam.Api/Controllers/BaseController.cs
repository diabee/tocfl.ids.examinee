﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using TOCFL.ISD.Model;
using static TOCFL.ISD.Model.Enum;

namespace TOCFL.ISD.Exam.Api.Controllers
{
    public class BaseController :  ApiController
    {
        /// <summary>
        /// 處理API Result 封包
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="apiStatus"></param>
        /// <param name="message"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        protected ApiResult<T> ThrowResult<T>(
           ApiStatusEnum apiStatus,
           int Status,
           string message, 
           T data)
        {
            var apiResult = new ApiResult<T>();

            if (apiStatus == ApiStatusEnum.OK)
            {
                //Success
                apiResult.ApiStatus = ApiStatusEnum.OK;
                apiResult.Status = Status;
                apiResult.Message = message;
                apiResult.Data = data;
            }
            else
            {
                //Error Handle
                apiResult.ApiStatus = apiStatus;
                apiResult.Status = -1;
                apiResult.Message = message;

            }

            return apiResult;
        }
    }
}