﻿namespace TOCFL.ISD.Exam.Api.Areas.Exam.Controllers
{
    public class ExamDataArg
    {
        public int ExamId { get; set; }
        public string ExamType { get; set; }

        public string ExamTypeId { get; set; }

        public string TestType { get; set; }

        public string TestTypeId { get; set; }
        public string TestCategory { get; set; }
        public string TestCategoryId { get; set; }
        public string Organizer { get; set; }
        public string Country { get; set; }

        public string CountryId { get; set; }

        public string ExamSite { get; set; }
        public string FromExamDate { get; set; }
        public string ToExamDate { get; set; }
        public string ExamYear { get; set; }
        public string UploadFlag { get; set; }
    }
}