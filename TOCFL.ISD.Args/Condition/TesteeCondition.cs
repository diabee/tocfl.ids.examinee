﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TOCFL.ISD.Args.Condition
{
    public class TesteeCondition
    {
        public string ExamId { get; set; }
        public string TesteeId { get; set; }

        public int RegStartNo { get; set; }

        public int PrintStartNo { get; set; }
    }
}