﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TOCFL.ISD.Args.Condition
{
    public class ExamUploadCondition
    {
        /// <summary>
        /// 考試類型 
        /// 聽讀 口語 寫作
        /// </summary>
        public string ExamType { get; set; }
        /// <summary>
        /// 類別
        /// 外館正式/外館預試 
        /// </summary>
        public string TestCategory { get; set; }
        /// <summary>
        /// 型態
        /// </summary>
        public string TestType { get; set; }

        /// <summary>
        /// 國家
        /// </summary>
        public int Country { get; set; }

        /// <summary>
        /// 主辦單位
        /// </summary>
        public string ExamOrganizer { get; set; }
        
        /// <summary>
        /// 考試區域
        /// </summary>
        public string ExamSite { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string FromExamDate { get; set; }

        /// <summary>
        /// Exam Date
        /// </summary>
        public string ToExamDate { get; set; }
    }
}