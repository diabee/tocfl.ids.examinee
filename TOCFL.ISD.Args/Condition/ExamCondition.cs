﻿using System;

namespace TOCFL.ISD.Args.Condition
{
    public class ExamCondition
    {
        /// <summary>
        /// 考試類型 
        /// 聽讀 口語 寫作
        /// </summary>
        public string ExamType { get; set; }
        /// <summary>
        /// 測驗等級
        /// </summary>
        public string ExamLevel { get; set; }
        /// <summary>
        /// 正體簡體
        /// </summary>
        public string ChTypeName { get; set; }
        public string ChTypeEName { get; set; }
        /// <summary>
        /// 年度
        /// </summary>
        public string FromExamDate { get; set; }
        /// <summary>
        /// 年度
        /// </summary>
        public string ToExamDate { get; set; }

        /// <summary>
        /// 國家
        /// </summary>
        public string CountryId { get; set; }
        /// <summary>
        /// 區域
        /// </summary>
        public string ExamOrganizer { get; set; }
        /// <summary>
        /// 考場
        /// </summary>
        public string ExamSite { get; set; }
        /// <summary>
        /// 開始年紀
        /// </summary>
        public int FromAge { get; set; }
        /// <summary>
        /// 結束年紀
        /// </summary>
        public int ToAge { get; set; }
        /// <summary>
        /// 性別
        /// </summary>
        public string Gender { get; set; }
    }
}