﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TOCFL.ISD.Args.Generic;

namespace TOCFL.ISD.Args.Condition
{
    public class ExamUploadGridCondition
    {
        public ExamUploadGridCondition()
        {
            this.PaggingArg = new PaggingArg();
            this.ExamUploadConditions = new ExamUploadCondition();
        }
        /// <summary>
        /// 分頁設定
        /// </summary>
        public PaggingArg PaggingArg { get; set; }

        public ExamUploadCondition ExamUploadConditions { get; set; }
    }
}