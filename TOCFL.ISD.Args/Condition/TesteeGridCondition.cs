﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TOCFL.ISD.Args.Generic;

namespace TOCFL.ISD.Args.Condition
{
    public class TesteeGridCondition
    {
        /// <summary>
        /// 建構子-分頁設定初始化
        /// </summary>
        public TesteeGridCondition()
        {
            this.PaggingArg = new PaggingArg();
            this.TesteeCondition = new TesteeCondition();
        }

        /// <summary>
        /// 分頁設定
        /// </summary>
        public PaggingArg PaggingArg { get; set; }
        /// <summary>
        /// 考生資訊條件
        /// </summary>
        public TesteeCondition TesteeCondition { get; set; }

        /// <summary>
        /// 考試資訊條件
        /// </summary>
        public IList<ExamCondition> ExamConditions { get; set; }
    }
}