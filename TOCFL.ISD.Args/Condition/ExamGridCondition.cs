﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TOCFL.ISD.Args.Generic;

namespace TOCFL.ISD.Args.Condition
{
    public class ExamGridCondition
    {
        /// <summary>
        /// 建構子-分頁設定初始化
        /// </summary>
        public ExamGridCondition()
        {
            this.PaggingArg = new PaggingArg();
            this.ExamConditions = new List<ExamCondition>();
        }

        /// <summary>
        /// 分頁設定
        /// </summary>
        public PaggingArg PaggingArg { get; set; }

        public IList<ExamCondition> ExamConditions { get; set; }
        

    }
}