﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static TOCFL.ISD.Args.Enum;

namespace TOCFL.ISD.Args.Generic
{
    public class PaggingArg
    {
        /// <summary>
        /// 頁碼
        /// </summary>
        public int PageIndex { get; set; }

        /// <summary>
        /// 每頁筆數
        /// </summary>
        public int PageSize { get; set; }

        /// <summary>
        /// 欲排序欄位-名稱
        /// </summary>
        public string SortField { get; set; }

        /// <summary>
        /// 正反序設定
        /// </summary>
        public SortExpression SortExpression { get; set; }
    }
}