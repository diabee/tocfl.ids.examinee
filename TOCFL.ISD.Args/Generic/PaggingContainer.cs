﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TOCFL.ISD.Args.Generic
{
    public class PaggingContainer<T>
    {
        
        public int Total { get; set; }
        public T Data { get; set; }
    }
}