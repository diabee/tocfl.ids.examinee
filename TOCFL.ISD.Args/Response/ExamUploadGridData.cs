﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TOCFL.ISD.Args.Response
{
    public class ExamUploadGridData
    {
        /// <summary>
        /// File Upload Id
        /// </summary>
        public string FileId { get; set; }

        /// <summary>
        /// Exam Id
        /// </summary>
        public string ExamId { get; set; }

        /// <summary>
        /// 考試類型 
        /// 聽讀 口語 寫作
        /// </summary>
        public string ExamType { get; set; }
        /// <summary>
        /// 考試類型 
        /// 聽讀 口語 寫作
        /// </summary>
        public string ExamTypeName { get; set; }
        /// <summary>
        /// 類別
        /// 外館正式/外館預試 
        /// </summary>
        public string TestCategory { get; set; }
        public string TestCategoryName { get; set; }
        /// <summary>
        /// 型態
        /// </summary>
        public string TestType { get; set; }
        public string TestTypeName { get; set; }
        /// <summary>
        /// 國家
        /// </summary>
        public string Country { get; set; }

        /// <summary>
        /// 主辦單位
        /// </summary>
        public string ExamOrganizer { get; set; }

        /// <summary>
        /// 考試區域
        /// </summary>
        public string SiteName { get; set; }

        /// <summary>
        /// 年度
        /// </summary>
        public string ExamYear { get; set; }

        /// <summary>
        /// Exam Date
        /// </summary>
        public string ExamDate
        {
            get
            {
                if (!string.IsNullOrEmpty(FromExamDate))
                {
                    if (ToExamDate.Equals(FromExamDate))
                    {
                        return ToExamDate.Split('/')[1] + ToExamDate.Split('/')[2];
                    }
                    else
                    {
                        return FromExamDate.Split('/')[1] + FromExamDate.Split('/')[2] + "~" + ToExamDate.Split('/')[1] + ToExamDate.Split('/')[2];
                    };
                }
                return "";
            }
        }

        /// <summary>
        /// Exam Date
        /// </summary>
        public string ToExamDate { get; set; }

        /// <summary>
        /// Exam Date
        /// </summary>
        public string FromExamDate { get; set; }
        /// <summary>
        /// 上傳file與否
        /// </summary>
        public string UploadFlag { get; set; }

        public DateTime CreateDate { get; set; }

        public string CreateDateFix { get { return CreateDate.ToString("yyyy/MM/dd HH:mm:ss"); } }
        public DateTime UpdateDate { get; set; }
        public string UpdateDateFix { get { return UpdateDate.ToString("yyyy/MM/dd HH:mm:ss"); } }
    }
}