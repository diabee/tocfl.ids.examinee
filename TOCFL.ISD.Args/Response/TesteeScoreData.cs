﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TOCFL.ISD.Args.Response
{
    public class TesteeScoreData
    {

        public string name { get; set; }

        public string ename { get; set; }

        public string eno { get; set; }

        public string band { get; set; }
        public string test_band { get; set; }
        public string tdate { get; set; }
        public string test_area_c { get; set; }

        public string test_area_e { get; set; }
        public string test_date { get; set; }
        public string tdate1 { get {
                DateTime dt = DateTime.ParseExact(test_date, "yyyy/MM/dd", System.Globalization.CultureInfo.InvariantCulture);
                return dt.ToString("yyyyMMdd"); } }
       

        public string c1 { get; set; }

        public string a1 { get; set; }

        public string a2 { get; set; }
        public string ls_cefr { get; set; }
        public string rd_cefr { get; set; }
        public string b1 { get; set; }
        public string b2 { get; set; }
        public string ls_actfl { get; set; }
        public string rd_actfl { get; set; }
        public string ls_s1 { get; set; }
        public string ls_s2 { get; set; }

        public string ls_s3 { get; set; }

        public string rd_s1 { get; set; }
        public string rd_s2 { get; set; }

        public string rd_s3 { get; set; }
    }
}