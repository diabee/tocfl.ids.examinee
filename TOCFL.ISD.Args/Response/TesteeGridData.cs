﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TOCFL.ISD.Args.Response
{
    public class TesteeGridData
    {
        public int TesteeId { get; set; }
        public string ChineseName { get; set; }
        public string EnglishName { get; set; }
        public string TestNo { get; set; }
        public string Absent { get; set; }
        public string RScore { get; set; }
        public string LScore { get; set; }
        public string RLevel { get; set; }
        public string LLevel { get; set; }
        public string LRLevel { get; set; }

        public string PassStatus { get; set; }
        //給前端去判斷的要不要show button
        public string c3 { get; set; }
    }
}