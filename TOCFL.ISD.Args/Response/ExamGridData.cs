﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TOCFL.ISD.Args.Response
{
    public class ExamGridData
    {
        /// <summary>
        /// 年度
        /// </summary>
        public string Years { get; set; }
        /// <summary>
        /// 考試日期
        /// </summary>
        public string ExamDateFix
        {
            get
            {
                if (!string.IsNullOrEmpty(FromExamDate))
                {
                    if (ToExamDate.Equals(FromExamDate))
                    {
                        return ToExamDate.Split('/')[1] + ToExamDate.Split('/')[2];
                    }
                    else
                    {
                        return FromExamDate.Split('/')[1] + FromExamDate.Split('/')[2] + "~" + ToExamDate.Split('/')[1] + ToExamDate.Split('/')[2];
                    };
                }
                return "";
            }
        }

        /// <summary>
        /// From考試日期
        /// </summary>
        public string FromExamDate { get; set; }
        /// <summary>
        /// TO考試日期
        /// </summary>
        public string ToExamDate { get; set; }
        public string ExamDate { get; set; }
        /// <summary>
        /// 國家
        /// </summary>
        public string Country { get; set; }
        /// <summary>
        /// 學校
        /// </summary>
        public string School { get; set; }
        /// <summary>
        /// 通過率
        /// </summary>
        public string PassRate { get { return Math.Round(PassCount/(NoPassCount+ PassCount), 2)*100 + "%"; } }
        /// <summary>
        /// 通過狀態
        /// </summary>
        public string PassStatus { get; set; }
        /// <summary>
        /// 通過人數
        /// </summary>
        public double PassCount { get; set; }
        /// <summary>
        /// 沒通過人數
        /// </summary>
        public double NoPassCount { get; set; }
        

        public string ExamDefine { get; set; }
        public int ExamId { get; set; }

        public int FileId { get; set; }
    }
}