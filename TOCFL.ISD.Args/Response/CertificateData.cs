﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TOCFL.ISD.Args.Response
{
    public class CertificateData
    {
        public int ExamResultId { get; set; }
        public int printno { get; set; }
        // regno證書序號
        public int regno { get; set; }
        // name中文名
        public string name { get; set; }
        // ename英文名
        public string ename { get; set; }
        // country_c國籍中文
        public string country_c { get; set; }
        // country_e國籍英文
        public string country_e { get; set; }


        public DateTime birthdate { get; set; }
        //bdate生日 yyyymmdd
        public string bdate
        {
            get
            {
                //DateTime dt = DateTime.ParseExact(birthdate, "yyyy/MM/dd", System.Globalization.CultureInfo.InvariantCulture);
                return birthdate.ToString("yyyyMMdd");
            }
        }
        //tdate1考試日期 yyyymmdd
        public string test_date { get; set; }
        public string tdate1
        {
            get
            {
                DateTime dt = DateTime.ParseExact(test_date, "yyyy/MM/dd", System.Globalization.CultureInfo.InvariantCulture);
                return dt.ToString("yyyyMMdd");
            }
        }
        //ctype測驗類型:聽讀、口語、寫作 (代碼)
        public string ctype { get; set; }

        //聽讀CAT 
        //$ctype_cht = '聽讀';
        //$ctype_eng = '’s Listening and Reading Comprehension';

        public string c2 { get; set; }
        public string c3 { get; set; }
        //$ctype_cht = '口語';
        //$ctype_eng = ' Speaking';
        public string d2 { get; set; }
        //$ctype_cht = '寫作';
        //$ctype_eng = ' Writing';
        public string e2 { get; set; }
    }
}